[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_miniobc-firmware&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_miniobc-firmware)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_miniobc-firmware&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_miniobc-firmware)
[![Pipeline](https://gitlab.com/sat-polsl/obc/miniobc-firmware/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/obc/miniobc-firmware/-/pipelines?page=1&scope=all&ref=main)

# MiniOBC-firmware

Firmware for the On Board Computer.

# Table of contents

- [Prerequisites](#prerequisites)
- [Setting up the project](#setting-up-the-project)
- [Reporitng an Issue](#reporting-an-issue)
- [Working with repsitory](#working-with-repository)
    - [Contribution flow](#contribution-flow)
    - [Commit message format](#commit-message-format)
- [Coding style](#coding-style)
- [Content overview](#contents-overview)
- [Useful links](#useful-links)

## Prerequisites

This project can be developed on following environments:

|        | Windows | Linux | macOS |
| ------ | ------- | ----- | ----- |
| x86_64 | ✅      | ✅    | ❌    |
| arm64  | ❌      | ❌    | ✅    |

> Tip: Always use the latest versions of these tools.

**Development**

* Git
* Make/Ninja (only Ninja on Windows)
* CMake
* Python3

**Programming**

* [OpenOCD] (at least 0.11)

**Formatting**

* Clang-Format

### Windows

* Use Ninja

## Setting up the project

Clone the repo with:

```
git clone https://gitlab.com/sat-polsl/obc/miniobc-firmware.git
```

In project's root directory:

1. Create `CMakeUserPresets.json` as described
   in [Wiki](https://sat-polsl.gitlab.io/wiki/sat-firmware-launcher-wiki/#/CMake-Presets) with
   required [Cache Variables](#cache-variables).
   `CMakeUserPresets.json.template` can be used as template.
2. Configure project: `cmake --preset <user-defined-preset>`, for example: `cmake --preset debug`.
3. Build and test project with CMake Presets or manually:
    1. CMake Presets:
        2. Run `cmake --build --preset <debug|release>` to build all.
        3. Run: `ctest --preset <debug|release>` to run tests.
    2. Manually:
        1. Enter `<build-dir>` and run:
        2. Run `ninja` to build all.
        3. Run `ctest` to run tests.

### Cache Variables

| Variable           | Required | Description                                                                            |
|--------------------|:--------:|----------------------------------------------------------------------------------------| 
| `CPM_SOURCE_CACHE` |    No    | Path to [CPM](https://sat-polsl.gitlab.io/wiki/sat-firmware-launcher-wiki/#/CPM) cache |

## Reporting an issue

Bugs are tracked with [GitLab Issue Board](https://gitlab.com/sat-polsl/obc/miniobc-firmware/-/boards)

Explain the problem and include additional details to help maintainers reproduce the problem.

## Working with repository

### Contribution flow

1. Branch out from `main` branch and name your branch `obc<issue_number>-<shortdescription>`.
2. Make commits to your branch.
3. Make sure to pass all tests and add new if needed.
4. Push your work to remote branch
5. Submit a Merge Request to `main` branch

### Commit message format

Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Coding style

Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Contents overview

The files inside the project are presented as follows:

* **cmake**: directory with CMake scripts, functions and macros.
* **docs**: directory with documentation.
* **include**: header files.
* **libs/external**: external libraries,
* **firmware**: firmware source files.
* **test**: directory with test project.
* **.clang-format**: config for code formatting.
* **.gdbinit**: debugger automatic setup.
* **.gitignore**: files to ignore by the version system.
* **.gitlab-ci.yml**: build config for GitLab CI pipelines.
* **.sonar.properties**: Sonarcloud config.
* **CMakeLists.txt**: main CMake build system script.

## Useful links

- [SAT Firmware Launcher Wiki](https://sat-polsl.gitlab.io/wiki/sat-firmware-launcher-wiki/#/)
    - in particular: [prerequisites setup wiki](https://sat-polsl.gitlab.io/wiki/sat-firmware-launcher-wiki/#/Prerequisites)
- [Doxygen](https://sat-polsl.gitlab.io/cubesat/obc/miniobc-firmware)

[OpenOCD]: https://github.com/openocd-org/openocd
