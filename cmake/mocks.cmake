function(add_mock_library TARGET)
    set(OPTIONS)
    set(ONE_VALUE)
    set(MULTI_VALUE INCLUDES LIBS SOURCES)
    cmake_parse_arguments(PARSE_ARGV 1 ADD_MOCK_LIBRARY "${OPTIONS}" "${ONE_VALUE}" "${MULTI_VALUE}")

    add_library(${TARGET} INTERFACE)

    target_include_directories(${TARGET}
        INTERFACE
        ${ADD_MOCK_LIBRARY_INCLUDES}
        )

    target_sources(${TARGET}
        PRIVATE
        ${ADD_MOCK_LIBRARY_SOURCES}
        )

    target_link_libraries(${TARGET}
        INTERFACE
        gmock
        ${ADD_MOCK_LIBRARY_LIBS}
        )
endfunction()

