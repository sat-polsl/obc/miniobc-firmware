set(ARM_NONE_EABI_GCC_DEBUG_OPTIMIZATION "g")
CPMAddPackage("gl:sat-polsl/software/packages/arm-none-eabi-gcc@13.2.1")
arm_none_eabi_gcc_setup()

CPMAddPackage("gl:sat-polsl/software/packages/doxygen@1.9.6")
CPMAddPackage("gl:sat-polsl/software/libopencm3#sat-main")
CPMAddPackage("gl:sat-polsl/software/satext@2.4.1")
CPMAddPackage("gl:sat-polsl/software/baremetal@1.1.0")
CPMAddPackage("gl:sat-polsl/software/satos@5.2.0")
CPMAddPackage("gl:sat-polsl/software/semihosting@1.2.0")
CPMAddPackage("gl:sat-polsl/software/sat-peripheral-library@0.17.0")

set(GIT_DIR_LOOKUP_POLICY ALLOW_LOOKING_ABOVE_CMAKE_SOURCE_DIR)
CPMAddPackage("gh:etlcpp/etl#20.38.6")

CPMAddPackage("gl:sat-polsl/software/packages/terminal@2.1.0")
CPMAddPackage("gl:sat-polsl/software/packages/telemetry@2.1.0")
CPMAddPackage("gl:sat-polsl/software/packages/nmea@1.3.0")
CPMAddPackage("gl:sat-polsl/software/packages/watchdog@1.2.0")
CPMAddPackage("gl:sat-polsl/software/packages/safe@1.1.0")

CPMAddPackage(
    NAME sml
    GITHUB_REPOSITORY boost-ext/sml
    GIT_TAG v1.1.6
    OPTIONS
        "SML_USE_EXCEPTIONS OFF"
        "SML_BUILD_BENCHMARKS OFF"
        "SML_BUILD_EXAMPLES OFF"
        "SML_BUILD_TESTS OFF"
)

CPMAddPackage("gh:fmtlib/fmt#10.1.1")

target_compile_definitions(fmt
    PUBLIC
    FMT_STATIC_THOUSANDS_SEPARATOR
    FMT_USE_FLOAT=0
    FMT_USE_DOUBLE=0
    FMT_USE_LONG_DOUBLE=0
    )

CPMAddPackage(
    NAME googletest
    GITHUB_REPOSITORY google/googletest
    GIT_TAG v1.13.0
    OPTIONS
    "gtest_disable_pthreads ON"
    EXCLUDE_FROM_ALL YES
)

set_target_properties(gtest gmock gmock_main
    PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON
    )

target_compile_definitions(gtest
    PRIVATE
    _POSIX_PATH_MAX=128
    )
