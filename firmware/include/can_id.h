#pragma once
#include <cstdint>

namespace obc::can::id {

struct echo {
    static constexpr std::uint32_t request = 0x500;
    static constexpr std::uint32_t response = 0x600;
};

} // namespace obc::can::id
