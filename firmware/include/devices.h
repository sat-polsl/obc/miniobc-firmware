#pragma once
#include <array>
#include <utility>
#include "drivers.h"
#include "peripherals.h"
#include "satext/noncopyable.h"
#include "types.h"

#include "devices/eps/eps.h"
#include "devices/fram/fram.h"
#include "devices/gps/gps.h"
#include "devices/mprls/mprls.h"
#include "devices/muons/muons.h"
#include "devices/radio/radio.h"
#include "devices/temperature_sensor/max31865.h"

namespace obc::threads {
class can_service;
}

namespace obc::devices {
class devices : private satext::noncopyable {
public:
    using fram_device = ::devices::fram::fram<spl::drivers::i2c::i2c_dma_vectored::i2c1>;
    using radio_device =
        ::devices::radio::radio<spl::drivers::uart::uart1, spl::peripherals::gpio::gpio>;
    using mprls_device = ::devices::mprls::mprls<spl::drivers::i2c::i2c_dma_vectored::i2c1>;
    using eps_device = ::devices::eps::eps<obc::types::can_context>;
    using gps_device = ::devices::gps::gps<spl::drivers::uart::uart2>;
    using max31865_device =
        ::devices::temperature_sensor::max31865<spl::peripherals::gpio::gpio,
                                                spl::drivers::spi::spi_dma_vectored::spi1>;
    using muons_device = ::devices::muons::muons<spl::drivers::i2c::i2c_dma_vectored::i2c1>;

    devices(obc::peripherals::peripherals& peripherals,
            obc::drivers::drivers& drivers,
            threads::can_service& can_service);

    void initialize();

    fram_device& fram();
    radio_device& radio();
    mprls_device& mprls();
    eps_device& eps();
    gps_device& gps();
    max31865_device& max31865();
    muons_device& muons();

private:
    fram_device fram_;
    radio_device radio_;
    mprls_device mprls_;
    eps_device eps_;
    std::array<char, 256> gps_buffer_;
    gps_device gps_;
    max31865_device max31865_;
    muons_device muons_;
};
} // namespace obc::devices
