#pragma once
#include "peripherals.h"
#include "satext/noncopyable.h"
#include "spl/drivers/can/can.h"
#include "spl/drivers/dma/dma.h"
#include "spl/drivers/i2c/i2c.h"
#include "spl/drivers/spi/spi.h"
#include "spl/drivers/uart/uart.h"

namespace obc::drivers {
class drivers : private satext::noncopyable {
public:
    explicit drivers(obc::peripherals::peripherals& peripherals);
    void initialize();
    void initialize_rtos_context();
    spl::drivers::uart::uart1& radio_uart();
    spl::drivers::uart::uart2& terminal_uart();
    spl::drivers::uart::uart2& gps_uart();
    spl::drivers::can::can1& can();
    spl::drivers::i2c::i2c_dma_vectored::i2c1& i2c();
    spl::drivers::dma::dma& dma1();
    spl::drivers::spi::spi_dma_vectored::spi1& spi();

private:
    spl::drivers::dma::dma dma1_;
    spl::drivers::uart::uart2 terminal_uart_;
    spl::drivers::uart::uart2& gps_uart_;
    spl::drivers::uart::uart1 radio_uart_;
    spl::drivers::can::can1 can_;
    spl::drivers::i2c::i2c_dma_vectored::i2c1 i2c_;
    spl::drivers::spi::spi_dma_vectored::spi1 spi_;
};
} // namespace obc::drivers
