#pragma once
#include <cstdint>

namespace obc {

enum class terminal_state : std::uint32_t { started = 0x1 };

}
