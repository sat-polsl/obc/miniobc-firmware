#pragma once
#include <cstdint>

namespace obc::fram_map {

constexpr std::uint32_t boot_counter_offset = 0x00;

} // namespace obc::fram_map
