#pragma once
#include "satext/units.h"
#include "spl/peripherals/can/types.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/rcc/enums.h"
#include "spl/peripherals/sysclk/configuration.h"

namespace hw {
struct led {
    static constexpr auto port = spl::peripherals::gpio::port::b;
    static constexpr auto pin = spl::peripherals::gpio::pin1;
};

struct radio {
    struct reset {
        static constexpr auto port = spl::peripherals::gpio::port::c;
        static constexpr auto pin = spl::peripherals::gpio::pin14;
    };
    struct tx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin9;
        static constexpr auto af = spl::peripherals::gpio::af7;
    };
    struct rx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin10;
        static constexpr auto af = spl::peripherals::gpio::af7;
    };
    static constexpr std::uint32_t baudrate = 57600;
};

struct terminal_uart {
    struct tx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin2;
        static constexpr auto af = spl::peripherals::gpio::af7;
    };
    struct rx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin15;
        static constexpr auto af = spl::peripherals::gpio::af3;
    };
    static constexpr std::uint32_t baudrate = 115200;
};

struct gps_uart {
    struct tx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin2;
        static constexpr auto af = spl::peripherals::gpio::af7;
    };
    struct rx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin3;
        static constexpr auto af = spl::peripherals::gpio::af7;
    };
    static constexpr std::uint32_t baudrate = 9600;
};

struct can {
    struct tx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin12;
    };
    struct rx {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin11;
    };
    static constexpr auto timings =
        spl::peripherals::can::timings{.sjw = 1, .ts1 = 8, .ts2 = 1, .brp = 5};
};

struct i2c {
    struct scl {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin6;
        static constexpr auto af = spl::peripherals::gpio::af4;
    };
    struct sda {
        static constexpr auto port = spl::peripherals::gpio::port::b;
        static constexpr auto pin = spl::peripherals::gpio::pin7;
        static constexpr auto af = spl::peripherals::gpio::af4;
    };
    static constexpr std::uint32_t clk_mhz = 4;
};

struct spi {
    struct mosi {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin7;
        static constexpr auto af = spl::peripherals::gpio::af5;
    };
    struct miso {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin6;
        static constexpr auto af = spl::peripherals::gpio::af5;
    };
    struct sck {
        static constexpr auto port = spl::peripherals::gpio::port::a;
        static constexpr auto pin = spl::peripherals::gpio::pin5;
        static constexpr auto af = spl::peripherals::gpio::af5;
    };
};

struct max31865_select {
    static constexpr auto port = spl::peripherals::gpio::port::a;
    static constexpr auto pin = spl::peripherals::gpio::pin4;
};

struct i2c_addr {
    static constexpr auto fram = 0x50;
};

static constexpr spl::peripherals::sysclk::configuration sysclk_cfg{
    .sysclk_src = spl::peripherals::rcc::clock_source::pll,
    .sysclk_hz = 50_MHz,
    .pll_clock_src = spl::peripherals::rcc::pll_clock_source::hsi,
    .pll_input_clock_hz = 16_MHz,
    .pll_input_prescaler = spl::peripherals::rcc::pll_input_prescaler::div4,
    .pll_vco_multiplier = 50,
    .pll_output_prescaler = spl::peripherals::rcc::pll_prescaler::div4};

constexpr auto sysclock = sysclk_cfg.sysclk_hz;
constexpr auto kernel_freq = 100_Hz;
constexpr std::uint32_t watchdog_period_ms = 2000;
} // namespace hw
