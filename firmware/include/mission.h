#pragma once
#include "mission/mission_state.h"
#include "satext/tag.h"
#include "threads/gps_service.h"
#include "types.h"
#include "watchdogs.h"

namespace obc::mission {

class mission {
public:
    using gps_service = obc::threads::gps_service;
    using terminal = obc::types::terminal;
    using terminal_thread = obc::types::terminal_thread;
    using hw_watchdog = satext::tag_ptr<watchdog::thread_watchdog, struct hw_watchdog_tag>;
    using eps_watchdog = satext::tag_ptr<watchdog::thread_watchdog, struct eps_watchdog_tag>;

    mission(gps_service& gps_service,
            terminal_thread& terminal_thread,
            terminal& terminal,
            watchdogs::watchdogs& watchdogs);

    template<typename T>
    void process_event(T&& event) {
        mission_state_.process_event(std::forward<T>(event));
    }

private:
    satos::clock::time_point terminal_startup_{};
    ::mission::terminal_state state_{};
    boost::sml::sm<
        ::mission::mission_state<gps_service, terminal, terminal_thread, hw_watchdog, eps_watchdog>>
        mission_state_;
};

} // namespace obc::mission
