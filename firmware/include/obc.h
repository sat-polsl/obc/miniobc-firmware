#pragma once
#include "boot_state/boot_state.h"
#include "devices.h"
#include "drivers.h"
#include "fram_map.h"
#include "mission.h"
#include "peripherals.h"
#include "telemetry.h"
#include "types.h"
#include "watchdogs.h"

#include "satext/noncopyable.h"
#include "satext/units.h"
#include "satos/flags.h"
#include "satos/thread.h"
#include "terminal/terminal.h"
#include "terminal/threads/terminal.h"

#include "threads/can_service.h"
#include "threads/gps_service.h"
#include "threads/mission_service.h"
#include "threads/radio_service.h"

namespace obc {
class application;
using application_base = satos::thread<application, satos::thread_priority::normal_0, 2_KiB, "obc">;

class application : private satext::noncopyable, public application_base {
    friend application_base;

public:
    application() = default;
    void run();

    peripherals::peripherals& peripherals();
    drivers::drivers& drivers();
    devices::devices& devices();

    auto& terminal();

    telemetry::telemetry& telemetry();

    threads::can_service& can_service();

    watchdogs::watchdogs& watchdogs();

    boot_state::boot_state& boot_state();

private:
    [[noreturn]] void thread_function();
    void increment_boot_counter();

    satos::flags terminal_started_;

    peripherals::peripherals peripherals_{};
    drivers::drivers drivers_{peripherals_};
    devices::devices devices_{peripherals_, drivers_, can_service_};
    watchdogs::watchdogs watchdogs_{peripherals_, devices_};

    std::array<char, 128> terminal_buffer_{};
    std::array<std::string_view, 10> terminal_tokens_{};
    types::terminal terminal_{
        drivers_.terminal_uart(), terminal_buffer_, terminal_tokens_, "obc>", "OBC terminal"};
    types::terminal_thread terminal_thread_{terminal_};

    boot_state::storage boot_state_storage_{
        [this](std::size_t offset, std::span<std::byte> buffer) {
            return devices_.fram().read(fram_map::boot_counter_offset + offset, buffer).has_value();
        },
        [this](std::size_t offset, std::span<const std::byte> buffer) {
            return devices_.fram().write(fram_map::boot_counter_offset + offset, buffer);
        }};

    boot_state::boot_state boot_state_{boot_state_storage_, peripherals_.crc()};

    mission::mission mission_{gps_service_, terminal_thread_, terminal_, watchdogs_};
    telemetry::telemetry telemetry_{devices_};
    threads::can_service can_service_{drivers_, watchdogs_};
    threads::radio_service radio_service_{devices_.radio(), telemetry_.state(), watchdogs_};
    threads::gps_service gps_service_{peripherals_, devices_.gps(), telemetry_.state(), watchdogs_};
    threads::mission_service mission_service_{mission_, watchdogs_};
};

inline auto& application::terminal() { return terminal_; }

inline application obc;

} // namespace obc
