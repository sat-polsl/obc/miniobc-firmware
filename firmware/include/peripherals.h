#pragma once
#include "hw.h"
#include "satext/noncopyable.h"
#include "spl/peripherals/can/can.h"
#include "spl/peripherals/crc/crc.h"
#include "spl/peripherals/dma/dma.h"
#include "spl/peripherals/flash/flash.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/i2c/i2c.h"
#include "spl/peripherals/iwdg/iwdg.h"
#include "spl/peripherals/nvic/nvic.h"
#include "spl/peripherals/rcc/rcc.h"
#include "spl/peripherals/spi/spi.h"
#include "spl/peripherals/sysclk/sysclk.h"
#include "spl/peripherals/uart/uart.h"

namespace obc::peripherals {
class peripherals : private satext::noncopyable {
public:
    enum class uart { terminal, gps };

    peripherals() = default;

    void initialize();

    spl::peripherals::rcc::rcc& rcc();
    spl::peripherals::nvic::nvic& nvic();
    spl::peripherals::gpio::gpio& led();
    spl::peripherals::gpio::gpio& radio_reset();
    spl::peripherals::uart::uart& terminal_uart();
    spl::peripherals::uart::uart& gps_uart();
    spl::peripherals::uart::uart& radio_uart();
    spl::peripherals::can::can& can();
    spl::peripherals::i2c::i2c& i2c();
    spl::peripherals::dma::dma& dma1();
    spl::peripherals::spi::spi& spi();
    spl::peripherals::gpio::gpio& max31865_select();
    spl::peripherals::flash::flash& flash();
    spl::peripherals::iwdg::iwdg& iwdg();
    spl::peripherals::crc::crc& crc();

    void initialize_uart(uart selected_uart) const;

private:
    spl::peripherals::gpio::gpio led_{hw::led::port, hw::led::pin};

    spl::peripherals::gpio::gpio terminal_uart_tx_gpio_{hw::terminal_uart::tx::port,
                                                        hw::terminal_uart::tx::pin};
    spl::peripherals::gpio::gpio terminal_uart_rx_gpio_{hw::terminal_uart::rx::port,
                                                        hw::terminal_uart::rx::pin};
    spl::peripherals::uart::uart terminal_uart_{spl::peripherals::uart::instance::uart2};

    spl::peripherals::gpio::gpio gps_uart_tx_gpio_{hw::gps_uart::tx::port, hw::gps_uart::tx::pin};
    spl::peripherals::gpio::gpio gps_uart_rx_gpio_{hw::gps_uart::rx::port, hw::gps_uart::rx::pin};
    spl::peripherals::uart::uart& gps_uart_{terminal_uart_};

    spl::peripherals::gpio::gpio can_tx_gpio_{hw::can::tx::port, hw::can::tx::pin};
    spl::peripherals::gpio::gpio can_rx_gpio_{hw::can::rx::port, hw::can::rx::pin};
    spl::peripherals::can::can can_{spl::peripherals::can::instance::can1};

    spl::peripherals::gpio::gpio i2c_sda_gpio_{hw::i2c::sda::port, hw::i2c::sda::pin};
    spl::peripherals::gpio::gpio i2c_scl_gpio_{hw::i2c::scl::port, hw::i2c::scl::pin};
    spl::peripherals::i2c::i2c i2c_{spl::peripherals::i2c::instance::i2c1};

    spl::peripherals::gpio::gpio spi_mosi_{hw::spi::mosi::port, hw::spi::mosi::pin};
    spl::peripherals::gpio::gpio spi_miso_{hw::spi::miso::port, hw::spi::miso::pin};
    spl::peripherals::gpio::gpio spi_sck_{hw::spi::sck::port, hw::spi::sck::pin};
    spl::peripherals::spi::spi spi_{spl::peripherals::spi::instance::spi1};
    spl::peripherals::gpio::gpio max31865_select_{hw::max31865_select::port,
                                                  hw::max31865_select::pin};

    spl::peripherals::gpio::gpio radio_reset_{hw::radio::reset::port, hw::radio::reset::pin};
    spl::peripherals::gpio::gpio radio_tx_gpio_{hw::radio::tx::port, hw::radio::tx::pin};
    spl::peripherals::gpio::gpio radio_rx_gpio_{hw::radio::rx::port, hw::radio::rx::pin};
    spl::peripherals::uart::uart radio_uart_{spl::peripherals::uart::instance::uart1};

    spl::peripherals::dma::dma dma1_{spl::peripherals::dma::instance::dma1};

    spl::peripherals::crc::crc crc_{};

    [[no_unique_address]] spl::peripherals::rcc::rcc rcc_;
    [[no_unique_address]] spl::peripherals::nvic::nvic nvic_;
    [[no_unique_address]] spl::peripherals::flash::flash flash_;
    [[no_unique_address]] spl::peripherals::iwdg::iwdg iwdg_;

    spl::peripherals::sysclk::sysclk<decltype(rcc_), decltype(flash_)> sysclk_{rcc_, flash_};
};
} // namespace obc::peripherals
