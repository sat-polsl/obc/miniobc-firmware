#pragma once
#include <cstdint>
#include "devices.h"
#include "satext/units.h"
#include "satos/mutex.h"
#include "telemetry/telemetry_service.h"
#include "telemetry/telemetry_state.h"
#include "telemetry/types.h"

#include "telemetry/samplers/eps_sampler.h"
#include "telemetry/samplers/muons_sampler.h"
#include "telemetry/samplers/pressure_sampler.h"
#include "telemetry/samplers/temperature_sampler.h"
#include "telemetry/samplers/uptime_sampler.h"

namespace obc::telemetry {

struct id {
    static constexpr ::telemetry::telemetry_id boot_counter{0};
    static constexpr ::telemetry::telemetry_id uptime{1};
    static constexpr ::telemetry::telemetry_id pressure{2};
    static constexpr ::telemetry::telemetry_id utc{3};
    static constexpr ::telemetry::telemetry_id latitude{4};
    static constexpr ::telemetry::telemetry_id longitude{5};
    static constexpr ::telemetry::telemetry_id altitude{6};
    static constexpr ::telemetry::telemetry_id temperature{7};
    static constexpr ::telemetry::telemetry_id muons{8};
    static constexpr ::telemetry::telemetry_id gps_mode{9};

    static constexpr ::telemetry::telemetry_id eps_boot_counter{20};
    static constexpr ::telemetry::telemetry_id eps_uptime{21};
    static constexpr ::telemetry::telemetry_id eps_temperature_3v3{22};
    static constexpr ::telemetry::telemetry_id eps_temperature_5v{23};
    static constexpr ::telemetry::telemetry_id eps_temperature_12v{24};
    static constexpr ::telemetry::telemetry_id eps_temperature_cell1{25};
    static constexpr ::telemetry::telemetry_id eps_temperature_cell2{26};
    static constexpr ::telemetry::telemetry_id eps_temperature_cell3{27};
    static constexpr ::telemetry::telemetry_id eps_v3v3_l2{28};
    static constexpr ::telemetry::telemetry_id eps_i3v3_l2{29};
    static constexpr ::telemetry::telemetry_id eps_v5v_l1{30};
    static constexpr ::telemetry::telemetry_id eps_i5v_l1{31};
    static constexpr ::telemetry::telemetry_id eps_vbms{32};
    static constexpr ::telemetry::telemetry_id eps_ibms{33};
};

namespace samplers = ::telemetry::samplers;

class telemetry {
public:
    explicit telemetry(devices::devices& devices);
    void initialize();
    void start();

    ::telemetry::telemetry_state& state();

private:
    using telemetry_state = ::telemetry::telemetry_state;

    devices::devices& devices_;

    [[no_unique_address]] samplers::uptime_sampler<telemetry_state, id::uptime> uptime_sampler_{};

    samplers::pressure_sampler<decltype(devices_.mprls()), telemetry_state, id::pressure>
        pressure_sampler_{devices_.mprls()};

    samplers::temperature_sampler<decltype(devices_.max31865()), telemetry_state, id::temperature>
        temperature_sampler_{devices_.max31865()};

    samplers::muons_sampler<decltype(devices_.muons()), telemetry_state, id::muons> muons_sampler_{
        devices_.muons()};

    static constexpr auto eps_sampler_config =
        samplers::eps_sampler_config{.boot_counter = id::eps_boot_counter,
                                     .uptime = id::eps_uptime,
                                     .temperature_3v3 = id::eps_temperature_3v3,
                                     .temperature_5v = id::eps_temperature_5v,
                                     .temperature_12v = id::eps_temperature_12v,
                                     .temperature_cell1 = id::eps_temperature_cell1,
                                     .temperature_cell2 = id::eps_temperature_cell2,
                                     .temperature_cell3 = id::eps_temperature_cell3,
                                     .v3v3v_l2 = id::eps_v3v3_l2,
                                     .i3v3v_l2 = id::eps_i3v3_l2,
                                     .v5v_l1 = id::eps_v5v_l1,
                                     .i5v_l1 = id::eps_i5v_l1,
                                     .vbms = id::eps_vbms,
                                     .ibms = id::eps_ibms};

    samplers::eps_sampler<decltype(devices_.eps()), telemetry_state, eps_sampler_config>
        eps_sampler_{devices_.eps()};

    ::telemetry::telemetry_value_buffer<40> telemetry_value_buffer_{};
    telemetry_state state_{telemetry_value_buffer_, {}};

    ::telemetry::telemetry_service<2_KiB,
                                   telemetry_state,
                                   decltype(uptime_sampler_),
                                   decltype(pressure_sampler_),
                                   decltype(temperature_sampler_),
                                   decltype(muons_sampler_),
                                   decltype(eps_sampler_)>
        telemetry_service_{state_};
};

} // namespace obc::telemetry
