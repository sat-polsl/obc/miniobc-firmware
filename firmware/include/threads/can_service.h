#pragma once
#include "communication/can/can_context.h"
#include "drivers.h"
#include "satext/units.h"
#include "satos/mutex.h"
#include "satos/thread.h"
#include "spl/drivers/can/can.h"
#include "types.h"
#include "watchdogs.h"

namespace obc::threads {

class can_service;
using can_service_base = satos::thread<can_service, satos::thread_priority::normal_0, 2_KiB, "can">;

class can_service : public can_service_base {
    friend can_service_base;

public:
    using can_context = obc::types::can_context;

    can_service(drivers::drivers& drivers, watchdogs::watchdogs& watchdogs);

    can_context& context();

private:
    [[noreturn]] void thread_function();

    can_context can_context_;
    watchdogs::watchdogs& watchdogs_;
};

} // namespace obc::threads
