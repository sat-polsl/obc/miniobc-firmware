#pragma once
#include <array>
#include "devices.h"
#include "peripherals.h"
#include "satext/units.h"
#include "satos/thread.h"
#include "telemetry.h"
#include "watchdogs.h"

namespace obc::threads {

class gps_service;
using gps_service_base = satos::thread<gps_service, satos::thread_priority::normal_0, 2_KiB, "gps">;

class gps_service : public gps_service_base {
    friend gps_service_base;

public:
    gps_service(peripherals::peripherals& peripherals,
                devices::devices::gps_device& gps,
                ::telemetry::telemetry_state& state,
                watchdogs::watchdogs& watchdogs);

private:
    [[noreturn]] void thread_function();

    peripherals::peripherals& peripherals_;
    devices::devices::gps_device& gps_;
    ::telemetry::telemetry_state& state_;
    watchdogs::watchdogs& watchdogs_;
};

} // namespace obc::threads
