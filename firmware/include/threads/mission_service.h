#pragma once
#include <array>
#include "mission.h"
#include "mission/mission_state.h"
#include "satext/units.h"
#include "satos/thread.h"
#include "watchdogs.h"

namespace obc::threads {

class mission_service;

using mission_service_base =
    satos::thread<mission_service, satos::thread_priority::normal_0, 8_KiB, "mission">;

class mission_service : public mission_service_base {
    friend mission_service_base;

public:
    mission_service(obc::mission::mission& mission, watchdogs::watchdogs& watchdogs);

private:
    [[noreturn]] void thread_function();

    obc::mission::mission& mission_;
    watchdogs::watchdogs& watchdogs_;
};

} // namespace obc::threads
