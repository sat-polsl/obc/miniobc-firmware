#pragma once
#include "devices.h"
#include "satext/units.h"
#include "satos/thread.h"
#include "telemetry.h"
#include "watchdogs.h"

namespace obc::threads {

class radio_service;
using radio_service_base =
    satos::thread<radio_service, satos::thread_priority::above_normal_0, 2_KiB, "radio">;

class radio_service : public radio_service_base {
    friend radio_service_base;

public:
    radio_service(devices::devices::radio_device& radio,
                  ::telemetry::telemetry_state& telemetry_state,
                  watchdogs::watchdogs& watchdogs);

private:
    [[noreturn]] void thread_function();
    void reset_radio();
    std::size_t encode_beacon_message();
    void handle_transmit_error();

    static constexpr std::uint8_t error_limit = 3;
    static constexpr std::uint32_t frequency = 434'700'000;
    static constexpr auto spreading_factor = ::devices::radio::spreading_factor::sf10;
    static constexpr auto beacon_interval = satos::chrono::seconds{30};
    using beacon_buffer = std::array<std::byte, 512>;

    static constexpr std::array<::telemetry::telemetry_id, 22> telemetry_ids = {
        telemetry::id::boot_counter,
        telemetry::id::uptime,
        telemetry::id::pressure,
        telemetry::id::latitude,
        telemetry::id::longitude,
        telemetry::id::altitude,
        telemetry::id::temperature,
        telemetry::id::muons,
        telemetry::id::eps_boot_counter,
        telemetry::id::eps_uptime,
        telemetry::id::eps_temperature_3v3,
        telemetry::id::eps_temperature_5v,
        telemetry::id::eps_temperature_12v,
        telemetry::id::eps_temperature_cell1,
        telemetry::id::eps_temperature_cell2,
        telemetry::id::eps_temperature_cell3,
        telemetry::id::eps_v3v3_l2,
        telemetry::id::eps_i3v3_l2,
        telemetry::id::eps_v5v_l1,
        telemetry::id::eps_i5v_l1,
        telemetry::id::eps_vbms,
        telemetry::id::eps_ibms};

    devices::devices::radio_device& radio_;
    ::telemetry::telemetry_state& telemetry_state_;
    std::uint8_t tx_counter_{};
    beacon_buffer beacon_buffer_{};
    std::uint8_t error_counter{};
    watchdogs::watchdogs& watchdogs_;
};

} // namespace obc::threads
