#pragma once
#include "communication/can/can_context.h"
#include "spl/drivers/can/can.h"
#include "spl/drivers/uart/uart.h"
#include "terminal/terminal.h"
#include "terminal/threads/terminal.h"
#include "watchdog/thread_watchdog/thread_watchdog.h"

namespace obc::types {
using can_context = communication::can::can_context<spl::drivers::can::can1, 8>;
using terminal_uart = spl::drivers::uart::uart2;
using terminal = ::terminal::terminal<terminal_uart>;
using terminal_thread = ::terminal::threads::terminal<terminal_uart>;
} // namespace obc::types
