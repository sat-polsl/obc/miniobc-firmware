#pragma once
#include "devices.h"
#include "peripherals.h"
#include "satos/timer.h"
#include "types.h"
#include "watchdog/thread_watchdog/thread_watchdog.h"

namespace obc::watchdogs {

class watchdogs {
public:
    explicit watchdogs(peripherals::peripherals& peripherals, devices::devices& devices);

    void initialize();

    watchdog::thread_watchdog& hw();

    watchdog::thread_watchdog& eps();

    void feed();

    void stop();

private:
    watchdog::watchdog_buffer<4> hw_watchdog_buffer_{};
    watchdog::thread_watchdog hw_watchdog_;
    satos::timer<1> hw_watchdog_timer_{satos::chrono::seconds{1}};

    watchdog::watchdog_buffer<4> eps_watchdog_buffer_{};
    watchdog::thread_watchdog eps_watchdog_;
    satos::timer<1> eps_watchdog_timer_{satos::chrono::seconds{10}};
};

} // namespace obc::watchdogs
