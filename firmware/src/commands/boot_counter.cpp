#include "obc.h"
#include "terminal/terminal.h"

const terminal::command boot_counter("boot_counter", [](std::span<std::string_view>) {
    auto boot_counter =
        obc::obc.telemetry().state().read<std::uint32_t>(obc::telemetry::id::boot_counter);
    if (boot_counter.has_value()) {
        obc::obc.terminal().print("boot counter: {}\n", *boot_counter);
    } else {
        obc::obc.terminal().print("error\n");
    }
});

const terminal::command reset_boot_state("reset_boot_state", [](std::span<std::string_view>) {
    obc::obc.boot_state().set({.boot_counter = 0});
});
