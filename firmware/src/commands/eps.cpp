#include "can_id.h"
#include "devices/eps/enums.h"
#include "obc.h"
#include "satext/charconv.h"
#include "satext/constexpr_map.h"
#include "satext/type_traits.h"
#include "satos/chrono.h"
#include "satos/semaphore.h"
#include "telemetry/formatters.h"
#include "terminal/terminal.h"

#include <ranges>

using namespace std::literals::string_view_literals;
using namespace satos::chrono_literals;
using namespace obc::can::id;
using namespace devices::eps;

static constexpr satext::constexpr_map<std::string_view, power_output, 7> output_lut{
    {"3v1"sv, power_output::v3v3_l1},
    {"3v2"sv, power_output::v3v3_l2},
    {"3v3"sv, power_output::v3v3_l3},
    {"3v4"sv, power_output::v3v3_l4},
    {"5v1"sv, power_output::v5_l1},
    {"5v2"sv, power_output::v5_l2},
    {"12v"sv, power_output::v12}};

void print_outputs(decltype(obc::obc.terminal())& terminal) {
    auto names = std::views::keys(output_lut);
    terminal.print("{}\n", fmt::join(names, ", "));
}

const terminal::command eps_echo("eps_echo", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& context = obc::obc.can_service().context();
    if (args.size() != 1) {
        terminal.print("usage: eps_echo <data>\n");
        return;
    }

    spl::peripherals::can::message request{};
    spl::peripherals::can::message response{};
    communication::can::status status{};
    satos::binary_semaphore semaphore{};

    if (auto [_, err] = satext::unhexify(args[0], request.data); err == std::errc{}) {
        request.size = args[0].size() / 2;
        request.id = echo::request;
        request.is_extended = true;
        context.send(request,
                     echo::response,
                     [&semaphore, &response, &status](auto transaction_status,
                                                      const auto& transaction_response) {
                         response = transaction_response;
                         status = transaction_status;
                         semaphore.release();
                     });

        if (semaphore.try_acquire_for(5_s)) {
            terminal.print("Status: {}\n", satext::to_underlying_type(status));
            terminal.print("Size: {}\n", response.size);
            terminal.print("Id: {:#x}\n", response.id);
            terminal.print("Data: {:#04x}\n",
                           fmt::join(std::span(response.data.data(), response.size), ", "));
        } else {
            terminal.print("Semaphore timeout\n");
        }
    }
});

const terminal::command eps_power_on("eps_power_on", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& eps = obc::obc.devices().eps();
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        terminal.print("usage: eps_power_on <output>, where <output>:\n");
        print_outputs(terminal);
        return;
    }

    auto status = eps.power_output_on(output_lut[args[0]]);
    terminal.print("Status: {}\n", satext::to_underlying_type(status));
});

const terminal::command eps_power_off("eps_power_off", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& eps = obc::obc.devices().eps();
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        terminal.print("usage: eps_power_off <output>, where <output>:\n");
        print_outputs(terminal);
        return;
    }

    auto status = eps.power_output_off(output_lut[args[0]]);
    terminal.print("Status: {}\n", satext::to_underlying_type(status));
});

const terminal::command eps_enable_wdt("eps_enable_wdt", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& eps = obc::obc.devices().eps();
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        terminal.print("usage: eps_enable_wdt <output>, where <output>:\n");
        print_outputs(terminal);
        return;
    }

    auto status = eps.enable_watchdog(output_lut[args[0]]);
    terminal.print("Status: {}\n", satext::to_underlying_type(status));
});

const terminal::command eps_disable_wdt("eps_disable_wdt", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& eps = obc::obc.devices().eps();
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        terminal.print("usage: eps_disable_wdt <output>, where <output>:\n");
        print_outputs(terminal);
        return;
    }

    auto status = eps.disable_watchdog(output_lut[args[0]]);
    terminal.print("Status: {}\n", satext::to_underlying_type(status));
});

const terminal::command eps_kick_wdt("eps_kick_wdt", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& eps = obc::obc.devices().eps();
    if (args.size() != 1 || !output_lut.contains(args[0])) {
        terminal.print("usage: eps_kick_wdt <output>, where <output>:\n");
        print_outputs(terminal);
        return;
    }

    auto status = eps.kick_watchdog(output_lut[args[0]]);
    terminal.print("Status: {}\n", satext::to_underlying_type(status));
});

const terminal::command
    eps_set_wdt_timeout("eps_set_wdt_timeout", [](std::span<std::string_view> args) {
        auto& terminal = obc::obc.terminal();
        auto& eps = obc::obc.devices().eps();
        if (args.size() != 2 || !output_lut.contains(args[0])) {
            terminal.print("usage: eps_set_wdt_timeout <output> <timeout_s>, where <output>:\n");
            print_outputs(terminal);
            return;
        }

        std::uint32_t timeout{};
        if (auto [_, e] = satext::from_chars(args[1], timeout); e != std::errc{}) {
            terminal.print("usage: eps_set_wdt_timeout <output> <timeout_s>, where <output>:\n");
            print_outputs(terminal);
            return;
        }

        auto status =
            eps.set_watchdog_timeout(output_lut[args[0]], satos::chrono::seconds(timeout));
        terminal.print("Status: {}\n", satext::to_underlying_type(status));
    });

const terminal::command eps_tlm("eps_tlm", [](std::span<std::string_view> args) {
    auto& terminal = obc::obc.terminal();
    auto& eps = obc::obc.devices().eps();
    if (args.size() != 1) {
        terminal.print("usage: eps_tlm <tlm_id>\n");
        return;
    }

    std::uint16_t id{};
    if (auto [_, e] = satext::from_chars(args[0], id); e != std::errc{}) {
        terminal.print("usage: eps_tlm <tlm_id>\n");
        return;
    }

    eps.get_telemetry(::devices::eps::telemetry_id{id})
        .map_error([&terminal](auto status) {
            terminal.print("Error: {}\n", satext::to_underlying_type(status));
        })
        .map([&terminal](auto value) { terminal.print("Result: {}\n", value); });
});
