#include "obc.h"
#include "satext/type_traits.h"
#include "terminal/terminal.h"

const terminal::command mprls("read_pressure", [](std::span<std::string_view>) {
    auto& mprls = obc::obc.devices().mprls();

    mprls.read_pressure()
        .map([](std::uint32_t val) { obc::obc.terminal().print("pressure: {} Pa\n", val); })
        .map_error([](auto status) {
            obc::obc.terminal().print("status: {}\n", satext::to_underlying_type(status));
        });
});
