#include "obc.h"
#include "satext/type_traits.h"
#include "terminal/terminal.h"

const terminal::command temperature("temperature", [](std::span<std::string_view>) {
    obc::obc.devices()
        .max31865()
        .read_temperature()
        .map([](float result) {
            obc::obc.terminal().print("Temperature: {}\n",
                                      static_cast<std::int32_t>(result * 10.0f));
        })
        .map_error([](auto error) {
            obc::obc.terminal().print("Error: {}\n", satext::to_underlying_type(error));
        });
});
