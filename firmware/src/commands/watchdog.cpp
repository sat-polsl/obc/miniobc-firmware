#include "obc.h"
#include "terminal/terminal.h"

const terminal::command stop_wdg("stop_wdg",
                                 [](std::span<std::string_view>) { obc::obc.watchdogs().stop(); });
