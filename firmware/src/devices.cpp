#include "devices.h"
#include "hw.h"
#include "satos/chrono.h"
#include "threads/can_service.h"

using namespace satos::chrono_literals;

namespace obc::devices {
devices::devices(obc::peripherals::peripherals& peripherals,
                 obc::drivers::drivers& drivers,
                 threads::can_service& can_service) :
    fram_{drivers.i2c(), hw::i2c_addr::fram, 5_s},
    radio_{drivers.radio_uart(), peripherals.radio_reset()},
    mprls_{drivers.i2c(), 1_s},
    eps_{can_service.context(), 200_ms},
    gps_buffer_{},
    gps_{drivers.gps_uart(), gps_buffer_},
    max31865_{drivers.spi(), peripherals.max31865_select(), 1_s},
    muons_{drivers.i2c(), 1_s} {}

void devices::initialize() { max31865_.initialize(); }

devices::fram_device& devices::fram() { return fram_; }

devices::radio_device& devices::radio() { return radio_; }

devices::mprls_device& devices::mprls() { return mprls_; }

devices::eps_device& devices::eps() { return eps_; }

devices::gps_device& devices::gps() { return gps_; }

devices::max31865_device& devices::max31865() { return max31865_; }

devices::muons_device& devices::muons() { return muons_; }

} // namespace obc::devices
