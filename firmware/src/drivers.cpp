#include "drivers.h"

namespace obc::drivers {
drivers::drivers(obc::peripherals::peripherals& peripherals) :
    dma1_(peripherals.dma1(), peripherals.nvic()),
    terminal_uart_{peripherals.terminal_uart(), peripherals.nvic()},
    gps_uart_{terminal_uart_},
    radio_uart_{peripherals.radio_uart(), peripherals.nvic()},
    can_{peripherals.can(), peripherals.nvic()},
    i2c_{peripherals.i2c(), dma1_},
    spi_{peripherals.spi(), dma1_} {}

void drivers::initialize() { spi_.initialize(); }

void drivers::initialize_rtos_context() { dma1_.initialize(); }

spl::drivers::uart::uart2& drivers::terminal_uart() { return terminal_uart_; }

spl::drivers::can::can1& drivers::can() { return can_; }

spl::drivers::i2c::i2c_dma_vectored::i2c1& drivers::i2c() { return i2c_; }

spl::drivers::dma::dma& drivers::dma1() { return dma1_; }

spl::drivers::uart::uart1& drivers::radio_uart() { return radio_uart_; }

spl::drivers::uart::uart2& drivers::gps_uart() { return gps_uart_; }

spl::drivers::spi::spi_dma_vectored::spi1& drivers::spi() { return spi_; }

} // namespace obc::drivers
