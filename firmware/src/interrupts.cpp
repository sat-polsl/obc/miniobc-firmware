#include "obc.h"

extern "C" void usart1_isr() { obc::obc.drivers().radio_uart().isr_handler(); }

extern "C" void usart2_isr() { obc::obc.drivers().terminal_uart().isr_handler(); }

extern "C" void can1_tx_isr() { obc::obc.drivers().can().tx_isr_handler(); }

extern "C" void can1_rx0_isr() { obc::obc.drivers().can().rx0_isr_handler(); }

extern "C" void can1_rx1_isr() { obc::obc.drivers().can().rx1_isr_handler(); }

extern "C" void dma1_channel2_isr() {
    obc::obc.drivers().dma1().isr_handler(spl::peripherals::nvic::irq::dma1_channel2,
                                          spl::peripherals::dma::channel2);
}

extern "C" void dma1_channel3_isr() {
    obc::obc.drivers().dma1().isr_handler(spl::peripherals::nvic::irq::dma1_channel3,
                                          spl::peripherals::dma::channel3);
}

extern "C" void dma1_channel6_isr() {
    obc::obc.drivers().dma1().isr_handler(spl::peripherals::nvic::irq::dma1_channel6,
                                          spl::peripherals::dma::channel6);
}

extern "C" void dma1_channel7_isr() {
    obc::obc.drivers().dma1().isr_handler(spl::peripherals::nvic::irq::dma1_channel7,
                                          spl::peripherals::dma::channel7);
}
