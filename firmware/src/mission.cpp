#include "mission.h"

namespace obc::mission {

mission::mission(gps_service& gps_service,
                 terminal_thread& terminal_thread,
                 terminal& terminal,
                 watchdogs::watchdogs& watchdogs) :
    mission_state_{terminal,
                   state_,
                   terminal_startup_,
                   terminal_thread,
                   gps_service,
                   hw_watchdog{watchdogs.hw()},
                   eps_watchdog{watchdogs.eps()}} {}

} // namespace obc::mission
