#include "obc.h"
#include "enums.h"
#include "fram_map.h"
#include "hw.h"
#include "satext/type_traits.h"
#include "satos/kernel.h"

using namespace satos::chrono_literals;
using namespace satext::struct_literals;

namespace obc {
void application::run() {
    peripherals_.initialize();
    drivers_.initialize();
    telemetry_.initialize();

    satos::kernel::initialize(hw::sysclock, hw::kernel_freq);
    satos::kernel::set_idle_handler([]() { asm volatile("wfi"); });

    this->start();

    satos::kernel::start();
}

void application::thread_function() {
    drivers_.initialize_rtos_context();
    devices_.initialize();
    watchdogs_.initialize();
    boot_state_.initialize();

    increment_boot_counter();

    telemetry_.start();
    can_service_.start();
    radio_service_.start();
    mission_service_.start();

    watchdogs_.hw().register_thread(1_s, can_service_.native_handle());
    watchdogs_.hw().register_thread(75_s, radio_service_.native_handle());
    watchdogs_.hw().register_thread(5_s, mission_service_.native_handle());
    watchdogs_.eps().register_thread(1_s, can_service_.native_handle());
    watchdogs_.eps().register_thread(75_s, radio_service_.native_handle());
    watchdogs_.eps().register_thread(5_s, mission_service_.native_handle());

    for (;;) {
        peripherals_.led().toggle();
        satos::this_thread::sleep_for(1_s);
    }
}

void application::increment_boot_counter() {
    auto boot_counter = boot_state_.get_boot_counter();
    boot_counter++;
    telemetry_.state().write(telemetry::id::boot_counter, boot_counter);
    boot_state_.set_boot_counter(boot_counter);
}

peripherals::peripherals& application::peripherals() { return peripherals_; }

drivers::drivers& application::drivers() { return drivers_; }

devices::devices& application::devices() { return devices_; }

telemetry::telemetry& application::telemetry() { return telemetry_; }

threads::can_service& application::can_service() { return can_service_; }

watchdogs::watchdogs& application::watchdogs() { return watchdogs_; }

boot_state::boot_state& application::boot_state() { return boot_state_; }

} // namespace obc
