#include "peripherals.h"
#include "hw.h"

using namespace spl::peripherals;

namespace obc::peripherals {
void peripherals::initialize() {
    sysclk_.setup<hw::sysclk_cfg>();

    rcc_.enable_clock(rcc::peripheral::gpiob)
        .enable_clock(rcc::peripheral::gpioa)
        .enable_clock(rcc::peripheral::gpioc)
        .enable_clock(rcc::peripheral::uart1)
        .enable_clock(rcc::peripheral::uart2)
        .enable_clock(rcc::peripheral::i2c1)
        .enable_clock(rcc::peripheral::dma1)
        .enable_clock(rcc::peripheral::can1)
        .enable_clock(rcc::peripheral::spi1)
        .enable_clock(rcc::peripheral::crc);

    led_.setup(gpio::mode::output, gpio::pupd::pullup);

    crc_.reset();
    crc_.set_reverse_input(true);
    crc_.set_reverse_output(true);
    crc_.set_final_xor_value(0xffffffff);

    radio_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::radio::tx::af);
    radio_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::radio::rx::af);
    radio_uart_.set_baudrate(hw::radio::baudrate).set_mode(::uart::mode::rx_tx).enable();

    initialize_uart(uart::terminal);

    can_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af9);
    can_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, gpio::af9);
    can_.initialize(hw::can::timings, can::options{});
    can_.setup_filter(can::filter0, 0x0, 0x0, can::rx_fifo0, true);

    radio_reset_.clear();
    radio_reset_.setup(gpio::mode::output, gpio::pupd::pullup);

    i2c_scl_gpio_.setup(gpio::mode::af, gpio::pupd::none, hw::i2c::scl::af);
    i2c_scl_gpio_.set_output_type(gpio::output_type::open_drain);
    i2c_sda_gpio_.setup(gpio::mode::af, gpio::pupd::none, hw::i2c::sda::af);
    i2c_sda_gpio_.set_output_type(gpio::output_type::open_drain);
    i2c_.set_speed(spl::peripherals::i2c::speed::i2c_100k, hw::i2c::clk_mhz);

    spi_.set_baudrate_prescaler(spi::baudrate_prescaler::div16)
        .set_clock_mode(spi::clock_mode::mode3);
    spi_mosi_.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi::mosi::af);
    spi_miso_.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi::miso::af);
    spi_sck_.setup(gpio::mode::af, gpio::pupd::pullup, hw::spi::sck::af);
    max31865_select_.set();
    max31865_select_.setup(gpio::mode::output, gpio::pupd::pullup);

    nvic_.set_priority(nvic::irq::uart1, 5).enable_irq(nvic::irq::uart1);
    nvic_.set_priority(nvic::irq::uart2, 5).enable_irq(nvic::irq::uart2);
    nvic_.set_priority(nvic::irq::can1_tx, 5).enable_irq(nvic::irq::can1_tx);
    nvic_.set_priority(nvic::irq::can1_rx0, 5).enable_irq(nvic::irq::can1_rx0);
    nvic_.set_priority(nvic::irq::i2c1_ev, 5).enable_irq(nvic::irq::i2c1_ev);
    nvic_.set_priority(nvic::irq::dma1_channel2, 5).enable_irq(nvic::irq::dma1_channel2);
    nvic_.set_priority(nvic::irq::dma1_channel3, 5).enable_irq(nvic::irq::dma1_channel3);
    nvic_.set_priority(nvic::irq::dma1_channel6, 5).enable_irq(nvic::irq::dma1_channel6);
    nvic_.set_priority(nvic::irq::dma1_channel7, 5).enable_irq(nvic::irq::dma1_channel7);

    iwdg_.initialize();
    iwdg_.set_period_ms(hw::watchdog_period_ms);
}

void peripherals::initialize_uart(peripherals::uart selected_uart) const {
    if (selected_uart == uart::terminal) {
        gps_uart_.disable();
        gps_uart_tx_gpio_.setup(gpio::mode::input, gpio::pupd::none);
        gps_uart_rx_gpio_.setup(gpio::mode::input, gpio::pupd::none);

        terminal_uart_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::terminal_uart::tx::af);
        terminal_uart_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::terminal_uart::rx::af);
        terminal_uart_.set_baudrate(hw::terminal_uart::baudrate)
            .set_mode(::uart::mode::rx_tx)
            .enable();
    } else if (selected_uart == uart::gps) {
        terminal_uart_.disable();
        terminal_uart_tx_gpio_.setup(gpio::mode::input, gpio::pupd::none);
        terminal_uart_rx_gpio_.setup(gpio::mode::input, gpio::pupd::none);

        gps_uart_tx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::gps_uart::tx::af);
        gps_uart_rx_gpio_.setup(gpio::mode::af, gpio::pupd::pullup, hw::gps_uart::rx::af);
        gps_uart_.set_baudrate(hw::gps_uart::baudrate).set_mode(::uart::mode::rx_tx).enable();
    }
}

spl::peripherals::gpio::gpio& peripherals::led() { return led_; }

spl::peripherals::uart::uart& peripherals::terminal_uart() { return terminal_uart_; }

spl::peripherals::rcc::rcc& peripherals::rcc() { return rcc_; }

spl::peripherals::nvic::nvic& peripherals::nvic() { return nvic_; }

spl::peripherals::can::can& peripherals::can() { return can_; }

spl::peripherals::gpio::gpio& peripherals::radio_reset() { return radio_reset_; }

spl::peripherals::i2c::i2c& peripherals::i2c() { return i2c_; }

spl::peripherals::dma::dma& peripherals::dma1() { return dma1_; }

spl::peripherals::flash::flash& peripherals::flash() { return flash_; }

spl::peripherals::uart::uart& peripherals::radio_uart() { return radio_uart_; }

spl::peripherals::uart::uart& peripherals::gps_uart() { return gps_uart_; }

spl::peripherals::spi::spi& peripherals::spi() { return spi_; }

spl::peripherals::gpio::gpio& peripherals::max31865_select() { return max31865_select_; }

spl::peripherals::iwdg::iwdg& peripherals::iwdg() { return iwdg_; }

spl::peripherals::crc::crc& peripherals::crc() { return crc_; }

} // namespace obc::peripherals
