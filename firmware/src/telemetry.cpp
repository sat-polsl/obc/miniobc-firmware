#include "telemetry.h"
#include "satos/chrono.h"

using namespace satos::chrono_literals;

namespace obc::telemetry {

telemetry::telemetry(devices::devices& devices) : devices_{devices} {}

void telemetry::initialize() {
    telemetry_service_.add_sampler(uptime_sampler_, 1_s);
    telemetry_service_.add_sampler(pressure_sampler_, 15_s);
    telemetry_service_.add_sampler(temperature_sampler_, 15_s);
    telemetry_service_.add_sampler(muons_sampler_, 30_s);
    telemetry_service_.add_sampler(eps_sampler_, 10_s);
}

void telemetry::start() { telemetry_service_.start(); }

::telemetry::telemetry_state& telemetry::state() { return state_; }

} // namespace obc::telemetry
