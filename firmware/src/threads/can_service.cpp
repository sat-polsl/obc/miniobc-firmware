#include "threads/can_service.h"

namespace obc::threads {

can_service::can_service(drivers::drivers& drivers, watchdogs::watchdogs& watchdogs) :
    can_context_{drivers.can()},
    watchdogs_{watchdogs} {}

can_service::can_context& can_service::context() { return can_context_; }

void can_service::thread_function() {
    for (;;) {
        can_context_.run_one();
        watchdogs_.feed();
    }
}

} // namespace obc::threads
