#include "threads/gps_service.h"
#include "nmea/types.h"
#include "satext/overload.h"
#include "satext/type_traits.h"

using namespace satos::chrono_literals;

namespace obc::threads {

gps_service::gps_service(peripherals::peripherals& peripherals,
                         devices::devices::gps_device& gps,
                         ::telemetry::telemetry_state& state,
                         watchdogs::watchdogs& watchdogs) :
    peripherals_{peripherals},
    gps_{gps},
    state_{state},
    watchdogs_{watchdogs} {}

void gps_service::thread_function() {
    using messages = nmea::messages<nmea::floating_policy::floating>;
    using namespace ::telemetry;

    peripherals_.initialize_uart(peripherals::peripherals::uart::gps);

    for (;;) {
        gps_.read_nmea().map([this](auto nmea_message) {
            std::visit(satext::overload(
                           [this](const messages::gpgga& gpgga) {
                               state_.write(telemetry::id::utc, gpgga.timestamp);
                               state_.write(telemetry::id::latitude, gpgga.latitude);
                               state_.write(telemetry::id::longitude, gpgga.longitude);
                               state_.write(telemetry::id::altitude, gpgga.altitude);
                           },
                           [this](const messages::gpgsa& gpgsa) {
                               state_.write(telemetry::id::gps_mode,
                                            static_cast<std::uint8_t>(gpgsa.fix));
                           }),
                       nmea_message);
        });
        watchdogs_.feed();
    }
}

} // namespace obc::threads
