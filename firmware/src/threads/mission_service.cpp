#include "threads/mission_service.h"

using namespace satos::chrono_literals;

namespace obc::threads {

mission_service::mission_service(obc::mission::mission& mission, watchdogs::watchdogs& watchdogs) :
    mission_{mission},
    watchdogs_{watchdogs} {}

void mission_service::thread_function() {
    for (;;) {
        mission_.process_event(::mission::tick{});

        watchdogs_.feed();
        satos::this_thread::sleep_until(satos::clock::next_time_point(100_ms));
    }
}

} // namespace obc::threads
