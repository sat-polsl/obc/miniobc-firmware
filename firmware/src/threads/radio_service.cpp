#include "threads/radio_service.h"
#include "communication/radio/beacon/encode.h"
#include "communication/radio/header/encode.h"
#include "satos/chrono.h"
#include "satos/clock.h"

using namespace satos::chrono_literals;

namespace obc::threads {
radio_service::radio_service(devices::devices::radio_device& radio,
                             ::telemetry::telemetry_state& telemetry_state,
                             watchdogs::watchdogs& watchdogs) :
    radio_{radio},
    telemetry_state_{telemetry_state},
    watchdogs_{watchdogs} {}

void radio_service::thread_function() {
    reset_radio();

    for (;;) {
        std::size_t beacon_size = encode_beacon_message();

        if (auto status = radio_.transmit(std::span(beacon_buffer_.data(), beacon_size));
            status != ::devices::radio::status::radio_tx_ok) {
            handle_transmit_error();
        }

        tx_counter_++;

        watchdogs_.feed();
        satos::this_thread::sleep_until(satos::clock::next_time_point(beacon_interval) + 100_ms);
    }
}

std::size_t radio_service::encode_beacon_message() {
    auto beacon_span = std::span(beacon_buffer_);

    auto size = communication::radio::header::encode(beacon_span, tx_counter_);
    size += communication::radio::beacon::encode(
        telemetry_ids, beacon_span.subspan(size), telemetry_state_);

    return size;
}

void radio_service::handle_transmit_error() {
    error_counter++;
    if (error_counter >= error_limit) {
        reset_radio();
        error_counter = 0;
    }
}
void radio_service::reset_radio() {
    radio_.reset();
    radio_.configure(::devices::radio::configuration{.frequency = frequency,
                                                     .spreading_factor = spreading_factor});
}

} // namespace obc::threads
