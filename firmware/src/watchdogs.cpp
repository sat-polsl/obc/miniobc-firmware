#include "watchdogs.h"
#include "devices.h"

namespace obc::watchdogs {
watchdogs::watchdogs(obc::peripherals::peripherals& peripherals, devices::devices& devices) :
    hw_watchdog_(hw_watchdog_buffer_, [&peripherals]() { peripherals.iwdg().kick(); }),
    eps_watchdog_(eps_watchdog_buffer_, [&devices]() {
        devices.eps().kick_watchdog(::devices::eps::power_output::v3v3_l2);
    }) {}

void watchdogs::initialize() {
    hw_watchdog_timer_.register_callback([this]() { hw_watchdog_.kick(); });
    hw_watchdog_timer_.start();
    eps_watchdog_timer_.register_callback([this]() { eps_watchdog_.kick(); });
    eps_watchdog_timer_.start();
}

watchdog::thread_watchdog& watchdogs::hw() { return hw_watchdog_; }

watchdog::thread_watchdog& watchdogs::eps() { return eps_watchdog_; }

void watchdogs::feed() {
    hw_watchdog_.feed();
    eps_watchdog_.feed();
}

void watchdogs::stop() {
    hw_watchdog_timer_.stop();
    eps_watchdog_timer_.stop();
}

} // namespace obc::watchdogs
