#pragma once
#include <cstdint>
#include <span>
#include "boot_state/descriptor.h"
#include "boot_state/serializer.h"
#include "boot_state/storage.h"
#include "safe/safe.h"
#include "satext/inplace_function.h"
#include "spl/peripherals/crc/crc.h"

namespace boot_state {

class boot_state {
public:
    explicit boot_state(storage& storage, spl::peripherals::crc::crc& crc);

    void initialize();

    std::uint32_t get_boot_counter() const;

    void set_boot_counter(std::uint32_t boot_counter);

    descriptor get() const;

    void set(const descriptor& desc);

private:
    safe::safe<descriptor, serializer, storage> safe_;
};

} // namespace boot_state
