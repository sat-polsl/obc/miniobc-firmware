#pragma once
#include <cstdint>

namespace boot_state {
struct descriptor {
    std::uint32_t boot_counter;
};
} // namespace boot_state
