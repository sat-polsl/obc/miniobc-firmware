#pragma once
#include <cstddef>
#include <span>
#include "boot_state/descriptor.h"

namespace boot_state {

struct serializer {
    static void serialize(const descriptor& desc, std::span<std::byte> buffer);
    static void deserialize(std::span<const std::byte> buffer, descriptor& desc);
};

} // namespace boot_state
