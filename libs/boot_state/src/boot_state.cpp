#include "boot_state/boot_state.h"

namespace boot_state {

boot_state::boot_state(storage& storage, spl::peripherals::crc::crc& crc) :
    safe_{storage, [&crc](std::span<const std::byte> buffer) {
              crc.reset();
              return crc.feed(buffer);
          }} {}

void boot_state::initialize() { safe_.initialize(); }

std::uint32_t boot_state::get_boot_counter() const { return safe_.get(&descriptor::boot_counter); }

void boot_state::set_boot_counter(std::uint32_t boot_counter) {
    safe_.set(&descriptor::boot_counter, boot_counter);
}

descriptor boot_state::get() const { return safe_.get(); }

void boot_state::set(const descriptor& desc) { safe_.set(desc); }

} // namespace boot_state
