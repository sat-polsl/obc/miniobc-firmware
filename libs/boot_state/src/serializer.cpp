#include "boot_state/serializer.h"
#include "satext/struct.h"

namespace boot_state {

using namespace satext::struct_literals;

void serializer::serialize(const descriptor& desc, std::span<std::byte> buffer) {
    satext::pack_to("<I"_fmt, buffer, desc.boot_counter);
}

void serializer::deserialize(std::span<const std::byte> buffer, descriptor& desc) {
    satext::unpack("<I"_fmt, buffer).map([&desc](auto unpacked) {
        auto [boot_counter] = unpacked;
        desc.boot_counter = boot_counter;
    });
}

} // namespace boot_state
