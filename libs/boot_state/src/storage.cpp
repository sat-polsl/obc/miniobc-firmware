#include "boot_state/storage.h"

namespace boot_state {

storage::storage(boot_state::storage::read_callback read,
                 boot_state::storage::write_callback write) :
    read_{std::move(read)},
    write_{std::move(write)} {}

bool storage::read(std::size_t offset, std::span<std::byte> output) const {
    return read_(offset, output);
}

bool storage::write(std::size_t offset, std::span<const std::byte> input) const {
    return write_(offset, input);
}

} // namespace boot_state
