#pragma once
#include <mutex>
#include <ranges>
#include "communication/can/enums.h"
#include "communication/can/types.h"
#include "etl/algorithm.h"
#include "etl/list.h"
#include "object_pool/object_pool.h"
#include "satos/clock.h"
#include "satos/mutex.h"
#include "spl/drivers/can/concepts/can.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief This class provides functionality for asynchronous CAN transactions.
 * @tparam CAN CAN driver type.
 * @tparam TransactionSize Size of transactions buffer.
 */
template<spl::drivers::can::concepts::can CAN, std::size_t TransactionSize>
class can_context {
public:
    /**
     * @brief Constructor.
     * @param can Reference to CAN driver.
     */
    explicit can_context(CAN& can) : can_{can} {}

    /**
     * @brief Sends given message to CAN bus and sets given callback to be executed when message
     * with given id will be received.
     * @param message Message to send.
     * @param response_id Response ID.
     * @param callback Callback to call on message reception.
     * The function signature of the callback must be `void(communication::can::status, const
     * spl::peripherals::can::message&)`.
     * @param timeout Operation timeout.
     */
    void send(const spl::peripherals::can::concepts::message& message,
              std::uint32_t response_id,
              can_callback callback,
              satos::chrono::milliseconds timeout = default_timeout) {
        transaction_pool_.make_unique(response_id, timeout + satos::clock::now(), callback)
            .map([this, &message, timeout](auto transaction) {
                if (can_.write(message, timeout) == spl::drivers::can::status::ok) {
                    std::lock_guard lck{ongoing_mtx_};
                    ongoing_.push_back(std::move(transaction));
                } else {
                    transaction->callback(status::timeout, {});
                }
            })
            .map_error([&callback](auto) { callback(status::no_memory, {}); });
    }

    /**
     * @brief Runs single CAN reception attempt and executes corresponding callback if message was
     * received successfully. Checks all transactions for timeout and executes timed out transaction
     * callbacks.
     */
    void run_one() {
        can_.read(spl::peripherals::can::concepts::rx_fifo{0}, read_timeout)
            .map([this](const spl::peripherals::can::concepts::message& message) {
                std::lock_guard lck{ongoing_mtx_};
                if (auto it = std::ranges::find(ongoing_, message.id, &transaction::response_id);
                    it != ongoing_.end()) {
                    it->get()->callback(status::ok, message);
                    ongoing_.erase(it);
                }
            });

        std::lock_guard lck{ongoing_mtx_};
        auto now = satos::clock::now();
        auto [it, _] = std::ranges::remove_if(ongoing_, [now](auto& transaction) {
            if (transaction->timeout < now) {
                transaction->callback(status::timeout, {});
                return true;
            } else {
                return false;
            }
        });
        ongoing_.erase(it, ongoing_.end());
    }

private:
    static constexpr auto read_timeout = satos::chrono::milliseconds{100};
    static constexpr auto default_timeout = satos::clock::duration::max();

    using pool = object_pool::object_pool<transaction, TransactionSize>;

    CAN& can_;
    pool transaction_pool_{};
    satos::mutex ongoing_mtx_{};
    etl::list<typename pool::unique_ptr, TransactionSize> ongoing_{};
};

/** @} */

} // namespace communication::can
