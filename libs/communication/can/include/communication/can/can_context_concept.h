#pragma once
#include <concepts>
#include "communication/can/types.h"
#include "satos/chrono.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can {

// clang-format off
template<typename T>
concept can_context_concept = requires(T can,
                                       spl::peripherals::can::concepts::message message,
                                       std::uint8_t id,
                                       can_callback callback,
                                       satos::chrono::milliseconds timeout) {
    { can.send(message, id, callback, timeout) };
};
// clang-format on

} // namespace communication::can
