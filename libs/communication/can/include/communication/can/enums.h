#pragma once

namespace communication::can {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief CAN Context operation status.
 */
enum class status { ok, timeout, no_memory, canceled };

/** @} */

} // namespace communication::can
