#pragma once
#include "communication/can/enums.h"
#include "satext/inplace_function.h"
#include "satos/clock.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can {

/**
 * @ingroup communication_can
 * @{
 */

/**
 * @brief CAN callback function wrapper.
 */
using can_callback =
    satext::inplace_function<void(status, const spl::peripherals::can::concepts::message&), 64>;

/**
 * @brief CAN Transaction.
 */
struct transaction {
    std::uint32_t response_id;
    satos::clock::time_point timeout;
    can_callback callback;
};

/** @} */

} // namespace communication::can
