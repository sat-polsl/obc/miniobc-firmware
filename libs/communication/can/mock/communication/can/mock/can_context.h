#pragma once
#include <cstdint>
#include <deque>
#include "gmock/gmock.h"
#include "communication/can/can_context_concept.h"
#include "communication/can/types.h"
#include "satos/chrono.h"
#include "spl/peripherals/can/concepts/types.h"

namespace communication::can::mock {

struct can_context {
    MOCK_METHOD(void,
                send_mock,
                (const spl::peripherals::can::concepts::message&,
                 std::uint32_t,
                 can_callback,
                 satos::chrono::milliseconds));

    void send(const spl::peripherals::can::concepts::message& message,
              std::uint32_t id,
              can_callback callback,
              satos::chrono::milliseconds timeout = satos::chrono::milliseconds::max()) {
        callbacks.push_back(callback);
        send_mock(message, id, callback, timeout);
    }

    void run_one() {
        callbacks.front()(mock_status, mock_message);
        callbacks.pop_front();
    }

    communication::can::status mock_status{};
    spl::peripherals::can::concepts::message mock_message{};

    std::deque<can_callback> callbacks{};
};

static_assert(can_context_concept<can_context>);

} // namespace communication::can::mock
