#pragma once
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <span>
#include "communication/radio/payload/payload_view.h"
#include "satext/struct.h"
#include "telemetry/telemetry_encoder.h"
#include "telemetry/telemetry_state_concept.h"
#include "telemetry/telemetry_writer.h"

namespace communication::radio::beacon {
/**
 * @ingroup communication_radio
 * @{
 */

/**
 * @brief Beacon ID
 */
constexpr payload::id beacon{0};

/**
 * @brief Encodes beacon buffer.
 * @tparam State Telemetry state type.
 * @param ids Span of telemetry IDs.
 * @param buffer Beacon buffer.
 * @param state Telemetry state reference.
 * @return Encoded buffer size.
 */
template<telemetry::telemetry_state_concept State>
std::size_t
encode(std::span<const telemetry::telemetry_id> ids, std::span<std::byte> buffer, State& state) {
    using namespace telemetry;

    payload::payload_view payload(buffer);
    telemetry_encoder encoder{state, payload.data()};

    std::size_t size{};
    for (auto id : ids) {
        size += encoder.encode(id);
    }

    payload.set_size(static_cast<std::uint8_t>(size));
    payload.set_id(beacon);

    return size + payload::payload_view::data_offset;
}

/** @} */

} // namespace communication::radio::beacon
