#pragma once
#include <cstddef>
#include <cstdint>
#include <span>

namespace communication::radio::header {

/**
 * @ingroup communication_radio
 * @{
 */

/**
 * @brief Encodes header buffer.
 * @param buffer Buffer.
 * @param counter Transmit counter.
 * @return Encoded buffer size.
 */
std::size_t encode(std::span<std::byte> buffer, std::uint8_t counter);

/** @} */
} // namespace communication::radio::header
