#pragma once
#include <cstdint>
#include <string_view>

namespace communication::radio::header {

/**
 * @ingroup communication_radio
 * @{
 */

/**
 * @brief Spacecraft callsign.
 */
constexpr std::string_view callsign = "sp9ftl";

/**
 * @brief Spacecraft identifier.
 */
constexpr std::uint8_t spacecraft_id = 1;

/** @} */

} // namespace communication::radio::header
