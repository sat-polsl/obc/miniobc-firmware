#include "communication/radio/header/encode.h"
#include "communication/radio/header/header.h"
#include "satext/struct.h"

using namespace satext::struct_literals;

std::size_t communication::radio::header::encode(std::span<std::byte> buffer,
                                                 std::uint8_t counter) {
    return satext::pack_to("<6sBB"_fmt, buffer, callsign, spacecraft_id, counter);
}
