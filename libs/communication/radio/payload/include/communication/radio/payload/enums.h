#pragma once
#include <cstdint>

namespace communication::radio::payload {

/**
 * @ingroup communication_radio
 * @{
 */

/**
 * @brief Message identifier enumeration.
 */
enum class id : std::uint8_t {};

/** @} */

} // namespace communication::radio::payload
