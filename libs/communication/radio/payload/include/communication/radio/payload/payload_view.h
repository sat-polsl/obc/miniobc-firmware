#pragma once
#include <cstddef>
#include <span>
#include "communication/radio/payload/enums.h"

namespace communication::radio::payload {

/**
 * @ingroup communication_radio
 * @{
 */

/**
 * @brief Describes an object that can refer to contiguous sequence of `std::byte`
 * and interprets it as radio message's payload.
 */
class payload_view {
public:
    /**
     * @brief Constructor.
     * Passed buffer shall have size greater or equal to 2.
     * @param buffer Payload buffer.
     */
    explicit payload_view(std::span<std::byte> buffer);

    /**
     * @brief Returns payload identifier.
     * @return Payload identifier.
     */
    [[nodiscard]] payload::id id() const;

    /**
     * @brief Returns payload size.
     * @return Payload size.
     */
    [[nodiscard]] std::uint8_t size() const;

    /**
     * @brief Returns payload data.
     * @return Payload data.
     */
    [[nodiscard]] std::span<std::byte> data() const;

    /**
     * @brief Sets payload identifier.
     * @param i Payload identifier.
     */
    void set_id(payload::id i);

    /**
     * @brief Sets payload size.
     * @param size Payload size;
     */
    void set_size(std::uint8_t size);

    /**
     * @brief Identifier offset.
     */
    static constexpr std::size_t id_offset = 0;

    /**
     * @brief Size offset.
     */
    static constexpr std::size_t size_offset = 1;

    /**
     * @brief Data offset.
     */
    static constexpr std::size_t data_offset = 2;

private:
    std::byte* id_;
    std::byte* size_;
    std::span<std::byte> data_;
};

/** @} */

} // namespace communication::radio::payload
