#include "communication/radio/payload/payload_view.h"
#include "satext/assert.h"
#include "satext/type_traits.h"

namespace communication::radio::payload {

payload_view::payload_view(std::span<std::byte> buffer) {
    satext::expects(buffer.size() >= 2);
    id_ = buffer.data() + id_offset;
    size_ = buffer.data() + size_offset;
    data_ = buffer.subspan(data_offset);
}

payload::id payload_view::id() const { return static_cast<payload::id>(*id_); }

std::uint8_t payload_view::size() const { return static_cast<std::uint8_t>(*size_); }

std::span<std::byte> payload_view::data() const { return data_; }

void payload_view::set_id(payload::id i) { *id_ = std::byte{satext::to_underlying_type(i)}; }

void payload_view::set_size(std::uint8_t size) { *size_ = std::byte{size}; }

}; // namespace communication::radio::payload
