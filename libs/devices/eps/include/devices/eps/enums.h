#pragma once
#include <cstdint>

namespace devices::eps {

/**
 * @ingroup eps
 * @{
 */

/**
 * @brief EPS operation status.
 */
enum class status { ok, timeout, communication_error };

/**
 * @brief EPS Power Output enumeration.
 */
enum class power_output { v3v3_l1, v3v3_l2, v3v3_l3, v3v3_l4, v5_l1, v5_l2, v12 };

/**
 * @brief EPS Telemetry ID enumeration.
 */
enum class telemetry_id : std::uint16_t {
    boot_counter = 0,
    uptime = 1,
    temperature_3v3 = 2,
    temperature_5v = 3,
    temperature_12v = 4,
    temperature_cell1 = 5,
    temperature_cell2 = 6,
    temperature_cell3 = 7,
    v3v3v_l1 = 8,
    i3v3v_l1 = 9,
    v3v3v_l2 = 10,
    i3v3v_l2 = 11,
    v3v3v_l3 = 12,
    i3v3v_l3 = 13,
    v3v3v_l4 = 14,
    i3v3v_l4 = 15,
    v5v_l1 = 16,
    i5v_l1 = 17,
    v5v_l2 = 18,
    i5v_l2 = 19,
    v12v = 20,
    i12v = 21,
    vbms = 22,
    ibms = 23,
    vmppt = 24,
    imppt1 = 25,
    imppt2 = 26
};

/** @} */

} // namespace devices::eps
