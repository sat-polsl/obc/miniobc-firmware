#pragma once
#include <mutex>
#include "communication/can/can_context_concept.h"
#include "communication/can/enums.h"
#include "devices/eps/enums.h"
#include "devices/eps/id.h"
#include "satext/constexpr_map.h"
#include "satext/expected.h"
#include "satext/struct.h"
#include "satext/type_traits.h"
#include "satos/chrono.h"
#include "satos/event.h"
#include "satos/mutex.h"
#include "telemetry/telemetry_decoder.h"
#include "telemetry/types.h"

namespace devices::eps {

/**
 * @ingroup eps
 * @{
 */

/**
 * @brief EPS device.
 * @tparam Context CAN context type.
 * @tparam Mutex Mutex type.
 * @tparam Event Event type.
 */
template<communication::can::can_context_concept Context>
class eps {
public:
    /**
     * @brief Constructor.
     * @param context CAN Context reference.
     * @param timeout Timeout for CAN operations.
     */
    eps(Context& context, satos::chrono::milliseconds timeout) :
        context_{context},
        timeout_{timeout} {}

    /**
     * @brief Turn EPS Power Output on.
     * @param output Power Output.
     * @return Operation status.
     */
    status power_output_on(power_output output) {
        return generic_transaction<id::power_on>(output);
    }

    /**
     * @brief Turn EPS Power Output off.
     * @param output Power Output.
     * @return Operation status.
     */
    status power_output_off(power_output output) {
        return generic_transaction<id::power_off>(output);
    }

    /**
     * @brief Enable watchdog for given output.
     * @param output Power Output.
     * @return Operation status.
     */
    status enable_watchdog(power_output output) {
        return generic_transaction<id::enable_watchdog>(output);
    }

    /**
     * @brief Disable watchdog for given output.
     * @param output Power Output.
     * @return Operation status.
     */
    status disable_watchdog(power_output output) {
        return generic_transaction<id::disable_watchdog>(output);
    }

    /**
     * @brief Kick watchdog for given output.
     * @param output Power Output.
     * @return Operation status.
     */
    status kick_watchdog(power_output output) {
        return generic_transaction<id::kick_watchdog>(output);
    }

    /**
     * @brief Sets watchdog timeout for given power output.
     * @param output Power Output.
     * @param timeout Timeout to set in seconds.
     * @return Operation status.
     */
    status set_watchdog_timeout(power_output output, satos::chrono::seconds timeout) {
        using namespace satext::struct_literals;
        auto output_value = output_lut[output];
        spl::peripherals::can::concepts::message request{.id = id::set_watchdog_watchdog::request,
                                                         .is_extended = true};
        request.size = static_cast<std::uint8_t>(satext::pack_to(
            ">BH"_fmt, request.data, static_cast<std::uint8_t>(output_value), timeout.count()));

        return do_transaction(request, id::set_watchdog_watchdog::response, output_value);
    }

    /**
     * @brief Request telemetry value of given ID.
     * @param id Telemetry ID.
     * @return Telemetry value on success, operation status otherwise.
     */
    satext::expected<telemetry::telemetry_value, status> get_telemetry(telemetry_id id) {
        using namespace satext::struct_literals;
        spl::peripherals::can::concepts::message request{.id = id::get_telemetry::request,
                                                         .is_extended = true};

        request.size = static_cast<std::uint8_t>(
            satext::pack_to(">H"_fmt, request.data, satext::to_underlying_type(id)));

        auto timeout_abs = satos::clock::now() + timeout_;

        std::unique_lock lck{mtx_, timeout_abs};
        if (!lck.owns_lock()) {
            return satext::unexpected{status::timeout};
        }

        satext::expected<telemetry::telemetry_value, status> result{};
        satos::event_guard event(event_);
        context_.send(request,
                      id::get_telemetry::response,
                      get_telemetry_response_callback(*this, result, id),
                      timeout_abs - satos::clock::now());
        if (!event.wait_until(timeout_abs)) {
            return satext::unexpected{status::timeout};
        }

        return result;
    }

private:
    class get_telemetry_response_callback {
    public:
        get_telemetry_response_callback(
            eps& eps,
            satext::expected<telemetry::telemetry_value, status>& result,
            telemetry_id id) :
            eps_{eps},
            result_{result},
            id_{id} {}

        void operator()(communication::can::status can_status,
                        const spl::peripherals::can::concepts::message& response) {
            if (auto status = map_can_status(can_status); status == status::ok) {
                telemetry::telemetry_decoder decoder{
                    std::span(response.data.data(), response.size)};

                result_ = decoder.decode()
                              .map_error([](auto) { return status::communication_error; })
                              .and_then([this](auto key_value) -> result_type {
                                  if (satext::to_underlying_type(id_) != key_value.id) {
                                      return satext::unexpected{status::communication_error};
                                  }
                                  return key_value.value;
                              });
            } else {
                result_ = satext::unexpected{status};
            }

            eps_.event_.notify();
        }

    private:
        using result_type = satext::expected<telemetry::telemetry_value, status>;

        eps& eps_;
        result_type& result_;
        telemetry_id id_;
    };

    static constexpr satext::constexpr_map<power_output, std::byte, 7> output_lut{
        {power_output::v3v3_l1, std::byte{0x00}},
        {power_output::v3v3_l2, std::byte{0x01}},
        {power_output::v3v3_l3, std::byte{0x02}},
        {power_output::v3v3_l4, std::byte{0x03}},
        {power_output::v5_l1, std::byte{0x04}},
        {power_output::v5_l2, std::byte{0x05}},
        {power_output::v12, std::byte{0x06}},
    };

    status do_transaction(const spl::peripherals::can::concepts::message& request,
                          std::uint32_t response_id,
                          std::byte output_value) {
        status result{};
        auto timeout_abs = satos::clock::now() + timeout_;

        std::unique_lock lck{mtx_, timeout_abs};
        if (!lck.owns_lock()) {
            return status::timeout;
        }

        satos::event_guard event(event_);
        context_.send(
            request,
            response_id,
            [this, &result, output_value](auto status, const auto& response) {
                result = map_can_status(status);

                if (result == status::ok &&
                    (response.size != 1 || response.data[0] != output_value)) {
                    result = status::communication_error;
                }

                event_.notify();
            },
            timeout_abs - satos::clock::now());
        if (!event.wait_until(timeout_abs)) {
            return status::timeout;
        }

        return result;
    }

    template<typename T>
    status generic_transaction(power_output output) {
        auto output_value = output_lut[output];
        spl::peripherals::can::concepts::message request{
            .id = T::request, .data = {output_value}, .size = 1, .is_extended = true};

        return do_transaction(request, T::response, output_value);
    }

    static status map_can_status(communication::can::status can_status) {
        switch (can_status) {
        case communication::can::status::ok:
            return status::ok;
        case communication::can::status::timeout:
            return status::timeout;
        default:
            return status::communication_error;
        }
    }

    Context& context_;
    satos::chrono::milliseconds timeout_;
    satos::mutex mtx_;
    satos::event event_;
};

/** @} */

} // namespace devices::eps
