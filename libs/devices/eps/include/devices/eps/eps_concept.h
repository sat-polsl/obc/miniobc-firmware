#pragma once
#include <concepts>
#include "devices/eps/enums.h"
#include "satext/expected.h"
#include "satos/chrono.h"
#include "telemetry/types.h"

namespace devices::eps {

// clang-format off
template<typename T>
concept eps_concept = requires(T eps, power_output output, satos::chrono::seconds timeout, telemetry_id id) {
    { eps.power_output_on(output) } -> std::same_as<status>;
    { eps.power_output_off(output) } -> std::same_as<status>;
    { eps.enable_watchdog(output) } -> std::same_as<status>;
    { eps.disable_watchdog(output) } -> std::same_as<status>;
    { eps.kick_watchdog(output) } -> std::same_as<status>;
    { eps.set_watchdog_timeout(output, timeout) } -> std::same_as<status>;
    { eps.get_telemetry(id) } -> std::same_as<satext::expected<telemetry::telemetry_value, status>>;
};
// clang-format on

} // namespace devices::eps
