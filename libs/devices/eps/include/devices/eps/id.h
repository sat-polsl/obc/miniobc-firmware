#pragma once
#include <cstdint>

namespace devices::eps::id {

/**
 * @defgroup eps_id EPS CAN frames identifiers.
 * @ingroup eps
 * @{
 */

struct echo {
    static constexpr std::uint32_t request = 0x500;
    static constexpr std::uint32_t response = 0x600;
};

struct power_on {
    static constexpr std::uint32_t request = 0x900;
    static constexpr std::uint32_t response = 0xa00;
};

struct power_off {
    static constexpr std::uint32_t request = 0xd00;
    static constexpr std::uint32_t response = 0xe00;
};

struct enable_watchdog {
    static constexpr std::uint32_t request = 0x1100;
    static constexpr std::uint32_t response = 0x1200;
};

struct disable_watchdog {
    static constexpr std::uint32_t request = 0x1500;
    static constexpr std::uint32_t response = 0x1600;
};

struct set_watchdog_watchdog {
    static constexpr std::uint32_t request = 0x1900;
    static constexpr std::uint32_t response = 0x1a00;
};

struct kick_watchdog {
    static constexpr std::uint32_t request = 0x1d00;
    static constexpr std::uint32_t response = 0x1e00;
};

struct get_telemetry {
    static constexpr std::uint32_t request = 0x2100;
    static constexpr std::uint32_t response = 0x2200;
};

/** @} */

} // namespace devices::eps::id
