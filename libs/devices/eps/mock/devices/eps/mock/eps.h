#pragma once
#include "gmock/gmock.h"
#include "devices/eps/enums.h"
#include "devices/eps/eps_concept.h"
#include "satext/expected.h"
#include "satos/chrono.h"
#include "telemetry/types.h"

namespace devices::eps::mock {

struct eps {
    using get_telemetry_result = satext::expected<telemetry::telemetry_value, devices::eps::status>;
    MOCK_METHOD(status, power_output_on, (devices::eps::power_output));
    MOCK_METHOD(status, power_output_off, (devices::eps::power_output));
    MOCK_METHOD(status, enable_watchdog, (devices::eps::power_output));
    MOCK_METHOD(status, disable_watchdog, (devices::eps::power_output));
    MOCK_METHOD(status, kick_watchdog, (devices::eps::power_output));
    MOCK_METHOD(status, set_watchdog_timeout, (devices::eps::power_output, satos::chrono::seconds));
    MOCK_METHOD(get_telemetry_result, get_telemetry, (devices::eps::telemetry_id));
};

static_assert(devices::eps::eps_concept<eps>);
} // namespace devices::eps::mock
