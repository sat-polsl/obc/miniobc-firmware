#pragma once
#include <span>
#include <variant>
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/i2c/concepts/i2c.h"

namespace devices::fram {
/**
 * @addtogroup fram
 * @{
 */

/**
 * @brief MB85RC256V FRAM driver.
 * @tparam I2C I2C vectored driver.
 */
template<spl::drivers::i2c::concepts::i2c_vectored I2C>
class fram {
public:
    /**
     * @brief Constructor.
     * @param i2c I2C driver reference.
     * @param i2c_address I2C address of FRAM.
     * @param timeout operations timeout.
     */
    fram(I2C& i2c,
         std::uint8_t i2c_address,
         satos::clock::duration timeout = satos::clock::duration::max()) :
        i2c_{i2c},
        i2c_address_{i2c_address},
        timeout_{timeout} {}

    /**
     * @brief Reads memory from given address.
     * @param address Memory address.
     * @param buffer Buffer where data will be stored.
     * @return Span with data if operation was successful, unexpected otherwise.
     */
    satext::expected<std::span<const std::byte>, std::monostate> read(std::uint32_t address,
                                                                      std::span<std::byte> output) {
        std::array<std::byte, 2> address_buffer = {std::byte((address >> 8) & 0xff),
                                                   std::byte(address & 0xff)};

        std::array<std::span<const std::byte>, 1> in = {address_buffer};
        std::array<std::span<std::byte>, 1> out = {output};

        auto status = i2c_.write_read(i2c_address_, in, out, timeout_);

        if (status == spl::drivers::i2c::status::ok) {
            return output;
        } else {
            return satext::unexpected{std::monostate{}};
        }
    }

    /**
     * @brief Write data to memory at given address
     * @param address Memory address
     * @param buffer Buffer to write
     * @return true if operation was successful, false otherwise
     */
    bool write(std::uint32_t address, std::span<const std::byte> input) {
        std::array<std::byte, 2> address_buffer = {std::byte((address >> 8) & 0xff),
                                                   std::byte(address & 0xff)};

        std::array<std::span<const std::byte>, 2> in = {address_buffer, input};

        return i2c_.write(i2c_address_, in, timeout_) == spl::drivers::i2c::status::ok;
    }

private:
    I2C& i2c_;
    std::uint8_t i2c_address_;
    satos::clock::duration timeout_;
};

/** @} */
} // namespace devices::fram
