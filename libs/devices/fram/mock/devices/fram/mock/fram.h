#pragma once
#include "gmock/gmock.h"
#include "devices/fram/fram_concept.h"

namespace devices::fram::mock {

struct fram {
    using read_result = satext::expected<std::span<const std::byte>, std::monostate>;
    MOCK_METHOD(read_result, read, (std::uint32_t, std::span<std::byte>));
    MOCK_METHOD(bool, write, (std::uint32_t, std::span<const std::byte>));
};

static_assert(devices::fram::fram_concept<fram>);
} // namespace devices::fram::mock
