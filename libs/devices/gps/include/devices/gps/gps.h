#pragma once
#include <ranges>
#include <span>
#include "nmea/floating_policy/floating.h"
#include "nmea/parse.h"
#include "nmea/types.h"
#include "spl/drivers/uart/concepts/uart.h"

namespace devices::gps {

/**
 * @ingroup gps
 * @{
 */

/**
 * @brief GPS UART Driver
 * @tparam UART
 */
template<spl::drivers::uart::concepts::uart UART>
class gps {
public:
    /**
     * @brief Constructor.
     * @param uart UART reference.
     * @param buffer Buffer to store readed data.
     */
    gps(UART& uart, std::span<char> buffer) : uart_{uart}, buffer_{buffer} {}

    /**
     * @brief Reads NMEA message from GPS.
     * @return NMEA message on success, NMEA error otherwise.
     */
    auto read_nmea() {
        std::ranges::fill(buffer_, 0);
        return uart_.read_until(std::as_writable_bytes(buffer_), std::byte('\n'))
            .map_error([](auto) { return nmea::error::parse_error; })
            .and_then([this](auto result) {
                return nmea::parse<nmea::floating_policy::floating>(
                    std::string_view(buffer_.data(), std::size(result)));
            });
    }

private:
    UART& uart_;
    std::span<char> buffer_;
};

/** @} */
} // namespace devices::gps
