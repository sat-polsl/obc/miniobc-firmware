#pragma once
#include "satext/expected.h"
#include "satos/chrono.h"
#include "satos/thread.h"
#include "spl/drivers/i2c/concepts/i2c.h"

namespace devices::mprls {
/**
 * @ingroup mprls
 * @{
 */

/**
 * @brief MPRLS device class.
 * @tparam I2C
 */
template<spl::drivers::i2c::concepts::i2c_vectored I2C>
class mprls {
public:
    /**
     * @brief Constructor.
     * @param i2c Reference to I2C driver.
     * @param timeout Operations timeout.
     */
    mprls(I2C& i2c, satos::chrono::milliseconds timeout) : i2c_{i2c}, timeout_{timeout} {}

    /**
     * @brief Reads pressure from device.
     * @return On success returns pressure in Pa, on failure returns I2C operation status.
     */
    satext::expected<std ::uint32_t, spl::drivers::i2c::status> read_pressure() {
        std::array<std::byte, 3> in_buffer = {std::byte{0xaa}, std::byte{}, std::byte{}};
        std::array<std::span<const std::byte>, 1> in = {in_buffer};
        std::array<std::byte, 4> out_buffer{};
        std::array<std::span<std::byte>, 1> out = {out_buffer};

        auto status = i2c_.write(address, in, timeout_);
        if (status != spl::drivers::i2c::status::ok) {
            return satext::unexpected{status};
        }

        satos::this_thread::sleep_for(satos::chrono::milliseconds{20});

        status = i2c_.read(address, out, timeout_);
        if (status != spl::drivers::i2c::status::ok) {
            return satext::unexpected{status};
        }
        std::uint32_t value = static_cast<std::uint32_t>(out_buffer[1]) << 16 |
                              static_cast<std::uint32_t>(out_buffer[2]) << 8 |
                              static_cast<std::uint32_t>(out_buffer[3]);

        std::uint64_t value64 = static_cast<std::uint64_t>(value - subtract_coefficient) *
                                multiply_coefficient / division_coefficient;
        return static_cast<std::uint32_t>(value64);
    }

private:
    I2C& i2c_;
    satos::chrono::milliseconds timeout_;

    static constexpr std::uint32_t subtract_coefficient = 1'677'722;
    static constexpr std::uint32_t multiply_coefficient = 12'842'475;
    static constexpr std::uint32_t division_coefficient = 1'000'000'000;
    static constexpr std::uint8_t address = 0x18;
};

/** @} */
} // namespace devices::mprls
