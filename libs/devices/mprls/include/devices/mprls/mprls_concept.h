#pragma once
#include <concepts>
#include <cstdint>
#include "satext/expected.h"
#include "spl/drivers/i2c/enums.h"

namespace devices::mprls {
// clang-format off
template<typename T>
concept mprls_concept = requires(T mprls) {
    { mprls.read_pressure() } -> std::same_as<satext::expected<std::uint32_t, spl::drivers::i2c::status>>;
};
// clang-format on

} // namespace devices::mprls
