#pragma once
#include "gmock/gmock.h"
#include "devices/mprls/mprls_concept.h"
#include "spl/drivers/i2c/enums.h"

namespace devices::mprls::mock {

struct mprls {
    using read_result = satext::expected<std::uint32_t, spl::drivers::i2c::status>;
    MOCK_METHOD(read_result, read_pressure, ());
};

static_assert(devices::mprls::mprls_concept<mprls>);
} // namespace devices::mprls::mock
