set(TARGET muons)

add_library(${TARGET} INTERFACE)

target_include_directories(${TARGET}
    INTERFACE
    include
    )

target_sources(${TARGET} PRIVATE
    include/devices/muons/muons.h
    )

target_link_libraries(${TARGET}
    INTERFACE
    spl::i2c_concept
    satext::satext
    satos::api
    )

add_mock_library(muons_mock
    INCLUDES
    include
    mock
    SOURCES
    mock/devices/muons/mock/muons.h
    )
