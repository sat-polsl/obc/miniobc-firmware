#pragma once
#include <array>
#include "satext/expected.h"
#include "satext/struct.h"
#include "satos/chrono.h"
#include "spl/drivers/i2c/concepts/i2c.h"

namespace devices::muons {

/**
 * @ingroup muons
 * @{
 */

/**
 * @brief Muons driver.
 * @tparam I2C I2C type.
 */
template<spl::drivers::i2c::concepts::i2c_vectored I2C>
class muons {
public:
    /**
     * @brief Constructor.
     * @param i2c Reference to I2C.
     * @param timeout Timeout for I2C operation.
     */
    muons(I2C& i2c, satos::chrono::milliseconds timeout) : i2c_{i2c}, timeout_{timeout} {}

    /**
     * @brief Reads count value from device.
     * @return Count value on success, communication status otherwise.
     */
    satext::expected<std::uint32_t, spl::drivers::i2c::status> read_count() {
        using namespace satext::struct_literals;
        std::array<std::byte, 4> buffer{};
        std::array<std::span<std::byte>, 1> out{buffer};

        if (auto status = i2c_.read(address, out, timeout_);
            status != spl::drivers::i2c::status::ok) {
            return satext::unexpected{status};
        }

        return satext::unpack(">I"_fmt, buffer)
            .map_error([](auto) { return spl::drivers::i2c::status::transfer_error; })
            .map([](auto unpacked) {
                auto [count] = unpacked;
                return count;
            });
    }

private:
    I2C& i2c_;
    satos::chrono::milliseconds timeout_;

    static constexpr std::uint8_t address = 0x47;
};

/** @} */

} // namespace devices::muons
