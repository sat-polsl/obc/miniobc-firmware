#pragma once
#include <concepts>
#include <cstdint>
#include "satext/expected.h"
#include "spl/drivers/i2c/enums.h"

namespace devices::muons {
// clang-format off
template<typename T>
concept muons_concept = requires(T muons) {
    { muons.read_count() } -> std::same_as<satext::expected<std::uint32_t, spl::drivers::i2c::status>>;
};
// clang-format on

} // namespace devices::muons
