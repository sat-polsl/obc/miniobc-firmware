#pragma once
#include "gmock/gmock.h"
#include "devices/muons/muons_concept.h"
#include "spl/drivers/i2c/enums.h"

namespace devices::muons::mock {

struct muons {
    using read_result = satext::expected<std::uint32_t, spl::drivers::i2c::status>;
    MOCK_METHOD(read_result, read_count, ());
};

static_assert(devices::muons::muons_concept<muons>);
} // namespace devices::muons::mock
