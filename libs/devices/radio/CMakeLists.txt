set(TARGET radio)

add_library(${TARGET} INTERFACE)

target_include_directories(${TARGET}
    INTERFACE
    include
    )

target_sources(${TARGET}
    PRIVATE
    include/devices/radio/radio.h
    include/devices/radio/radio_concept.h
    include/devices/radio/configuration.h
    include/devices/radio/enums.h
    )

target_link_libraries(${TARGET}
    INTERFACE
    spl::uart_concept
    spl::gpio_ll_concept
    satos::api
    fmt
    etl
    )

add_mock_library(radio_mock
    INCLUDES
    include
    mock
    SOURCES
    mock/devices/radio/mock/radio.h
    )
