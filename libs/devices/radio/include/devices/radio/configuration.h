#pragma once
#include <string_view>
#include "devices/radio/enums.h"
#include "etl/vector.h"
#include "satos/chrono.h"

namespace devices::radio {

/**
 * @ingroup radio
 * @{
 */

/**
 * @brief RN2483 radio configuration struct.
 */
struct configuration {
    /**
     * @brief Modulation. "lora" or "fsk"
     */
    std::string_view modulation{devices::radio::modulation::lora};

    /**
     * @brief Frequency from 433000000 to 434800000 Hz.
     */
    std::uint32_t frequency{};

    /**
     * @brief Power in dBm from -3 to 15.
     */
    std::int32_t power{15};

    /**
     * @brief LoRa spreading factor.
     */
    std::string_view spreading_factor{devices::radio::spreading_factor::sf12};

    /**
     * @brief LoRa bandwidth.
     */
    bandwidth lora_bandwidth{bandwidth::bw125khz};

    /**
     * @brief LoRa coding rate.
     */
    std::string_view coding_rate{devices::radio::coding_rate::cr4_5};

    /**
     * @brief sync word. 1-byte for LoRa, 8-bytes for FSK.
     */
    etl::vector<std::byte, 8> sync{std::byte{0x12}};

    /**
     * @brief LoRa IQ inversion
     */
    std::string_view iq_inversion{devices::radio::on_off::off};

    /**
     * @brief Watchdog timeout in milliseconds. Zero means watchdog is disabled.
     */
    satos::chrono::milliseconds watchdog_timeout{0};
};

/** @} */

} // namespace devices::radio
