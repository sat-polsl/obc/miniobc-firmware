#pragma once
#include <string_view>

namespace devices::radio {
/**
 * @ingroup radio
 * @{
 */

/**
 * @brief RN2483 radio commands.
 */
struct command {
    static constexpr std::string_view sys = "sys";
    static constexpr std::string_view mac = "mac";
    static constexpr std::string_view radio = "radio";
    static constexpr std::string_view get = "get";
    static constexpr std::string_view set = "set";
    static constexpr std::string_view pause = "pause";
    static constexpr std::string_view sleep = "sleep";
    static constexpr std::string_view rx = "rx";
    static constexpr std::string_view tx = "tx";
    static constexpr std::string_view modulation = "mod";
    static constexpr std::string_view frequency = "freq";
    static constexpr std::string_view power = "pwr";
    static constexpr std::string_view spreading_factor = "sf";
    static constexpr std::string_view bandwidth = "bw";
    static constexpr std::string_view coding_rate = "cr";
    static constexpr std::string_view iq_inversion = "iqi";
    static constexpr std::string_view sync = "sync";
    static constexpr std::string_view watchdog = "wdt";
    static constexpr std::string_view data_shaping = "bt";
    static constexpr std::string_view automatic_frequency_correction_bandwidth = "afcbw";
    static constexpr std::string_view rx_bandwidth = "rxbw";
    static constexpr std::string_view bitrate = "bitrate";
    static constexpr std::string_view frequency_deviation = "fdev";
    static constexpr std::string_view preamble_length = "prlen";
    static constexpr std::string_view crc = "crc";
    static constexpr std::string_view snr = "snr";
};

/**
 * @brief RN2483 radio modulations.
 */
struct modulation {
    static constexpr std::string_view lora = "lora";
    static constexpr std::string_view fsk = "fsk";
};

/**
 * @brief RN2483 radio LoRa spreading factor.
 */
struct spreading_factor {
    static constexpr std::string_view sf7 = "sf7";
    static constexpr std::string_view sf8 = "sf8";
    static constexpr std::string_view sf9 = "sf9";
    static constexpr std::string_view sf10 = "sf10";
    static constexpr std::string_view sf11 = "sf11";
    static constexpr std::string_view sf12 = "sf12";
};

/**
 * @brief RN2483 radio LoRa coding rate.
 */
struct coding_rate {
    static constexpr std::string_view cr4_5 = "4/5";
    static constexpr std::string_view cr4_6 = "4/6";
    static constexpr std::string_view cr4_7 = "4/7";
    static constexpr std::string_view cr4_8 = "4/8";
};

/**
 * @brief RN2483 radio on/off options.
 */
struct on_off {
    static constexpr std::string_view on = "on";
    static constexpr std::string_view off = "off";
};

/**
 * @brief RN2483 radio serial responses.
 */
struct response {
    static constexpr std::string_view ok = "ok\r\n";
    static constexpr std::string_view invalid_parameter = "invalid_param\r\n";
    static constexpr std::string_view busy = "busy\r\n";
    static constexpr std::string_view radio_rx = "radio_rx";
    static constexpr std::string_view radio_tx_ok = "radio_tx_ok\r\n";
    static constexpr std::string_view radio_err = "radio_err\r\n";
    static constexpr std::string_view version = "RN2483 1.0.3 Mar 22 2017 06:00:42\r\n";
};

/**
 * @brief RN2483 response status enumeration.
 */
enum class status {
    ok,
    invalid_parameter,
    busy,
    radio_rx,
    radio_tx_ok,
    radio_error,
    version,
    parse_error
};

/**
 * @brief RN2483 LoRa bandwidth enumeration.
 */
enum class bandwidth { bw125khz = 125, bw250khz = 250, bw500khz = 500 };

/** @} */
} // namespace devices::radio
