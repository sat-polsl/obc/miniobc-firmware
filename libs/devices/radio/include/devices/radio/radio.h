#pragma once
#include <optional>
#include <ranges>
#include <string_view>
#include "devices/radio/configuration.h"
#include "devices/radio/enums.h"
#include "etl/vector.h"
#include "fmt/format.h"
#include "satext/charconv.h"
#include "satext/type_traits.h"
#include "satos/chrono.h"
#include "satos/thread.h"
#include "spl/drivers/uart/concepts/uart.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace devices::radio {

/**
 * @ingroup radio
 * @{
 */

/**
 * @brief RN2483 radio device.
 *
 * [Datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/50002346C.pdf)
 *
 * [Command Reference Guide](https://ww1.microchip.com/downloads/en/DeviceDoc/40001784B.pdf)
 * @tparam UART UART driver
 * @tparam GPIO GPIO peripheral
 */
template<spl::drivers::uart::concepts::uart UART, spl::peripherals::gpio::concepts::gpio GPIO>
class radio {
public:
    /**
     * @brief Constructor.
     * @param uart Reference to UART driver.
     * @param reset Reference to reset GPIO.
     */
    radio(UART& uart, GPIO& reset) : uart_{uart}, reset_{reset} {}

    /**
     * @brief Resets radio.
     * @return true if success, false otherwise.
     */
    bool reset() {
        reset_.clear();
        satos::this_thread::sleep_for(reset_sleep);
        reset_.set();
        auto response = read_response(powerup_timeout);

        if (response.has_value()) {
            return *response == response::version;
        } else {
            return false;
        }
    }

    /**
     * @brief Sends command to pause mac.
     * @return true if success, false otherwise.
     */
    bool mac_pause() {
        auto end = fmt::format_to(buffer_.begin(), "{} {}{}", command::mac, command::pause, crlf);
        send_command({buffer_.begin(), end});
        auto response = read_response();
        return response.has_value();
    }

    /**
     * @brief Sets radio modulation.
     * @param modulation Modulation to set.
     * @return Operation status.
     */
    status set_modulation(std::string_view modulation) {
        return request_response(command::radio, command::set, command::modulation, modulation);
    }

    /**
     * @brief Sets radio frequency.
     * @param frequency Frequency to set.
     * @return Operation status.
     */
    status set_frequency(std::uint32_t frequency) {
        return request_response(command::radio, command::set, command::frequency, frequency);
    }

    /**
     * @brief Sets radio power.
     * @param power Power to set.
     * @return Operation status.
     */
    status set_power(std::int32_t power) {
        return request_response(command::radio, command::set, command::power, power);
    }

    /**
     * @brief Sets LoRa spreading factor.
     * @param spreading_factor Spreading factor to set.
     * @return Operation status.
     */
    status set_spreading_factor(std::string_view spreading_factor) {
        return request_response(
            command::radio, command::set, command::spreading_factor, spreading_factor);
    }

    /**
     * @brief Sets LoRa bandwidth.
     * @param bw Bandwidth to set.
     * @return Operation status.
     */
    status set_bandwidth(bandwidth bw) {
        return request_response(
            command::radio, command::set, command::bandwidth, satext::to_underlying_type(bw));
    }

    /**
     * @brief Sets LoRa coding rate.
     * @param coding_rate Coding rate to set.
     * @return Operation status.
     */
    status set_coding_rate(std::string_view coding_rate) {
        return request_response(command::radio, command::set, command::coding_rate, coding_rate);
    }

    /**
     * @brief Sets sync word.
     * @param sync Sync word to set.
     * @return Operation status.
     */
    status set_sync(const etl::vector<std::byte, 8>& sync) {
        decltype(buffer_.begin()) it{};
        it = fmt::format_to(
            buffer_.begin(), "{} {} {} ", command::radio, command::set, command::sync);
        if (auto [end, err] = satext::hexify(sync, std::span(it, sync.size() * 2));
            err == std::errc{}) {
            it = end;
        } else {
            return status::parse_error;
        }
        it = fmt::format_to(it, "{}", crlf);

        send_command({buffer_.begin(), it});
        auto response = read_response().map(map_status);
        return response.has_value() ? response.value() : response.error();
    }

    /**
     * @brief Sets IQ inversion.
     * @param iqi On/off option.
     * @return Operation status.
     */
    status set_iq_inversion(std::string_view iqi) {
        return request_response(command::radio, command::set, command::iq_inversion, iqi);
    }

    /**
     * @brief Sets watchdog timeout.
     * @param timeout Timeout to set.
     * @return Operation status.
     */
    status set_watchdog_timeout(satos::chrono::milliseconds timeout) {
        return request_response(command::radio, command::set, command::watchdog, timeout.count());
    }

    /**
     * @brief Configures radio in LoRa mode.
     * @param cfg Configuration.
     * @return true on success, false otherwise.
     */
    bool configure(const devices::radio::configuration& cfg) {
        using enum devices::radio::status;
        return mac_pause() && set_modulation(cfg.modulation) == ok //
               && set_frequency(cfg.frequency) == ok               //
               && set_power(cfg.power) == ok                       //
               && set_spreading_factor(cfg.spreading_factor) == ok //
               && set_bandwidth(cfg.lora_bandwidth) == ok          //
               && set_coding_rate(cfg.coding_rate) == ok           //
               && set_sync(cfg.sync) == ok                         //
               && set_iq_inversion(cfg.iq_inversion) == ok         //
               && set_watchdog_timeout(cfg.watchdog_timeout) == ok;
    }

    /**
     * @brief Transmits given buffer.
     * @param buffer Buffer to transmit.
     * @return Operation status.
     */
    status transmit(std::span<const std::byte> buffer) {
        auto it = fmt::format_to(buffer_.begin(), "{} {} ", command::radio, command::tx);
        auto distance_to_end = std::distance(it, buffer_.end());
        if (distance_to_end % 2 == 1) {
            distance_to_end--;
        }
        if (auto [end, err] = satext::hexify(buffer, std::span(it, distance_to_end));
            err == std::errc{}) {
            it = end;
        } else {
            return status::parse_error;
        }
        it = fmt::format_to(it, "{}", crlf);
        send_command({buffer_.begin(), it});
        auto result =
            read_response()
                .map(map_status)
                .and_then([this](status tx_start_status) -> satext::expected<status, status> {
                    if (tx_start_status == status::ok) {
                        return read_response(tx_timeout).map(map_status);
                    } else {
                        return satext::unexpected{tx_start_status};
                    }
                });

        return result.has_value() ? result.value() : result.error();
    }

    /**
     * @brief Starts receive operation for given timeout and saves received data to given buffer.
     * @param buffer Buffer to save received data.
     * @param timeout Receive operation timeout.
     * @return On success span with received data, operation status otherwise.
     */
    satext::expected<std::span<const std::byte>, status>
    receive(std::span<std::byte> buffer,
            satos::chrono::milliseconds timeout = satos::chrono::milliseconds::max()) {
        if (status request_status = request_response(command::radio, command::rx, 0);
            request_status != status::ok) {
            return satext::unexpected{request_status};
        }

        return read_response(timeout).and_then(
            [buffer](auto response) -> satext::expected<std::span<const std::byte>, status> {
                response = response.substr(0, response.find_first_of(crlf));
                auto status_end = response.find_first_of(' ');
                if (auto rx_status = response.substr(0, status_end);
                    rx_status != response::radio_rx) {
                    return satext::unexpected{status::radio_error};
                }

                auto data_begin = status_end + response.substr(status_end).find_first_not_of(' ');
                auto data = response.substr(data_begin);
                if (auto [_, err] = satext::unhexify(data, buffer); err != std::errc{}) {
                    return satext::unexpected{status::parse_error};
                }
                return buffer.subspan(0, data.size() / 2);
            });
    }

private:
    template<typename... Args>
    status request_response(Args&&... args) {
        decltype(buffer_.begin()) end{};
        if constexpr (sizeof...(Args) == 4) {
            end = fmt::format_to(buffer_.begin(), "{} {} {} {}{}", args..., crlf);
        } else if constexpr (sizeof...(Args) == 3) {
            end = fmt::format_to(buffer_.begin(), "{} {} {}{}", args..., crlf);
        }

        send_command({buffer_.begin(), end});
        auto response = read_response().map(map_status);
        return response.has_value() ? response.value() : response.error();
    }

    static status map_status(std::string_view response) {
        if (response == response::ok) {
            return status::ok;
        } else if (response == response::version) {
            return status::version;
        } else if (response == response::busy) {
            return status::busy;
        } else if (response == response::radio_err) {
            return status::radio_error;
        } else if (response == response::radio_tx_ok) {
            return status::radio_tx_ok;
        } else if (response == response::invalid_parameter) {
            return status::invalid_parameter;
        } else {
            return status::parse_error;
        }
    }

    void send_command(std::string_view command) {
        uart_.write(std::as_bytes(std::span(command.data(), command.size())), write_timeout);
    }

    satext::expected<std::string_view, status>
    read_response(satos::chrono::milliseconds timeout = read_timeout) {
        std::ranges::fill(buffer_, 0);
        return uart_
            .read_until(std::as_writable_bytes(std::span(buffer_)), std::byte{new_line}, timeout)
            .map_error([](auto) { return status::parse_error; })
            .and_then([](std::span<const std::byte> result)
                          -> satext::expected<std::span<const std::byte>, status> {
                if (result.back() == std::byte{new_line}) {
                    return result;
                } else {
                    return satext::unexpected{status::parse_error};
                }
            })
            .map([this](std::span<const std::byte> result) {
                return std::string_view{buffer_.data(), result.size()};
            });
    }

    static constexpr auto new_line = std::byte{'\n'};
    static constexpr auto crlf = std::string_view{"\r\n"};
    static constexpr auto reset_sleep = satos::chrono::milliseconds{100};
    static constexpr auto powerup_timeout = satos::chrono::milliseconds{500};
    static constexpr auto write_timeout = satos::chrono::milliseconds{100};
    static constexpr auto read_timeout = satos::chrono::milliseconds{100};
    static constexpr auto tx_timeout = satos::chrono::seconds{3};

    UART& uart_;
    GPIO& reset_;

    std::array<char, 512> buffer_{};
};

/** @} */
} // namespace devices::radio
