#pragma once
#include "devices/radio/configuration.h"
#include "devices/radio/enums.h"
#include "etl/vector.h"
#include "satext/expected.h"
#include "satos/chrono.h"

namespace devices::radio {
// clang-format off
template<typename T>
concept radio_concept = requires(T radio, std::span<const std::byte> const_buffer,
                                 std::span<std::byte> buffer, configuration cfg,
                                 std::string_view sv, std::uint32_t frequency,
                                 std::int32_t power, bandwidth bw,
                                 satos::chrono::milliseconds timeout,
                                 etl::vector<std::byte, 8> sync) {
    { radio.transmit(const_buffer) } -> std::same_as<status>;
    { radio.receive(buffer, timeout) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
    { radio.configure(cfg) } -> std::same_as<bool>;
    { radio.reset() } -> std::same_as<bool>;
    { radio.mac_pause() } -> std::same_as<bool>;
    { radio.set_modulation(sv) } -> std::same_as<status>;
    { radio.set_frequency(frequency) } -> std::same_as<status>;
    { radio.set_power(power) } -> std::same_as<status>;
    { radio.set_spreading_factor(sv) } -> std::same_as<status>;
    { radio.set_bandwidth(bw) } -> std::same_as<status>;
    { radio.set_coding_rate(sv) } -> std::same_as<status>;
    { radio.set_sync(sync) } -> std::same_as<status>;
    { radio.set_iq_inversion(sv) } -> std::same_as<status>;
    { radio.set_watchdog_timeout(timeout) } -> std::same_as<status>;
};
// clang-format on

} // namespace devices::radio
