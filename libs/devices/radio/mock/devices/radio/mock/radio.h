#pragma once
#include "gmock/gmock.h"
#include "devices/radio/radio_concept.h"

namespace devices::radio::mock {

struct radio {
    using receive_result_type = satext::expected<std::span<const std::byte>, status>;
    MOCK_METHOD(status, transmit, (std::span<const std::byte>))
    MOCK_METHOD(receive_result_type, receive, (std::span<std::byte>, satos::chrono::milliseconds))
    MOCK_METHOD(bool, configure, (configuration))
    MOCK_METHOD(bool, reset, ())
    MOCK_METHOD(bool, mac_pause, ())
    MOCK_METHOD(status, set_modulation, (std::string_view));
    MOCK_METHOD(status, set_frequency, (std::uint32_t));
    MOCK_METHOD(status, set_power, (std::int32_t));
    MOCK_METHOD(status, set_spreading_factor, (std::string_view));
    MOCK_METHOD(status, set_bandwidth, (bandwidth));
    MOCK_METHOD(status, set_coding_rate, (std::string_view));
    MOCK_METHOD(status, set_sync, (std::string_view));
    MOCK_METHOD(status, set_iq_inversion, (std::string_view));
    MOCK_METHOD(status, set_watchdog_timeout, (satos::chrono::milliseconds));
};

static_assert(devices::radio::radio_concept<radio>);
} // namespace devices::radio::mock
