#pragma once

namespace devices::temperature_sensor {

/**
 * @ingroup temperature_sensor
 * @{
 */

/**
 * @brief Max31865 error enumeration.
 */
enum class error { device_fault, communication_error };

/** @} */

} // namespace devices::temperature_sensor
