#pragma once
#include "devices/temperature_sensor/enums.h"
#include "satext/expected.h"
#include "satext/struct.h"
#include "satos/chrono.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace devices::temperature_sensor {

/**
 * @ingroup temperature_sensor
 * @{
 */

/**
 * @brief Max31865 Device class.
 *
 *
 * [Max31865
 * Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/max31865.pdf)
 * @tparam GPIO GPIO peripheral type.
 * @tparam SPI SPI driver type.
 */
template<spl::peripherals::gpio::concepts::gpio GPIO,
         spl::drivers::spi::concepts::spi_vectored<GPIO> SPI>
class max31865 {
public:
    /**
     * @brief Constructor.
     * @param spi SPI driver reference.
     * @param gpio Max31865 chip select referecne.
     * @param timeout Device timeout.
     */
    max31865(SPI& spi, GPIO& gpio, satos::chrono::milliseconds timeout) :
        spi_{spi},
        gpio_{gpio},
        timeout_{timeout} {}

    /**
     * @brief Initializes device.
     */
    void initialize() {
        std::array<std::byte, 2> buffer{config_address, config_value};
        std::array<std::span<const std::byte>, 1> in = {buffer};
        spi_.write(gpio_, in, timeout_);
    }

    /**
     * @brief Reads raw temperature.
     * @return Raw temperature on success, error otherwise.
     */
    satext::expected<std::uint16_t, error> read_temperature_raw() {
        using namespace satext::struct_literals;
        std::array<std::byte, 2> buffer{};

        std::array<std::span<const std::byte>, 1> in = {std::span(&rtd_address, 1)};
        std::array<std::span<std::byte>, 1> out = {buffer};

        if (spi_.write_read(gpio_, in, out, timeout_) != spl::drivers::spi::status::ok) {
            return satext::unexpected{error::communication_error};
        }

        return satext::unpack(">H"_fmt, buffer)
            .map_error([](auto) { return error::communication_error; })
            .and_then([](auto unpacked) -> satext::expected<std::uint16_t, error> {
                auto [raw] = unpacked;
                if ((raw & fault) != 0) {
                    return satext::unexpected{error::device_fault};
                }
                return raw >> rtd_shift;
            });
    }

    /**
     * @brief Reads temperature in 1/10th of degrees Celsius, with 0.1 degree resolution.
     * @return Temperature on success, error otherwise.
     */
    satext::expected<float, error> read_temperature() {
        return read_temperature_raw().map([](std::uint16_t value) {
            auto resistance = static_cast<float>(value) / max_value * reference;
            return a + (b + (c * resistance)) * resistance;
        });
    }

private:
    SPI& spi_;
    GPIO& gpio_;
    satos::chrono::milliseconds timeout_;

    static constexpr auto config_address = std::byte{0x80};
    static constexpr auto config_value = std::byte{0xd1};
    static constexpr auto rtd_address = std::byte{0x01};
    static constexpr auto rtd_shift = 1u;

    static constexpr float max_value = 32768.0f;
    static constexpr float reference = 430.0f;
    static constexpr float a = -242.97f;
    static constexpr float c = 0.0014727f;
    static constexpr float b = 2.2838f;

    static constexpr std::uint16_t fault = 0x1;
};

/* @} */

} // namespace devices::temperature_sensor
