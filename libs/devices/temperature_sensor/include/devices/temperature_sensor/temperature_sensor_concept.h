#pragma once
#include <concepts>
#include <cstdint>
#include "devices/temperature_sensor/enums.h"
#include "satext/expected.h"

namespace devices::temperature_sensor {

// clang-format off
template<typename T>
concept temperature_sensor_raw_concept = requires(T temperature_sensor) {
    {temperature_sensor.read_temperature_raw() };
};

template<typename T>
concept temperature_sensor_converted_concept = requires(T temperature_sensor) {
    {temperature_sensor.read_temperature() };
};


template<typename T>
concept temperature_sensor_concept = temperature_sensor_raw_concept<T> || temperature_sensor_converted_concept<T>;
// clang-format on
} // namespace devices::temperature_sensor
