#pragma once
#include "gmock/gmock.h"
#include "devices/temperature_sensor/temperature_sensor_concept.h"

namespace devices::temperature_sensor::mock {

struct temperature_sensor {
    using raw_result = satext::expected<std::uint16_t, devices::temperature_sensor::error>;
    using result = satext::expected<float, devices::temperature_sensor::error>;

    MOCK_METHOD(raw_result, read_temperature_raw, ());
    MOCK_METHOD(result, read_temperature, ());
};

static_assert(devices::temperature_sensor::temperature_sensor_concept<temperature_sensor>);
} // namespace devices::temperature_sensor::mock
