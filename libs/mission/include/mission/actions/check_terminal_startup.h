#pragma once
#include "mission/enums.h"
#include "terminal/concepts/terminal.h"

namespace mission::actions {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Check terminal startup action.
 * @tparam Terminal Terminal type.
 */
template<terminal::concepts::terminal Terminal>
struct check_terminal_startup {
    /**
     * @brief Check terminal startup action.
     * @param terminal Reference to terminal.
     * @param state Reference to terminal state.
     */
    void operator()(Terminal& terminal, terminal_state& state) const {
        std::byte data;
        auto result = terminal.read_line(std::span(&data, 1), satos::chrono::milliseconds{90});
        if (result.has_value()) {
            state = terminal_state::started;
        }
    }
};

/** @} */
} // namespace mission::actions
