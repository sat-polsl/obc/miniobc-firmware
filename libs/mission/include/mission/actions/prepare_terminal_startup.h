#pragma once
#include "mission/enums.h"
#include "satos/clock.h"
#include "terminal/concepts/terminal.h"

namespace mission::actions {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Prepare terminal startup action.
 * @tparam Terminal Terminal type.
 */
template<terminal::concepts::terminal Terminal>
struct prepare_terminal_startup {
    /**
     * @brief Prepare terminal startup action.
     * @param terminal_startup_time Reference to terminal startup time.
     * @param terminal_state Reference to terminal state.
     * @param terminal Reference to terminal.
     */
    void operator()(satos::clock::time_point& terminal_startup_time,
                    terminal_state& terminal_state,
                    Terminal& terminal) const {
        terminal_startup_time = satos::clock::now();
        terminal_state = terminal_state::not_started;
        terminal.print("Press enter within 3 seconds to start terminal!\n");
    }
};

/** @} */
} // namespace mission::actions
