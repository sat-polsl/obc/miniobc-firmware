#pragma once
#include "satos/concepts/thread.h"
#include "terminal/concepts/terminal.h"
#include "watchdog/concepts/watchdog.h"

namespace mission::actions {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Start mission action.
 * @tparam GpsThread GPS thread type.
 * @tparam Terminal Terminal type.
 * @tparam HwWatchdog Hardware watchdog type.
 * @tparam EpsWatchdog EPS watchdog type.
 */
template<satos::concepts::thread GpsThread,
         terminal::concepts::terminal Terminal,
         typename HwWatchdog,
         typename EpsWatchdog>
    requires(watchdog::concepts::watchdog<typename HwWatchdog::value> &&
             watchdog::concepts::watchdog<typename EpsWatchdog::value>)
struct start_mission {
    /**
     * @brief Start mission action.
     * @param gps_thread Reference to GPS thread.
     * @param terminal Reference to terminal.
     */
    void operator()(GpsThread& gps_thread,
                    Terminal& terminal,
                    HwWatchdog hw_watchdog,
                    EpsWatchdog eps_watchdog) const {
        terminal.print("Starting mission!\n");
        gps_thread.start();
        hw_watchdog->register_thread(satos::chrono::seconds{5}, gps_thread.native_handle());
        eps_watchdog->register_thread(satos::chrono::seconds{5}, gps_thread.native_handle());
    }
};

/** @} */
} // namespace mission::actions
