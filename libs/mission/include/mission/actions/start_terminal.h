#pragma once
#include "satos/concepts/thread.h"

namespace mission::actions {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Start terminal action.
 * @tparam TerminalThread Terminal thread type.
 */
template<satos::concepts::thread TerminalThread>
struct start_terminal {

    /**
     * @brief Start terminal action.
     * @param terminal Reference to terminal.
     */
    void operator()(TerminalThread& terminal) const { terminal.start(); }
};

/** @} */
} // namespace mission::actions
