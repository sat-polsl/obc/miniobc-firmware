#pragma once

namespace mission {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Terminal state enumeration.
 */
enum class terminal_state { not_started, started, timeout };

/** @} */
} // namespace mission
