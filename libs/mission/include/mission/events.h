#pragma once

namespace mission {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Tick event.
 */
struct tick {};

/** @} */
} // namespace mission
