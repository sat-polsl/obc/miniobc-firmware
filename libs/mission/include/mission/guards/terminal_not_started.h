#pragma once
#include "mission/enums.h"

namespace mission::guards {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Terminal not started guard.
 */
struct terminal_not_started {
    /**
     * @brief Terminal not started guard.
     * @param state Reference to terminal state.
     * @return True if terminal not started yet, false otherwise.
     */
    bool operator()(const terminal_state& state) const {
        return state == terminal_state::not_started;
    }
};

/** @} */
} // namespace mission::guards
