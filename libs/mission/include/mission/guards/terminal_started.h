#pragma once
#include "mission/enums.h"

namespace mission::guards {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Terminal started guard.
 */
struct terminal_started {
    /**
     * @brief Terminal started guard.
     * @param state Reference to terminal state.
     * @return True if terminal started, false otherwise.
     */
    bool operator()(const terminal_state& state) const { return state == terminal_state::started; }
};

/** @} */

} // namespace mission::guards
