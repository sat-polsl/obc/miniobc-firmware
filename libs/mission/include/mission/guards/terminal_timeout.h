#pragma once
#include "satos/clock.h"

namespace mission::guards {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Terminal timeout guard.
 */
struct terminal_timeout {

    static constexpr auto terminal_startup_timeout = satos::chrono::seconds{3};

    /**
     * @brief Terminal timeout guard.
     * @param terminal_startup_time Reference to terminal startup time.
     * @return True if terminal startup time is expired, false otherwise.
     */
    bool operator()(const satos::clock::time_point& terminal_startup_time) const {
        return satos::clock::now() > terminal_startup_time + terminal_startup_timeout;
    }
};

/** @} */
} // namespace mission::guards
