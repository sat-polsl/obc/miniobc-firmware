#pragma once
#include "boost/sml.hpp"
#include "mission/events.h"
#include "mission/states.h"
#include "satos/clock.h"
#include "satos/concepts/thread.h"
#include "terminal/concepts/terminal.h"
#include "watchdog/concepts/watchdog.h"

#include "mission/actions/check_terminal_startup.h"
#include "mission/actions/prepare_terminal_startup.h"
#include "mission/actions/start_mission.h"
#include "mission/actions/start_terminal.h"

#include "mission/guards/terminal_not_started.h"
#include "mission/guards/terminal_started.h"
#include "mission/guards/terminal_timeout.h"

namespace mission {

/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Mission state machine.
 * @tparam GpsThread GPS Thread type.
 * @tparam Terminal Terminal type.
 * @tparam TerminalThread Terminal thread type.
 * @tparam HwWatchdog Hardware watchdog type.
 * @tparam EpsWatchdog EPS watchdog type.
 */
template<satos::concepts::thread GpsThread,
         terminal::concepts::terminal Terminal,
         satos::concepts::thread TerminalThread,
         typename HwWatchdog,
         typename EpsWatchdog>
    requires(watchdog::concepts::watchdog<typename HwWatchdog::value> &&
             watchdog::concepts::watchdog<typename EpsWatchdog::value>)
class mission_state {
public:
    using prepare_terminal_startup = actions::prepare_terminal_startup<Terminal>;
    using check_terminal_startup = actions::check_terminal_startup<Terminal>;
    using start_mission = actions::start_mission<GpsThread, Terminal, HwWatchdog, EpsWatchdog>;
    using start_terminal = actions::start_terminal<TerminalThread>;

    /**
     * @brief Returns transition table.
     * @return Transition table.
     */
    auto operator()() const {
        using namespace boost::sml;
        // clang-format off
        return make_transition_table(
            *state<start> + event<tick> / prepare_terminal_startup{} = state<terminal_startup>,
            state<terminal_startup> + event<tick> [guards::terminal_timeout{} ] / start_mission{} = state<idle>,
            state<terminal_startup> + event<tick> [guards::terminal_started{} ] / start_terminal{} = state<idle>,
            state<terminal_startup> + event<tick> [guards::terminal_not_started{}] / check_terminal_startup{} = state<terminal_startup>,
            state<idle> + event<tick> = state<idle>
            );
        // clang-format on
    }
};

/** @} */

} // namespace mission
