#pragma once

namespace mission {
/**
 * @ingroup mission
 * @{
 */

/**
 * @brief Starting state.
 */
struct start {};

/**
 * @brief Idle state.
 */
struct idle {};

/**
 * @brief Terminal startup state.
 */
struct terminal_startup {};

/** @} */
} // namespace mission
