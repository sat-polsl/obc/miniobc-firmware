#pragma once
#include <cstdint>
#include <memory>
#include <mutex>
#include <variant>
#include "etl/pool.h"
#include "satext/expected.h"
#include "satos/mutex.h"

namespace object_pool {

/**
 * @ingroup object_pool
 * @{
 */

/**
 * @brief Object pool.
 * @tparam T Type of object.
 * @tparam Size Size of the pool.
 */
template<typename T, std::size_t Size>
class object_pool {
private:
    class deleter {
    public:
        deleter() = default;
        explicit deleter(etl::pool<T, Size>& pool, satos::mutex& mtx) : pool_{&pool}, mtx_{&mtx} {}
        deleter(const deleter&) = default;
        deleter& operator=(const deleter&) = default;
        deleter(deleter&&) noexcept = default;
        deleter& operator=(deleter&&) noexcept = default;
        ~deleter() = default;
        void operator()(T* ptr) {
            std::lock_guard lck{*mtx_};
            pool_->destroy(ptr);
        }

    private:
        etl::pool<T, Size>* pool_{};
        satos::mutex* mtx_{};
    };

public:
    using value = T;
    using unique_ptr = std::unique_ptr<value, deleter>;

    /**
     * @brief Constructor.
     */
    object_pool() = default;

    /**
     * @brief Constructs objects. The arguments are passed to the constructor of the object.
     * @tparam Args Types of passed arguments.
     * @param args List of arguments with which an instance of the object will be constructed.
     * @return `std::unique_ptr` owning constructed object on success, `std::monostate` otherwise.
     */
    template<typename... Args>
    satext::expected<unique_ptr, std::monostate> make_unique(Args&&... args) {
        std::lock_guard lck{mtx_};
        auto* ptr = pool_.create(std::forward<Args>(args)...);
        if (ptr) {
            return unique_ptr{ptr, deleter{pool_, mtx_}};
        } else {
            return satext::unexpected{std::monostate{}};
        }
    }

private:
    etl::pool<T, Size> pool_;
    satos::mutex mtx_;
};

/** @} */

} // namespace object_pool
