#pragma once
#include "devices/eps/eps_concept.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry::samplers {
/**
 * @ingroup telemetry_samplers
 * @{
 */

/**
 * @brief EPS sampler configuration.
 */
struct eps_sampler_config {
    ::telemetry::telemetry_id boot_counter;
    ::telemetry::telemetry_id uptime;
    ::telemetry::telemetry_id temperature_3v3;
    ::telemetry::telemetry_id temperature_5v;
    ::telemetry::telemetry_id temperature_12v;
    ::telemetry::telemetry_id temperature_cell1;
    ::telemetry::telemetry_id temperature_cell2;
    ::telemetry::telemetry_id temperature_cell3;
    ::telemetry::telemetry_id v3v3v_l2;
    ::telemetry::telemetry_id i3v3v_l2;
    ::telemetry::telemetry_id v5v_l1;
    ::telemetry::telemetry_id i5v_l1;
    ::telemetry::telemetry_id vbms;
    ::telemetry::telemetry_id ibms;
};

/**
 * @brief Samples EPS telemetry to telemetry state.
 * @tparam TelemetryState Telemetry State type.
 * @tparam BootCounterId Boot counter telemetry ID.
 * @tparam UptimeId Uptime telemetry ID.
 */
template<devices::eps::eps_concept EPS,
         telemetry_state_writable TelemetryState,
         eps_sampler_config Config>
class eps_sampler {
public:
    explicit eps_sampler(EPS& eps) : eps_{eps} {}

    using state = TelemetryState;
    void operator()(TelemetryState& state) {
        eps_.get_telemetry(devices::eps::telemetry_id::boot_counter).map([&state](auto value) {
            state.write(Config.boot_counter, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::uptime).map([&state](auto value) {
            state.write(Config.uptime, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::temperature_3v3).map([&state](auto value) {
            state.write(Config.temperature_3v3, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::temperature_5v).map([&state](auto value) {
            state.write(Config.temperature_5v, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::temperature_12v).map([&state](auto value) {
            state.write(Config.temperature_12v, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::temperature_cell1).map([&state](auto value) {
            state.write(Config.temperature_cell1, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::temperature_cell2).map([&state](auto value) {
            state.write(Config.temperature_cell2, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::temperature_cell3).map([&state](auto value) {
            state.write(Config.temperature_cell3, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::v3v3v_l2).map([&state](auto value) {
            state.write(Config.v3v3v_l2, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::i3v3v_l2).map([&state](auto value) {
            state.write(Config.i3v3v_l2, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::v5v_l1).map([&state](auto value) {
            state.write(Config.v5v_l1, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::i5v_l1).map([&state](auto value) {
            state.write(Config.i5v_l1, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::vbms).map([&state](auto value) {
            state.write(Config.vbms, value);
        });
        eps_.get_telemetry(devices::eps::telemetry_id::ibms).map([&state](auto value) {
            state.write(Config.ibms, value);
        });
    }

private:
    EPS& eps_;
};

/** @} */

} // namespace telemetry::samplers
