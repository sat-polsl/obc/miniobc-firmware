#pragma once
#include "devices/muons/muons_concept.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry::samplers {
/**
 * @ingroup telemetry_samplers
 * @{
 */

/**
 * @brief Samples muons count to telemetry state.
 * @tparam TelemetryState Telemetry State type.
 * @tparam Id Telemetry ID.
 */
template<devices::muons::muons_concept Muons,
         telemetry_state_writable TelemetryState,
         ::telemetry::telemetry_id Id>
class muons_sampler {
public:
    explicit muons_sampler(Muons& muons) : muons_{muons} {}

    using state = TelemetryState;
    void operator()(TelemetryState& state) {
        muons_.read_count()
            .map([&state](auto value) { state.write(Id, value); })
            .map_error([&state](auto) { state.write(Id, std::monostate{}); });
    }

private:
    Muons& muons_;
};

/** @} */

} // namespace telemetry::samplers
