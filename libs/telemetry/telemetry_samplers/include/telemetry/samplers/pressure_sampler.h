#pragma once
#include "devices/mprls/mprls_concept.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry::samplers {
/**
 * @ingroup telemetry_samplers
 * @{
 */

/**
 * @brief Samples pressure value to telemetry state.
 * @tparam TelemetryState Telemetry State type.
 * @tparam Id Telemetry ID.
 */
template<devices::mprls::mprls_concept Mprls,
         telemetry_state_writable TelemetryState,
         ::telemetry::telemetry_id Id>
class pressure_sampler {
public:
    explicit pressure_sampler(Mprls& mprls) : mprls_{mprls} {}

    using state = TelemetryState;
    void operator()(TelemetryState& state) {
        mprls_.read_pressure()
            .map([&state](auto value) { state.write(Id, value); })
            .map_error([&state](auto) { state.write(Id, std::monostate{}); });
    }

private:
    Mprls& mprls_;
};

/** @} */

} // namespace telemetry::samplers
