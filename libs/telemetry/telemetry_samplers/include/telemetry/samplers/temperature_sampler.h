#pragma once
#include "devices/temperature_sensor/temperature_sensor_concept.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry::samplers {
/**
 * @ingroup telemetry_samplers
 * @{
 */

/**
 * @brief Samples temperature value to telemetry state.
 * @tparam TemperatureSensor Temperature Sensor type.
 * @tparam TelemetryState Telemetry State type.
 * @tparam Id Telemetry ID.
 */
template<devices::temperature_sensor::temperature_sensor_concept TemperatureSensor,
         telemetry_state_writable TelemetryState,
         ::telemetry::telemetry_id Id>
class temperature_sampler {
public:
    using state = TelemetryState;

    /**
     * @brief Constructor.
     * @param temperatureSensor Temperature sensor reference.
     */
    explicit temperature_sampler(TemperatureSensor& temperatureSensor) :
        temperatureSensor_{temperatureSensor} {}

    /**
     * @brief Samples telemetry from Max31865 and writes value to Telemetry State.
     * @param state Reference to Telemetry State.
     */
    void operator()(TelemetryState& state) {
        temperatureSensor_.read_temperature()
            .map([&state](auto value) { state.write(Id, value); })
            .map_error([&state](auto) { state.write(Id, std::monostate{}); });
    }

private:
    TemperatureSensor& temperatureSensor_;
};

/** @} */

} // namespace telemetry::samplers
