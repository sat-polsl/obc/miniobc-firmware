#define ETL_NO_CHECKS
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/can/can_context.h"
#include "satos/chrono.h"
#include "satos/clock.h"
#include "satos/mock/clock.h"
#include "satos/mock/mutex.h"
#include "spl/drivers/can/mock/can.h"

namespace {
using namespace testing;
using namespace communication::can;
using namespace satos::chrono_literals;

using context_type = can_context<spl::drivers::can::mock::can, 1>;

static constexpr std::size_t ongoing_id = 1;

struct CanContextTest : Test {
    StrictMock<spl::drivers::can::mock::can> can_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

TEST_F(CanContextTest, SendSuccess) {
    context_type context{can_};
    spl::peripherals::can::concepts::message expected_msg{};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(can_, write_mock(_, _))
        .WillOnce(Invoke([&expected_msg](const spl::peripherals::can::concepts::message& msg,
                                         satos::clock::duration timeout) {
            EXPECT_THAT(&expected_msg, Eq(&msg));
            EXPECT_THAT(timeout, Eq(satos::clock::duration::max()));
            return spl::drivers::can::status::ok;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    context.send(expected_msg, 0, [](auto, const auto&) {});
}

TEST_F(CanContextTest, SendFailure) {
    context_type context{can_};
    std::uint32_t callback_counter{};
    spl::peripherals::can::concepts::message expected_msg{};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(can_, write_mock(_, _))
        .WillOnce(Invoke([&expected_msg](const spl::peripherals::can::concepts::message& msg,
                                         satos::clock::duration timeout) {
            EXPECT_THAT(&expected_msg, Eq(&msg));
            EXPECT_THAT(timeout, Eq(satos::clock::duration::max()));
            return spl::drivers::can::status::timeout;
        }));

    context.send(expected_msg, 0, [&callback_counter](auto status, const auto&) {
        ASSERT_THAT(status, Eq(status::timeout));
        callback_counter++;
    });

    ASSERT_THAT(callback_counter, Eq(1));
}

TEST_F(CanContextTest, SendNoMemory) {
    context_type context{can_};
    std::uint32_t callback_counter{};
    spl::peripherals::can::concepts::message expected_msg{};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(can_, write_mock(_, _))
        .WillOnce(Invoke([&expected_msg](const spl::peripherals::can::concepts::message& msg,
                                         satos::clock::duration timeout) {
            EXPECT_THAT(&expected_msg, Eq(&msg));
            EXPECT_THAT(timeout, Eq(satos::clock::duration::max()));
            return spl::drivers::can::status::ok;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));

    context.send(expected_msg, 0, [](auto status, const auto&) { ASSERT_THAT(true, Eq(false)); });

    context.send(expected_msg, 0, [&callback_counter](auto status, const auto&) {
        ASSERT_THAT(status, Eq(status::no_memory));
        callback_counter++;
    });

    ASSERT_THAT(callback_counter, Eq(1));
}

TEST_F(CanContextTest, RunOne) {
    context_type context{can_};
    spl::peripherals::can::concepts::message expected_msg{};

    InSequence s;
    EXPECT_CALL(can_, read_mock(spl::peripherals::can::concepts::rx_fifo{0}, 100_ms))
        .WillOnce(Return(expected_msg));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    context.run_one();
}

TEST_F(CanContextTest, TransactionSuccess) {
    context_type context{can_};
    std::uint32_t callback_counter{};
    spl::peripherals::can::concepts::message expected_msg{};
    expected_msg.data = {std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
    expected_msg.size = 4;

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(can_, write_mock(_, _))
        .WillOnce(Invoke([&expected_msg](const spl::peripherals::can::concepts::message& msg,
                                         satos::clock::duration timeout) {
            EXPECT_THAT(&expected_msg, Eq(&msg));
            EXPECT_THAT(timeout, Eq(satos::clock::duration::max()));
            return spl::drivers::can::status::ok;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    EXPECT_CALL(can_, read_mock(spl::peripherals::can::concepts::rx_fifo{0}, 100_ms))
        .WillOnce(Invoke([](auto, auto) {
            spl::peripherals::can::concepts::message result;
            result.data = {std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
            result.size = 4;
            result.id = 0xbeef;
            return result;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    context.send(expected_msg,
                 0xbeef,
                 [&callback_counter, data = expected_msg.data](auto status, const auto& msg) {
                     ASSERT_THAT(status, Eq(status::ok));
                     ASSERT_THAT(msg.data, ContainerEq(data));
                     ASSERT_THAT(msg.size, Eq(4));
                     ASSERT_THAT(msg.id, Eq(0xbeef));
                     callback_counter++;
                 });

    context.run_one();
    ASSERT_THAT(callback_counter, Eq(1));
}

TEST_F(CanContextTest, TransactionNoMatchingId) {
    context_type context{can_};
    std::uint32_t callback_counter{};
    spl::peripherals::can::concepts::message expected_msg{};
    expected_msg.data = {std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
    expected_msg.size = 4;

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(can_, write_mock(_, _))
        .WillOnce(Invoke([&expected_msg](const spl::peripherals::can::concepts::message& msg,
                                         satos::clock::duration timeout) {
            EXPECT_THAT(&expected_msg, Eq(&msg));
            EXPECT_THAT(timeout, Eq(1000_ms));
            return spl::drivers::can::status::ok;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    EXPECT_CALL(can_, read_mock(spl::peripherals::can::concepts::rx_fifo{0}, 100_ms))
        .WillOnce(Invoke([](auto, auto) {
            spl::peripherals::can::concepts::message result;
            result.data = {std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
            result.size = 4;
            result.id = 0xbeef;
            return result;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    context.send(
        expected_msg, 0xbeee, [&callback_counter](auto, const auto&) { callback_counter++; }, 1_s);

    context.run_one();
    ASSERT_THAT(callback_counter, Eq(0));
}

TEST_F(CanContextTest, TransactionTimeout) {
    context_type context{can_};
    std::uint32_t callback_counter{};
    spl::peripherals::can::concepts::message expected_msg{};
    expected_msg.data = {std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};
    expected_msg.size = 4;

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(can_, write_mock(_, _))
        .WillOnce(Invoke([&expected_msg](const spl::peripherals::can::concepts::message& msg,
                                         satos::clock::duration timeout) {
            EXPECT_THAT(&expected_msg, Eq(&msg));
            EXPECT_THAT(timeout, Eq(1000_ms));
            return spl::drivers::can::status::ok;
        }));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    EXPECT_CALL(can_, read_mock(spl::peripherals::can::concepts::rx_fifo{0}, 100_ms))
        .WillOnce(Return(satext::unexpected{spl::drivers::can::status::timeout}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], lock());
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{3000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[ongoing_id], unlock());

    context.send(
        expected_msg,
        0xbeef,
        [&callback_counter](auto status, const auto&) {
            ASSERT_THAT(status, Eq(status::timeout));
            callback_counter++;
        },
        1_s);

    context.run_one();
    ASSERT_THAT(callback_counter, Eq(1));
}

} // namespace
