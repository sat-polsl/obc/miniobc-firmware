#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/radio/beacon/encode.h"
#include "telemetry/mock/telemetry_state.h"

namespace {
using namespace testing;
using telemetry::telemetry_id;

TEST(CommunicationRadioBeacon, Encode) {
    std::array<std::byte, 44> buffer{};
    telemetry::mock::telemetry_state state;

    std::array<telemetry_id, 7> ids{telemetry_id{0},
                                    telemetry_id{1},
                                    telemetry_id{2},
                                    telemetry_id{4},
                                    telemetry_id{5},
                                    telemetry_id{6},
                                    telemetry_id{7}};

    EXPECT_CALL(state, read(telemetry_id{0})).WillRepeatedly(Return(std::uint32_t{0xdeadc0de}));
    EXPECT_CALL(state, read(telemetry_id{1})).WillRepeatedly(Return(std::uint32_t{0xdeadbeef}));
    EXPECT_CALL(state, read(telemetry_id{2})).WillRepeatedly(Return(std::uint32_t{0x12345678}));
    EXPECT_CALL(state, read(telemetry_id{4})).WillRepeatedly(Return(float{51.25f}));
    EXPECT_CALL(state, read(telemetry_id{5})).WillRepeatedly(Return(float{18.125f}));
    EXPECT_CALL(state, read(telemetry_id{6})).WillRepeatedly(Return(float{29000.0f}));
    EXPECT_CALL(state, read(telemetry_id{7})).WillRepeatedly(Return(float{-45.25f}));

    auto result = communication::radio::beacon::encode(ids, buffer, state);

    ASSERT_THAT(result, Eq(44));
    ASSERT_THAT(buffer[0], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0x2a}));

    ASSERT_THAT(buffer[2], Eq(std::byte{0x03}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[6], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0xde}));

    ASSERT_THAT(buffer[8], Eq(std::byte{0x13}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[10], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[11], Eq(std::byte{0xbe}));
    ASSERT_THAT(buffer[12], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[13], Eq(std::byte{0xde}));

    ASSERT_THAT(buffer[14], Eq(std::byte{0x23}));
    ASSERT_THAT(buffer[15], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[16], Eq(std::byte{0x78}));
    ASSERT_THAT(buffer[17], Eq(std::byte{0x56}));
    ASSERT_THAT(buffer[18], Eq(std::byte{0x34}));
    ASSERT_THAT(buffer[19], Eq(std::byte{0x12}));

    ASSERT_THAT(buffer[20], Eq(std::byte{0x48}));
    ASSERT_THAT(buffer[21], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[22], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[23], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[24], Eq(std::byte{0x4d}));
    ASSERT_THAT(buffer[25], Eq(std::byte{0x42}));

    ASSERT_THAT(buffer[26], Eq(std::byte{0x58}));
    ASSERT_THAT(buffer[27], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[28], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[29], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[30], Eq(std::byte{0x91}));
    ASSERT_THAT(buffer[31], Eq(std::byte{0x41}));

    ASSERT_THAT(buffer[32], Eq(std::byte{0x68}));
    ASSERT_THAT(buffer[33], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[34], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[35], Eq(std::byte{0x90}));
    ASSERT_THAT(buffer[36], Eq(std::byte{0xe2}));
    ASSERT_THAT(buffer[37], Eq(std::byte{0x46}));

    ASSERT_THAT(buffer[38], Eq(std::byte{0x78}));
    ASSERT_THAT(buffer[39], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[40], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[41], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[42], Eq(std::byte{0x35}));
    ASSERT_THAT(buffer[43], Eq(std::byte{0xc2}));
}
} // namespace
