#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/radio/header/encode.h"

namespace {
using namespace testing;

TEST(CommunicationRadioHeaderTest, Encode) {
    std::array<std::byte, 8> buffer{};
    auto size = communication::radio::header::encode(buffer, 100);

    ASSERT_THAT(size, Eq(size));
    ASSERT_THAT(buffer[0], Eq(std::byte('s')));
    ASSERT_THAT(buffer[1], Eq(std::byte('p')));
    ASSERT_THAT(buffer[2], Eq(std::byte('9')));
    ASSERT_THAT(buffer[3], Eq(std::byte('f')));
    ASSERT_THAT(buffer[4], Eq(std::byte('t')));
    ASSERT_THAT(buffer[5], Eq(std::byte('l')));
    ASSERT_THAT(buffer[6], Eq(std::byte(1)));
    ASSERT_THAT(buffer[7], Eq(std::byte(100)));
}

TEST(CommunicationRadioHeaderTest, EncodeBufferTooSmall) {
    std::array<std::byte, 4> buffer{};
    auto size = communication::radio::header::encode(buffer, 100);

    ASSERT_THAT(size, Eq(0));
}

} // namespace
