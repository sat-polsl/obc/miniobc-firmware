#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/radio/payload/payload_view.h"

namespace {
using namespace testing;

TEST(CommunicationRadioPayload, Getters) {
    std::array<std::byte, 4> buffer{std::byte{10}, std::byte{20}, std::byte{30}, std::byte{40}};

    communication::radio::payload::payload_view payload{buffer};

    ASSERT_THAT(payload.id(), Eq(communication::radio::payload::id{10}));
    ASSERT_THAT(payload.size(), Eq(20));
    ASSERT_THAT(payload.data()[0], Eq(std::byte{30}));
    ASSERT_THAT(payload.data()[1], Eq(std::byte{40}));
}

TEST(CommunicationRadioPayload, Setters) {
    std::array<std::byte, 4> buffer{};

    communication::radio::payload::payload_view payload{buffer};

    payload.set_id(communication::radio::payload::id{10});
    payload.set_size(20);
    payload.data()[0] = std::byte{30};
    payload.data()[1] = std::byte{40};

    ASSERT_THAT(buffer[0], Eq(std::byte{10}));
    ASSERT_THAT(buffer[1], Eq(std::byte{20}));
    ASSERT_THAT(buffer[2], Eq(std::byte{30}));
    ASSERT_THAT(buffer[3], Eq(std::byte{40}));
}

} // namespace
