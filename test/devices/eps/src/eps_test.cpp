#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "communication/can/mock/can_context.h"
#include "devices/eps/eps.h"
#include "devices/eps/eps_concept.h"
#include "satos/chrono.h"
#include "satos/mock/clock.h"
#include "satos/mock/event.h"
#include "satos/mock/mutex.h"

namespace {
using namespace testing;
using namespace devices::eps;
using namespace satos::chrono_literals;

using time_point = satos::clock::time_point;

using eps_type = eps<communication::can::mock::can_context>;
static_assert(eps_concept<eps_type>);

struct EpsTest : Test {
    StrictMock<communication::can::mock::can_context> can_context_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

TEST_F(EpsTest, PowerOutputOnSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_on::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_on::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x04}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::power_on::response, .data = {std::byte{0x04}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.power_output_on(power_output::v5_l1);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(EpsTest, PowerOutputOnMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.power_output_on(power_output::v5_l1);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, PowerOutputOnSendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_on::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_on::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::power_on::response, .data = {std::byte{0x00}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.power_output_on(power_output::v3v3_l1);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, PowerOutputOnSendCommunicationFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_on::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_on::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x05}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::power_on::response, .data = {std::byte{0x04}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.power_output_on(power_output::v5_l2);
    ASSERT_THAT(result, Eq(status::communication_error));
}

TEST_F(EpsTest, PowerOutputOnEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_on::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_on::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x05}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {
        .id = id::power_on::response, .data = {std::byte{0x05}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.power_output_on(power_output::v5_l2);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, PowerOutputOffSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_off::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_off::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x06}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::power_off::response, .data = {std::byte{0x06}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.power_output_off(power_output::v12);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(EpsTest, PowerOutputOffMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.power_output_off(power_output::v12);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, PowerOutputOffSendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_off::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_off::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x02}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::power_off::response, .data = {std::byte{0x02}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.power_output_off(power_output::v3v3_l3);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, PowerOutputOffSendCommunicationFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_off::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_off::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x01}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::power_off::response, .data = {std::byte{0x04}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.power_output_off(power_output::v3v3_l2);
    ASSERT_THAT(result, Eq(status::communication_error));
}

TEST_F(EpsTest, PowerOutputOffEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::power_off::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::power_off::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x01}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {
        .id = id::power_off::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.power_output_off(power_output::v3v3_l2);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, EnableWatchdogSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::enable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::enable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::enable_watchdog::response, .data = {std::byte{0x00}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.enable_watchdog(power_output::v3v3_l1);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(EpsTest, EnableWatchdogMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.enable_watchdog(power_output::v3v3_l1);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, EnableWatchdogSendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::enable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::enable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x02}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::enable_watchdog::response, .data = {std::byte{0x02}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.enable_watchdog(power_output::v3v3_l3);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, EnableWatchdogSendCommunicationFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::enable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::enable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x06}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::enable_watchdog::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.enable_watchdog(power_output::v12);
    ASSERT_THAT(result, Eq(status::communication_error));
}

TEST_F(EpsTest, EnableWatchdogEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::enable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::enable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x02}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {
        .id = id::enable_watchdog::response, .data = {std::byte{0x02}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.enable_watchdog(power_output::v3v3_l3);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, DisableWatchdogSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::disable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::disable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x04}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::disable_watchdog::response, .data = {std::byte{0x04}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.disable_watchdog(power_output::v5_l1);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(EpsTest, DisableWatchdogMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.disable_watchdog(power_output::v3v3_l1);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, DisableWatchdogSendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::disable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::disable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x03}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::disable_watchdog::response, .data = {std::byte{0x03}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.disable_watchdog(power_output::v3v3_l4);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, DisableWatchdogSendCommunicationFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::disable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::disable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x06}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::disable_watchdog::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.disable_watchdog(power_output::v12);
    ASSERT_THAT(result, Eq(status::communication_error));
}

TEST_F(EpsTest, DisableWatchdogEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::disable_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::disable_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x05}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {
        .id = id::disable_watchdog::response, .data = {std::byte{0x05}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.disable_watchdog(power_output::v5_l2);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, KickWatchdogSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::kick_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::kick_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x06}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::kick_watchdog::response, .data = {std::byte{0x06}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.kick_watchdog(power_output::v12);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(EpsTest, KickWatchdogMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.kick_watchdog(power_output::v3v3_l1);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, KickWatchdogSendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::kick_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::kick_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x01}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::kick_watchdog::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.kick_watchdog(power_output::v3v3_l2);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, KickWatchdogSendCommunicationFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::kick_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::kick_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x04}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::kick_watchdog::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.kick_watchdog(power_output::v5_l1);
    ASSERT_THAT(result, Eq(status::communication_error));
}

TEST_F(EpsTest, KickWatchdogEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::kick_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::kick_watchdog::request));
            EXPECT_THAT(message.size, Eq(1));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x02}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {
        .id = id::kick_watchdog::response, .data = {std::byte{0x02}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.kick_watchdog(power_output::v3v3_l3);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, SetWatchdogTimeoutWatchdogSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::set_watchdog_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::set_watchdog_watchdog::request));
            EXPECT_THAT(message.size, Eq(3));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x05}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x12}));
            EXPECT_THAT(message.data[2], Eq(std::byte{0x34}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::set_watchdog_watchdog::response, .data = {std::byte{0x05}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.set_watchdog_timeout(power_output::v5_l2, 4660_s);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(EpsTest, SetWatchdogTimeoutMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.set_watchdog_timeout(power_output::v3v3_l1, 1234_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, SetWatchdogTimeoutSendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::set_watchdog_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::set_watchdog_watchdog::request));
            EXPECT_THAT(message.size, Eq(3));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x06}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0xde}));
            EXPECT_THAT(message.data[2], Eq(std::byte{0xad}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::set_watchdog_watchdog::response, .data = {std::byte{0x06}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.set_watchdog_timeout(power_output::v12, 57005_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, SetWatchdogTimeoutSendCommunicationFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::set_watchdog_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::set_watchdog_watchdog::request));
            EXPECT_THAT(message.size, Eq(3));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x05}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0xbe}));
            EXPECT_THAT(message.data[2], Eq(std::byte{0xef}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::set_watchdog_watchdog::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.set_watchdog_timeout(power_output::v5_l2, 48879_s);
    ASSERT_THAT(result, Eq(status::communication_error));
}

TEST_F(EpsTest, SetWatchdogTimeoutEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::set_watchdog_watchdog::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::set_watchdog_watchdog::request));
            EXPECT_THAT(message.size, Eq(3));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x01}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0xc0}));
            EXPECT_THAT(message.data[2], Eq(std::byte{0xde}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {
        .id = id::set_watchdog_watchdog::response, .data = {std::byte{0x01}}, .size = 1};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.set_watchdog_timeout(power_output::v3v3_l2, 49374_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(EpsTest, GetTelemetryU32Success) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::get_telemetry::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::get_telemetry::request));
            EXPECT_THAT(message.size, Eq(2));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x08}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {.id = id::get_telemetry::response,
                                 .data = {std::byte{0x83},
                                          std::byte{0x00},
                                          std::byte{0xef},
                                          std::byte{0xbe},
                                          std::byte{0xad},
                                          std::byte{0xde}},
                                 .size = 6};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.get_telemetry(telemetry_id(8));
    ASSERT_THAT(result.has_value(), Eq(true));
    auto* value = std::get_if<std::uint32_t>(std::addressof(*result));
    ASSERT_THAT(value, Ne(nullptr));
    ASSERT_THAT(*value, Eq(0xdeadbeef));
}

TEST_F(EpsTest, GetTelemetryFloatSuccess) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::get_telemetry::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::get_telemetry::request));
            EXPECT_THAT(message.size, Eq(2));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x0a}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {.id = id::get_telemetry::response,
                                 .data = {std::byte{0xa8},
                                          std::byte{0x00},
                                          std::byte{0xc3},
                                          std::byte{0xf5},
                                          std::byte{0xaa},
                                          std::byte{0x41}},
                                 .size = 6};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.get_telemetry(telemetry_id(10));
    ASSERT_THAT(result.has_value(), Eq(true));
    auto* value = std::get_if<float>(std::addressof(*result));
    ASSERT_THAT(value, Ne(nullptr));
    ASSERT_THAT(*value, FloatEq(21.37f));
}

TEST_F(EpsTest, GetTelemetryMutexTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    auto result = eps.get_telemetry(telemetry_id(0));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(EpsTest, GetTelemetrySendTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::get_telemetry::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::get_telemetry::request));
            EXPECT_THAT(message.size, Eq(2));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x05}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {
        .id = id::get_telemetry::response, .data = {std::byte{0x00}}, .size = 1};
    can_context_.mock_status = communication::can::status::timeout;

    auto result = eps.get_telemetry(telemetry_id(5));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(EpsTest, GetTelemetryEventTimeoutFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::get_telemetry::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::get_telemetry::request));
            EXPECT_THAT(message.size, Eq(2));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x09}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(false));

    can_context_.mock_message = {.id = id::get_telemetry::response,
                                 .data = {std::byte{0x93},
                                          std::byte{0x00},
                                          std::byte{0xef},
                                          std::byte{0xbe},
                                          std::byte{0xad},
                                          std::byte{0xde}},
                                 .size = 6};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.get_telemetry(telemetry_id(9));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(EpsTest, GetTelemetryWrongResponseIdFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::get_telemetry::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::get_telemetry::request));
            EXPECT_THAT(message.size, Eq(2));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x03}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {.id = id::get_telemetry::response,
                                 .data = {std::byte{0x83},
                                          std::byte{0x00},
                                          std::byte{0xef},
                                          std::byte{0xbe},
                                          std::byte{0xad},
                                          std::byte{0xde}},
                                 .size = 6};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.get_telemetry(telemetry_id(3));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::communication_error));
}

TEST_F(EpsTest, GetTelemetryEmptyResponseFailure) {
    eps_type eps{can_context_, 1_s};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1000_ms}));
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(time_point{2000_ms}))
        .WillOnce(Return(true));
    EXPECT_CALL(clock_, now()).WillOnce(Return(time_point{1500_ms}));
    EXPECT_CALL(can_context_, send_mock(_, id::get_telemetry::response, _, 500_ms))
        .WillOnce(Invoke([this](const auto& message, auto, auto, auto) {
            EXPECT_THAT(message.id, Eq(id::get_telemetry::request));
            EXPECT_THAT(message.size, Eq(2));
            EXPECT_THAT(message.is_extended, Eq(true));
            EXPECT_THAT(message.data[0], Eq(std::byte{0x00}));
            EXPECT_THAT(message.data[1], Eq(std::byte{0x01}));
            can_context_.run_one();
        }));
    EXPECT_CALL(*satos::mock::event::events[0], notify());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(time_point{2000_ms}))
        .WillOnce(Return(true));

    can_context_.mock_message = {.id = id::get_telemetry::response, .data = {}, .size = 0};
    can_context_.mock_status = communication::can::status::ok;

    auto result = eps.get_telemetry(telemetry_id(1));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::communication_error));
}

} // namespace
