#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/fram/fram.h"
#include "devices/fram/fram_concept.h"
#include "satos/chrono.h"
#include "satos/clock.h"
#include "spl/drivers/i2c/mock/i2c.h"

namespace {

using namespace testing;
using namespace devices::fram;
using namespace satos::chrono_literals;

using fram_type = fram<spl::drivers::i2c::mock::i2c_vectored>;

static_assert(fram_concept<fram_type>);

struct FramTest : Test {
    NiceMock<spl::drivers::i2c::mock::i2c_vectored> i2c_;
    static constexpr std::uint8_t i2c_address_ = 0xaa;
    static constexpr satos::clock::duration timeout_ = 5_s;
};

TEST_F(FramTest, ReadSuccess) {
    fram_type fram{i2c_, i2c_address_, timeout_};

    std::array<std::byte, 8> buffer;

    EXPECT_CALL(i2c_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto address, auto in, auto out, auto timeout) {
            EXPECT_THAT(address, Eq(i2c_address_));
            EXPECT_THAT(timeout, Eq(timeout_));

            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));

            EXPECT_THAT(in[0].size(), Eq(2));
            EXPECT_THAT(in[0][0], Eq(std::byte{0x12}));
            EXPECT_THAT(in[0][1], Eq(std::byte{0x34}));

            for (unsigned int i = 0; i < out[0].size(); i++) {
                out[0][i] = std::byte(i);
            }

            return spl::drivers::i2c::status::ok;
        }));

    auto result = fram.read(0x1234, buffer);

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->size(), Eq(buffer.size()));

    for (unsigned int i = 0; i < result->size(); i++) {
        ASSERT_THAT(result->data()[i], Eq(std::byte(i)));
    }
}

TEST_F(FramTest, ReadFailure) {
    fram_type fram{i2c_, i2c_address_, timeout_};

    std::array<std::byte, 8> buffer;

    EXPECT_CALL(i2c_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([this](auto address, auto in, auto out, auto timeout) {
            EXPECT_THAT(address, Eq(i2c_address_));
            EXPECT_THAT(timeout, Eq(timeout_));

            EXPECT_THAT(in.size(), Eq(1));
            EXPECT_THAT(out.size(), Eq(1));

            EXPECT_THAT(in[0].size(), Eq(2));
            EXPECT_THAT(in[0][0], Eq(std::byte{0x12}));
            EXPECT_THAT(in[0][1], Eq(std::byte{0x34}));

            return spl::drivers::i2c::status::transfer_error;
        }));

    auto result = fram.read(0x1234, buffer);

    ASSERT_THAT(result.has_value(), Eq(false));
}

TEST_F(FramTest, WriteSucces) {
    fram_type fram{i2c_, i2c_address_, timeout_};

    std::array<std::byte, 8> buffer;
    for (unsigned int i = 0; i < buffer.size(); i++) {
        buffer[i] = std::byte(i);
    }

    EXPECT_CALL(i2c_, write_mock(_, _, _))
        .WillOnce(Invoke([this](auto address, auto in, auto timeout) {
            EXPECT_THAT(address, Eq(i2c_address_));
            EXPECT_THAT(timeout, Eq(timeout_));

            EXPECT_THAT(in.size(), Eq(2));

            EXPECT_THAT(in[0].size(), Eq(2));
            EXPECT_THAT(in[0][0], Eq(std::byte{0x12}));
            EXPECT_THAT(in[0][1], Eq(std::byte{0x34}));

            EXPECT_THAT(in[1].size(), Eq(8));

            for (unsigned int i = 0; i < in[1].size(); i++) {
                EXPECT_THAT(std::byte(i), Eq(in[1][i]));
            }

            return spl::drivers::i2c::status::ok;
        }));

    auto result = fram.write(0x1234, buffer);
    ASSERT_THAT(result, Eq(true));
}

TEST_F(FramTest, WriteFailure) {
    fram_type fram{i2c_, i2c_address_, timeout_};

    std::array<std::byte, 8> buffer;
    for (unsigned int i = 0; i < buffer.size(); i++) {
        buffer[i] = std::byte(i);
    }

    EXPECT_CALL(i2c_, write_mock(_, _, _))
        .WillOnce(Invoke([this](auto address, auto in, auto timeout) {
            EXPECT_THAT(address, Eq(i2c_address_));
            EXPECT_THAT(timeout, Eq(timeout_));

            EXPECT_THAT(in.size(), Eq(2));

            EXPECT_THAT(in[0].size(), Eq(2));
            EXPECT_THAT(in[0][0], Eq(std::byte{0x12}));
            EXPECT_THAT(in[0][1], Eq(std::byte{0x34}));

            EXPECT_THAT(in[1].size(), Eq(8));

            for (unsigned int i = 0; i < in[1].size(); i++) {
                EXPECT_THAT(std::byte(i), Eq(in[1][i]));
            }

            return spl::drivers::i2c::status::timeout;
        }));

    auto result = fram.write(0x1234, buffer);
    ASSERT_THAT(result, Eq(false));
}
} // namespace
