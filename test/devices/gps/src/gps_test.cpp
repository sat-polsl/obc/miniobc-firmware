#include <array>
#include <string_view>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/gps/gps.h"
#include "satext/ranges/enumerate.h"
#include "spl/drivers/uart/mock/uart.h"

namespace {
using namespace testing;
using namespace devices::gps;

using gps_type = gps<spl::drivers::uart::mock::uart>;

struct GpsTest : Test {
    NiceMock<spl::drivers::uart::mock::uart> uart_{};
    std::array<char, 256> buffer_{};
};

TEST_F(GpsTest, ParseSuccess) {
    using messages = nmea::messages<nmea::floating_policy::floating>;
    gps_type gps{uart_, buffer_};
    std::string_view message =
        "$GPGGA,020324,5016.457,N,1840.9022,E,1,01,1.0,420.0,M,0,M,,0000*6D\r\n";

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke([message](auto buffer, auto, auto) {
            auto message_span = std::as_bytes(std::span(message));

            EXPECT_THAT(message_span.size(), Lt(buffer.size()));

            for (auto [i, v] : satext::ranges::enumerate(message_span)) {
                buffer[i] = v;
            }

            return buffer.subspan(0, message_span.size());
        }));

    auto result = gps.read_nmea();

    ASSERT_THAT(result.has_value(), Eq(true));
    auto value = result.value();
    auto p = std::get_if<messages::gpgga>(&value);
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->timestamp, Eq(7404));
    ASSERT_THAT(p->latitude, FloatEq(50.2742844f));
    ASSERT_THAT(p->longitude, FloatEq(18.6817f));
    ASSERT_THAT(p->altitude, FloatEq(420.0f));
    ASSERT_THAT(p->dilution, FloatEq(1.0f));
    ASSERT_THAT(p->satellites, Eq(1));
    ASSERT_THAT(p->quality, Eq(nmea::gps_quality::sps_mode));
}

TEST_F(GpsTest, ParseFailure) {
    gps_type gps{uart_, buffer_};

    EXPECT_CALL(uart_, read_until_mock(_, _, _)).WillOnce(Invoke([](auto, auto, auto) {
        return satext::unexpected{spl::drivers::uart::status::timeout};
    }));

    auto result = gps.read_nmea();

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}
} // namespace
