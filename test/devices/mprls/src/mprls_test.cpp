#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/mprls/mprls.h"
#include "devices/mprls/mprls_concept.h"
#include "satos/chrono.h"
#include "satos/mock/this_thread.h"
#include "spl/drivers/i2c/mock/i2c.h"

namespace {

using namespace testing;
using namespace devices::mprls;
using namespace satos::chrono_literals;

using mprls_type = mprls<spl::drivers::i2c::mock::i2c_vectored>;

static_assert(mprls_concept<mprls_type>);

struct MprlsTest : Test {
    NiceMock<spl::drivers::i2c::mock::i2c_vectored> i2c_;
    satos::mock::this_thread& this_thread_{satos::mock::this_thread::instance()};
    static constexpr satos::clock::duration timeout_ = 1_s;
};

TEST_F(MprlsTest, ReadPressureSuccess) {
    mprls_type mprls{i2c_, timeout_};

    EXPECT_CALL(i2c_, write_mock(_, _, _))
        .WillOnce(Invoke([](auto address, auto buffer, auto timeout) {
            EXPECT_THAT(address, Eq(0x18));
            EXPECT_THAT(timeout, Eq(1000_ms));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte{0xaa}));
            EXPECT_THAT(buffer[0][1], Eq(std::byte{}));
            EXPECT_THAT(buffer[0][2], Eq(std::byte{}));

            return spl::drivers::i2c::status::ok;
        }));

    EXPECT_CALL(this_thread_, sleep_for(20_ms));

    EXPECT_CALL(i2c_, read_mock(_, _, _))
        .WillOnce(Invoke([](auto address, auto buffer, auto timeout) {
            EXPECT_THAT(address, Eq(0x18));
            EXPECT_THAT(timeout, Eq(1000_ms));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(4));

            buffer[0][1] = std::byte{0x90};
            buffer[0][2] = std::byte{0x78};
            buffer[0][3] = std::byte{0x03};

            return spl::drivers::i2c::status::ok;
        }));

    auto result = mprls.read_pressure();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(100045));
}

TEST_F(MprlsTest, ReadPressureI2CWriteFailure) {
    mprls_type mprls{i2c_, timeout_};

    EXPECT_CALL(i2c_, write_mock(_, _, _))
        .WillOnce(Invoke([](auto address, auto buffer, auto timeout) {
            EXPECT_THAT(address, Eq(0x18));
            EXPECT_THAT(timeout, Eq(1000_ms));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte{0xaa}));
            EXPECT_THAT(buffer[0][1], Eq(std::byte{}));
            EXPECT_THAT(buffer[0][2], Eq(std::byte{}));

            return spl::drivers::i2c::status::timeout;
        }));

    auto result = mprls.read_pressure();
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(MprlsTest, ReadPressureI2CReadFailure) {
    mprls_type mprls{i2c_, timeout_};

    EXPECT_CALL(i2c_, write_mock(_, _, _))
        .WillOnce(Invoke([](auto address, auto buffer, auto timeout) {
            EXPECT_THAT(address, Eq(0x18));
            EXPECT_THAT(timeout, Eq(1000_ms));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(3));
            EXPECT_THAT(buffer[0][0], Eq(std::byte{0xaa}));
            EXPECT_THAT(buffer[0][1], Eq(std::byte{}));
            EXPECT_THAT(buffer[0][2], Eq(std::byte{}));

            return spl::drivers::i2c::status::ok;
        }));

    EXPECT_CALL(this_thread_, sleep_for(20_ms));

    EXPECT_CALL(i2c_, read_mock(_, _, _))
        .WillOnce(Invoke([](auto address, auto buffer, auto timeout) {
            EXPECT_THAT(address, Eq(0x18));
            EXPECT_THAT(timeout, Eq(1000_ms));
            EXPECT_THAT(buffer.size(), Eq(1));
            EXPECT_THAT(buffer[0].size(), Eq(4));
            return spl::drivers::i2c::status::timeout;
        }));

    auto result = mprls.read_pressure();
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}
} // namespace
