set(TARGET muons_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/muons_test.cpp
    )

target_include_directories(${TARGET} PUBLIC
    include
    )

target_link_libraries(${TARGET} PRIVATE
    muons
    spl::i2c_mock
    )

add_unit_test(${TARGET})
