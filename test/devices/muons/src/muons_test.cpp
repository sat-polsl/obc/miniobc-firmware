#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/muons/muons.h"
#include "devices/muons/muons_concept.h"
#include "satos/chrono.h"
#include "spl/drivers/i2c/mock/i2c.h"

namespace {
using namespace testing;
using namespace devices::muons;
using namespace satos::chrono_literals;

using muons_type = muons<spl::drivers::i2c::mock::i2c_vectored>;

static_assert(muons_concept<muons_type>);

struct MuonsTest : Test {
    NiceMock<spl::drivers::i2c::mock::i2c_vectored> i2c_;
    std::uint8_t address = 0x47;
};

TEST_F(MuonsTest, ReadCountSuccess) {
    muons_type muons{i2c_, 1000_ms};

    EXPECT_CALL(i2c_, read_mock(address, _, _)).WillOnce(Invoke([](auto, auto out, auto timeout) {
        EXPECT_THAT(timeout, Eq(1000_ms));
        EXPECT_THAT(std::size(out), 1);
        EXPECT_THAT(std::size(out[0]), 4);
        out[0][0] = std::byte{0xde};
        out[0][1] = std::byte{0xad};
        out[0][2] = std::byte{0xc0};
        out[0][3] = std::byte{0xde};

        return spl::drivers::i2c::status::ok;
    }));

    auto result = muons.read_count();

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0xdeadc0de));
}

TEST_F(MuonsTest, ReadCountFailure) {
    muons_type muons{i2c_, 1000_ms};

    EXPECT_CALL(i2c_, read_mock(address, _, _)).WillOnce(Invoke([](auto, auto out, auto timeout) {
        EXPECT_THAT(timeout, Eq(1000_ms));
        EXPECT_THAT(std::size(out), 1);
        EXPECT_THAT(std::size(out[0]), 4);

        return spl::drivers::i2c::status::nack_received;
    }));

    auto result = muons.read_count();

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::nack_received));
}

} // namespace
