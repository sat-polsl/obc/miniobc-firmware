#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/radio/radio.h"
#include "devices/radio/radio_concept.h"
#include "satos/mock/this_thread.h"
#include "spl/drivers/uart/mock/uart.h"
#include "spl/peripherals/gpio/mock/gpio.h"

namespace {
using namespace testing;
using namespace devices::radio;

using radio_type = radio<spl::drivers::uart::mock::uart, spl::peripherals::gpio::mock::gpio>;
static_assert(radio_concept<radio_type>);

struct RadioTest : Test {
    NiceMock<spl::drivers::uart::mock::uart> uart_;
    NiceMock<spl::peripherals::gpio::mock::gpio> gpio_;
    satos::mock::this_thread& this_thread_{satos::mock::this_thread::instance()};

    void expect_request_response_success(std::string_view command, std::string_view response);
    void expect_request_response_timeout(std::string_view command);
};

MATCHER_P(SpanEq, s, "") {
    if (arg.size() != s.size()) {
        return false;
    }

    for (unsigned int i = 0; i < s.size(); i++) {
        if (arg[i] != s[i]) {
            return false;
        }
    }

    return true;
}

void RadioTest::expect_request_response_success(std::string_view command,
                                                std::string_view response) {
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(
            Invoke([command](std::span<const std::byte> buffer, satos::clock::duration timeout) {
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto command_span = std::as_bytes(std::span(command.data(), command.size()));

                EXPECT_THAT(buffer, SpanEq(command_span));

                return spl::drivers::uart::status::ok;
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke([response](std::span<std::byte> buffer,
                                    std::byte expected,
                                    satos::clock::duration timeout) {
            EXPECT_THAT(expected, Eq(std::byte{'\n'}));
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            auto response_span = std::as_bytes(std::span(response.data(), response.size()));
            std::copy(response_span.begin(), response_span.end(), buffer.begin());
            return buffer.subspan(0, response_span.size());
        }));
}

void RadioTest::expect_request_response_timeout(std::string_view command) {
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(
            Invoke([command](std::span<const std::byte> buffer, satos::clock::duration timeout) {
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto command_span = std::as_bytes(std::span(command.data(), command.size()));

                EXPECT_THAT(buffer, SpanEq(command_span));

                return spl::drivers::uart::status::ok;
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                return satext::unexpected{spl::drivers::uart::status::timeout};
            }));
}

TEST_F(RadioTest, ResetSuccess) {
    radio_type radio{uart_, gpio_};

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(this_thread_, sleep_for(satos::chrono::milliseconds{100}));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{500}));
                std::string_view version = "RN2483 1.0.3 Mar 22 2017 06:00:42\r\n";
                auto version_span = std::as_bytes(std::span(version.data(), version.size()));
                std::copy(version_span.begin(), version_span.end(), buffer.begin());
                return buffer.subspan(0, version_span.size());
            }));

    EXPECT_CALL(gpio_, set());

    auto result = radio.reset();
    ASSERT_THAT(result, Eq(true));
}

TEST_F(RadioTest, ResetWrongVersionFailure) {
    radio_type radio{uart_, gpio_};

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(this_thread_, sleep_for(satos::chrono::milliseconds{100}));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{500}));
                std::string_view version = "RN2483 1.0.2 Mar 22 2017 06:00:42\r\n";
                auto version_span = std::as_bytes(std::span(version.data(), version.size()));
                std::copy(version_span.begin(), version_span.end(), buffer.begin());
                return buffer.subspan(0, version_span.size());
            }));

    EXPECT_CALL(gpio_, set());

    auto result = radio.reset();
    ASSERT_THAT(result, Eq(false));
}

TEST_F(RadioTest, ResetTimeoutFailure) {
    radio_type radio{uart_, gpio_};

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(this_thread_, sleep_for(satos::chrono::milliseconds{100}));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{500}));
                return satext::unexpected{spl::drivers::uart::status::timeout};
            }));

    EXPECT_CALL(gpio_, set());

    auto result = radio.reset();
    ASSERT_THAT(result, Eq(false));
}

TEST_F(RadioTest, MacPauseSuccess) {
    radio_type radio{uart_, gpio_};

    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "mac pause\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));

            EXPECT_THAT(buffer, SpanEq(command_span));

            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                std::string_view mac_pause = "4294967295\r\n";
                auto mac_pause_span = std::as_bytes(std::span(mac_pause.data(), mac_pause.size()));
                std::copy(mac_pause_span.begin(), mac_pause_span.end(), buffer.begin());
                return buffer.subspan(0, mac_pause_span.size());
            }));

    auto result = radio.mac_pause();
    ASSERT_THAT(result, Eq(true));
}

TEST_F(RadioTest, MacPauseFailure) {
    radio_type radio{uart_, gpio_};

    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "mac pause\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));

            EXPECT_THAT(buffer, SpanEq(command_span));

            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                return satext::unexpected{spl::drivers::uart::status::timeout};
            }));

    auto result = radio.mac_pause();
    ASSERT_THAT(result, Eq(false));
}

TEST_F(RadioTest, SetModulationSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set mod lora\r\n", response::ok);

    auto result = radio.set_modulation(modulation::lora);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetModulationInvalidParameterFailure) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set mod lora\r\n", response::invalid_parameter);

    auto result = radio.set_modulation(modulation::lora);
    ASSERT_THAT(result, Eq(status::invalid_parameter));
}

TEST_F(RadioTest, SetModulationTimeoutFailure) {
    radio_type radio{uart_, gpio_};

    expect_request_response_timeout("radio set mod lora\r\n");

    auto result = radio.set_modulation(modulation::lora);
    ASSERT_THAT(result, Eq(status::parse_error));
}

TEST_F(RadioTest, SetModulationParseFailure) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set mod lora\r\n", "ok\r");

    auto result = radio.set_modulation(modulation::lora);
    ASSERT_THAT(result, Eq(status::parse_error));
}

TEST_F(RadioTest, SetFrequencySuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set freq 434700000\r\n", response::ok);

    auto result = radio.set_frequency(434700000);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetPowerSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set pwr 15\r\n", response::ok);

    auto result = radio.set_power(15);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetSpreadingFactorSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set sf sf12\r\n", response::ok);

    auto result = radio.set_spreading_factor(spreading_factor::sf12);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetBandwidthSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set bw 500\r\n", response::ok);

    auto result = radio.set_bandwidth(bandwidth::bw500khz);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetCodingRateSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set cr 4/7\r\n", response::ok);

    auto result = radio.set_coding_rate(coding_rate::cr4_7);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetSyncSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set sync 12\r\n", response::ok);

    auto result = radio.set_sync({std::byte{0x12}});
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetIqInversionSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set iqi on\r\n", response::ok);

    auto result = radio.set_iq_inversion(on_off::on);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, SetWatchdogTimeoutSuccess) {
    radio_type radio{uart_, gpio_};

    expect_request_response_success("radio set wdt 2000\r\n", response::ok);

    auto result = radio.set_watchdog_timeout(satos::chrono::seconds{2});
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(RadioTest, TransmitSuccess) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 4> buffer = {
        std::byte{0xde}, std::byte{0xad}, std::byte{0xbe}, std::byte{0xef}};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio tx deadbeef\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::ok.data(), response::ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{3000}));
                auto response_span = std::as_bytes(
                    std::span(response::radio_tx_ok.data(), response::radio_tx_ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.transmit(buffer);
    ASSERT_THAT(result, Eq(status::radio_tx_ok));
}

TEST_F(RadioTest, TransmitInvalidParameterFailure) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 4> buffer = {
        std::byte{0xde}, std::byte{0xad}, std::byte{0xbe}, std::byte{0xef}};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio tx deadbeef\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span = std::as_bytes(std::span(response::invalid_parameter.data(),
                                                             response::invalid_parameter.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.transmit(buffer);
    ASSERT_THAT(result, Eq(status::invalid_parameter));
}

TEST_F(RadioTest, TransmitRadioErrorFailure) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 4> buffer = {
        std::byte{0xde}, std::byte{0xad}, std::byte{0xbe}, std::byte{0xef}};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio tx deadbeef\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::ok.data(), response::ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{3000}));
                auto response_span = std::as_bytes(
                    std::span(response::radio_err.data(), response::radio_err.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.transmit(buffer);
    ASSERT_THAT(result, Eq(status::radio_error));
}

TEST_F(RadioTest, ReceiveSuccess) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 4> buffer{};
    std::array<std::byte, 4> expected = {
        std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio rx 0\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::ok.data(), response::ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{2000}));
                std::string_view response = "radio_rx  deadc0de\r\n";
                auto response_span = std::as_bytes(std::span(response.data(), response.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.receive(buffer, satos::chrono::seconds{2});
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value().size(), Eq(4));
    ASSERT_THAT(buffer, ContainerEq(expected));
}

TEST_F(RadioTest, ReceiveRequestFailure) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 4> buffer{};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio rx 0\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::busy.data(), response::busy.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.receive(buffer, satos::chrono::seconds{2});
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::busy));
}

TEST_F(RadioTest, ReceiveRadioErrorFailure) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 4> buffer{};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio rx 0\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::ok.data(), response::ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{2000}));
                std::string_view response = "radio_error\r\n";
                auto response_span = std::as_bytes(std::span(response.data(), response.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.receive(buffer, satos::chrono::seconds{2});
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::radio_error));
}

TEST_F(RadioTest, ReceiveParseErrorFailure) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 3> buffer{};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio rx 0\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::ok.data(), response::ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{2000}));
                std::string_view response = "radio_rx deadc0de\r\n";
                auto response_span = std::as_bytes(std::span(response.data(), response.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.receive(buffer, satos::chrono::seconds{2});
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::parse_error));
}

TEST_F(RadioTest, ReceiveSuccessGreaterBuffer) {
    radio_type radio{uart_, gpio_};
    std::array<std::byte, 8> buffer{};
    std::array<std::byte, 4> expected = {
        std::byte{0xde}, std::byte{0xad}, std::byte{0xc0}, std::byte{0xde}};

    InSequence s;
    EXPECT_CALL(uart_, write_mock(_, _))
        .WillOnce(Invoke([](std::span<const std::byte> buffer, satos::clock::duration timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
            std::string_view command = "radio rx 0\r\n";
            auto command_span = std::as_bytes(std::span(command.data(), command.size()));
            EXPECT_THAT(buffer, SpanEq(command_span));
            return spl::drivers::uart::status::ok;
        }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{100}));
                auto response_span =
                    std::as_bytes(std::span(response::ok.data(), response::ok.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    EXPECT_CALL(uart_, read_until_mock(_, _, _))
        .WillOnce(Invoke(
            [](std::span<std::byte> buffer, std::byte expected, satos::clock::duration timeout) {
                EXPECT_THAT(expected, Eq(std::byte{'\n'}));
                EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds{2000}));
                std::string_view response = "radio_rx deadc0de\r\n";
                auto response_span = std::as_bytes(std::span(response.data(), response.size()));
                std::copy(response_span.begin(), response_span.end(), buffer.begin());
                return buffer.subspan(0, response_span.size());
            }));

    auto result = radio.receive(buffer, satos::chrono::seconds{2});
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value().size(), Eq(4));
    for (unsigned int i = 0; i < result.value().size(); i++) {
        ASSERT_THAT(result.value()[i], Eq(expected[i]));
    }
}

} // namespace
