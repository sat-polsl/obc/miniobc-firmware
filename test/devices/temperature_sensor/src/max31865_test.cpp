#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/temperature_sensor/max31865.h"
#include "devices/temperature_sensor/temperature_sensor_concept.h"
#include "satos/chrono.h"
#include "spl/drivers/spi/mock/spi.h"
#include "spl/peripherals/gpio/mock/gpio.h"

namespace {
using namespace testing;
using namespace devices::temperature_sensor;
using namespace satos::chrono_literals;

using max_type =
    max31865<spl::peripherals::gpio::mock::gpio, spl::drivers::spi::mock::spi_vectored>;

static_assert(temperature_sensor_concept<max_type>);

struct Max31865Test : Test {
    NiceMock<spl::peripherals::gpio::mock::gpio> gpio_;
    NiceMock<spl::drivers::spi::mock::spi_vectored> spi_;
};

TEST_F(Max31865Test, ReadTemperatureRawSuccess) {
    max_type max{spi_, gpio_, 1000_ms};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([](auto&, auto in, auto out, auto timeout) {
            EXPECT_THAT(std::size(in), 1);
            EXPECT_THAT(std::size(in[0]), 1);
            EXPECT_THAT(in[0][0], std::byte{0x01});

            EXPECT_THAT(std::size(out), 1);
            EXPECT_THAT(std::size(out[0]), 2);
            out[0][0] = std::byte{0xaa};
            out[0][1] = std::byte{0xcc};

            return spl::drivers::spi::status::ok;
        }));

    auto result = max.read_temperature_raw();

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0x5566));
}

TEST_F(Max31865Test, ReadTemperatureRawCommunicationFailure) {
    max_type max{spi_, gpio_, 1000_ms};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([](auto&, auto in, auto out, auto timeout) {
            EXPECT_THAT(std::size(in), 1);
            EXPECT_THAT(std::size(in[0]), 1);
            EXPECT_THAT(in[0][0], std::byte{0x01});
            return spl::drivers::spi::status::transfer_error;
        }));

    auto result = max.read_temperature_raw();

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::communication_error));
}

TEST_F(Max31865Test, ReadTemperatureRawDeviceFailure) {
    max_type max{spi_, gpio_, 1000_ms};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([](auto&, auto in, auto out, auto timeout) {
            EXPECT_THAT(std::size(in), 1);
            EXPECT_THAT(std::size(in[0]), 1);
            EXPECT_THAT(in[0][0], std::byte{0x01});

            EXPECT_THAT(std::size(out), 1);
            EXPECT_THAT(std::size(out[0]), 2);
            out[0][0] = std::byte{0x00};
            out[0][1] = std::byte{0x01};

            return spl::drivers::spi::status::ok;
        }));

    auto result = max.read_temperature_raw();

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::device_fault));
}

TEST_F(Max31865Test, ReadTemperatureSuccess) {
    max_type max{spi_, gpio_, 1000_ms};

    EXPECT_CALL(spi_, write_read_mock(_, _, _, _))
        .WillOnce(Invoke([](auto&, auto in, auto out, auto timeout) {
            EXPECT_THAT(std::size(in), 1);
            EXPECT_THAT(std::size(in[0]), 1);
            EXPECT_THAT(in[0][0], std::byte{0x01});

            EXPECT_THAT(std::size(out), 1);
            EXPECT_THAT(std::size(out[0]), 2);
            out[0][0] = std::byte{0x40};
            out[0][1] = std::byte{0x00};

            return spl::drivers::spi::status::ok;
        }));

    auto result = max.read_temperature();

    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), FloatEq(19.557373f));
}

} // namespace
