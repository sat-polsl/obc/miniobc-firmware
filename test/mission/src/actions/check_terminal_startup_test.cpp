#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mission/actions/check_terminal_startup.h"
#include "spl/drivers/uart/mock/uart.h"
#include "terminal/terminal.h"

namespace {

using namespace ::testing;
using namespace mission::actions;

struct CheckTerminalStartupTest : public Test {
    using terminal_uart = spl::drivers::uart::mock::uart;

    std::array<char, 128> terminal_buffer_{};
    std::array<std::string_view, 10> terminal_tokens_{};

    terminal_uart uart_{};
    terminal::terminal<terminal_uart> terminal_{uart_, terminal_buffer_, terminal_tokens_, "", ""};
};

TEST_F(CheckTerminalStartupTest, TerminalLineReaded) {
    check_terminal_startup<terminal::terminal<terminal_uart>> check_terminal_startup{};
    mission::terminal_state state{mission::terminal_state::not_started};

    EXPECT_CALL(uart_, read_until_mock(_, _, _)).WillOnce(Invoke([](auto buffer, auto, auto) {
        return std::span<const std::byte>(buffer);
    }));

    check_terminal_startup(terminal_, state);
    EXPECT_THAT(state, Eq(mission::terminal_state::started));
}

TEST_F(CheckTerminalStartupTest, TerminalLineNotReaded) {
    check_terminal_startup<terminal::terminal<terminal_uart>> check_terminal_startup{};
    mission::terminal_state state{mission::terminal_state::not_started};

    EXPECT_CALL(uart_, read_until_mock(_, _, _)).WillOnce(Invoke([](auto, auto, auto) {
        return satext::unexpected{spl::drivers::uart::status::timeout};
    }));

    check_terminal_startup(terminal_, state);
    EXPECT_THAT(state, Eq(mission::terminal_state::not_started));
}

} // namespace
