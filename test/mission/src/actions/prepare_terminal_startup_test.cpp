#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mission/actions/prepare_terminal_startup.h"
#include "satos/mock/clock.h"
#include "spl/drivers/uart/mock/uart.h"
#include "terminal/terminal.h"

namespace {

using namespace ::testing;
using namespace mission::actions;

struct PrepareTerminalStartupTest : public Test {
    using terminal_uart = spl::drivers::uart::mock::uart;

    satos::mock::clock& clock_{satos::mock::clock::instance()};

    std::array<char, 128> terminal_buffer_{};
    std::array<std::string_view, 10> terminal_tokens_{};

    terminal_uart uart_{};
    terminal::terminal<terminal_uart> terminal_{uart_, terminal_buffer_, terminal_tokens_, "", ""};
};

TEST_F(PrepareTerminalStartupTest, PrepareTerminalStartup) {
    prepare_terminal_startup<terminal::terminal<terminal_uart>> prepare_terminal_startup{};
    mission::terminal_state state{mission::terminal_state::timeout};
    satos::clock::time_point time_point{};

    EXPECT_CALL(clock_, now())
        .WillOnce(Return(satos::clock::time_point{satos::chrono::milliseconds{1000}}));

    prepare_terminal_startup(time_point, state, terminal_);
    EXPECT_THAT(state, Eq(mission::terminal_state::not_started));
    EXPECT_THAT(time_point, Eq(satos::clock::time_point{satos::chrono::milliseconds{1000}}));
}

} // namespace
