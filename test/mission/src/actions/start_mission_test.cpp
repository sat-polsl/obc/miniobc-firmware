#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mission/actions/start_mission.h"
#include "satext/tag.h"
#include "satos/mock/thread.h"
#include "spl/drivers/uart/mock/uart.h"
#include "terminal/terminal.h"
#include "watchdog/thread_watchdog/mock/thread_watchdog.h"

namespace {

using namespace ::testing;
using namespace mission::actions;

struct StartMissionTest : public Test {
    using terminal_uart = spl::drivers::uart::mock::uart;

    std::array<char, 128> terminal_buffer_{};
    std::array<std::string_view, 10> terminal_tokens_{};

    terminal_uart uart_{};
    terminal::terminal<terminal_uart> terminal_{uart_, terminal_buffer_, terminal_tokens_, "", ""};

    satos::mock::thread gps_thread_{};

    watchdog::mock::thread_watchdog hw_watchdog_{};
    watchdog::mock::thread_watchdog eps_watchdog_{};

    using hw_watchdog = satext::tag_ptr<watchdog::mock::thread_watchdog, struct hw_watchdog_tag>;
    using eps_watchdog = satext::tag_ptr<watchdog::mock::thread_watchdog, struct eps_watchdog_tag>;
};

TEST_F(StartMissionTest, StartMission) {
    start_mission<satos::mock::thread, terminal::terminal<terminal_uart>, hw_watchdog, eps_watchdog>
        start_mission{};

    EXPECT_CALL(gps_thread_, start());
    EXPECT_CALL(gps_thread_, native_handle())
        .WillRepeatedly(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(hw_watchdog_, register_thread_mock(_, satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(eps_watchdog_, register_thread_mock(_, satos::thread_native_handle(0xdeadc0de)));
    start_mission(gps_thread_, terminal_, hw_watchdog{hw_watchdog_}, eps_watchdog{eps_watchdog_});
}

} // namespace
