#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mission/actions/start_terminal.h"
#include "satos/mock/thread.h"

namespace {

using namespace ::testing;
using namespace mission::actions;

struct StartTerminalTest : public Test {
    satos::mock::thread terminal_thread_{};
};

TEST_F(StartTerminalTest, StartMission) {
    start_terminal<satos::mock::thread> start_terminal{};

    EXPECT_CALL(terminal_thread_, start());
    start_terminal(terminal_thread_);
}

} // namespace
