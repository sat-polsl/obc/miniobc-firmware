#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mission/guards/terminal_not_started.h"
#include "mission/guards/terminal_started.h"
#include "mission/guards/terminal_timeout.h"
#include "satos/mock/clock.h"

namespace {
using namespace ::testing;
using namespace satos::chrono_literals;

struct TerminalGuardsTest : public Test {
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

TEST_F(TerminalGuardsTest, TerminalNotStartedTrue) {
    mission::guards::terminal_not_started terminal_not_started{};
    EXPECT_THAT(terminal_not_started(mission::terminal_state::not_started), Eq(true));
}

TEST_F(TerminalGuardsTest, TerminalNotStartedFalse) {
    mission::guards::terminal_not_started terminal_not_started{};
    EXPECT_THAT(terminal_not_started(mission::terminal_state::started), Eq(false));
}

TEST_F(TerminalGuardsTest, TerminalStartedTrue) {
    mission::guards::terminal_started terminal_started{};
    EXPECT_THAT(terminal_started(mission::terminal_state::started), Eq(true));
}

TEST_F(TerminalGuardsTest, TerminalStartedFalse) {
    mission::guards::terminal_started terminal_started{};
    EXPECT_THAT(terminal_started(mission::terminal_state::not_started), Eq(false));
}

TEST_F(TerminalGuardsTest, TerminalTimeoutTrue) {
    mission::guards::terminal_timeout terminal_timeout{};
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point(5_s)));
    EXPECT_THAT(terminal_timeout(satos::clock::time_point(1_s)), Eq(true));
}

TEST_F(TerminalGuardsTest, TerminalTimeoutFalse) {
    mission::guards::terminal_timeout terminal_timeout{};
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point(2_s)));
    EXPECT_THAT(terminal_timeout(satos::clock::time_point(1_s)), Eq(false));
}

} // namespace
