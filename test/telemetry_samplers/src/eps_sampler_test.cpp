#include "gtest/gtest.h"

#include "gmock/gmock.h"
#include "devices/eps/mock/eps.h"
#include "satext/expected.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/eps_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;
using telemetry::telemetry_id;

constexpr telemetry::samplers::eps_sampler_config config{telemetry_id{5},
                                                         telemetry_id{6},
                                                         telemetry_id{7},
                                                         telemetry_id{8},
                                                         telemetry_id{9},
                                                         telemetry_id{10},
                                                         telemetry_id{11},
                                                         telemetry_id{12},
                                                         telemetry_id{13},
                                                         telemetry_id{14},
                                                         telemetry_id{15},
                                                         telemetry_id{16},
                                                         telemetry_id{17},
                                                         telemetry_id{18}};

using sampler = telemetry::samplers::
    eps_sampler<devices::eps::mock::eps, telemetry::mock::telemetry_state, config>;

static_assert(telemetry::sampler_concept<sampler>);

struct EpsSamplerTest : Test {
    StrictMock<telemetry::mock::telemetry_state> state_;
    StrictMock<devices::eps::mock::eps> eps_;
};

TEST_F(EpsSamplerTest, SampleSuccess) {
    sampler eps_sampler{eps_};

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::boot_counter))
        .WillOnce(Return(telemetry::telemetry_value{1234u}));
    EXPECT_CALL(state_, write_telemetry_value(telemetry_id{5}, telemetry::telemetry_value{1234u}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::uptime))
        .WillOnce(Return(telemetry::telemetry_value{120000u}));
    EXPECT_CALL(state_,
                write_telemetry_value(telemetry_id{6}, telemetry::telemetry_value{120000u}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_3v3))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{1234}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{7}, telemetry::telemetry_value{std::int16_t{1234}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_5v))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{2345}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{8}, telemetry::telemetry_value{std::int16_t{2345}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_12v))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-1234}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{9}, telemetry::telemetry_value{std::int16_t{-1234}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell1))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-2345}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{10}, telemetry::telemetry_value{std::int16_t{-2345}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell2))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-3456}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{11}, telemetry::telemetry_value{std::int16_t{-3456}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell3))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-4567}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{12}, telemetry::telemetry_value{std::int16_t{-4567}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::v3v3v_l2))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{1234}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{13}, telemetry::telemetry_value{std::uint16_t{1234}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::i3v3v_l2))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{2345}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{14}, telemetry::telemetry_value{std::uint16_t{2345}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::v5v_l1))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{3456}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{15}, telemetry::telemetry_value{std::uint16_t{3456}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::i5v_l1))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{4567}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{16}, telemetry::telemetry_value{std::uint16_t{4567}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::vbms))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{5678}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{17}, telemetry::telemetry_value{std::uint16_t{5678}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::ibms))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{6789}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{18}, telemetry::telemetry_value{std::uint16_t{6789}}));

    eps_sampler(state_);
}

TEST_F(EpsSamplerTest, SampleFailure) {
    sampler eps_sampler{eps_};

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::boot_counter))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::uptime))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_3v3))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_5v))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_12v))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell1))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell2))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell3))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::v3v3v_l2))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::i3v3v_l2))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::v5v_l1))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::i5v_l1))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::vbms))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));
    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::ibms))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));

    eps_sampler(state_);
}

TEST_F(EpsSamplerTest, SampleSingleFailure) {
    sampler eps_sampler{eps_};

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::boot_counter))
        .WillOnce(Return(telemetry::telemetry_value{1234u}));
    EXPECT_CALL(state_, write_telemetry_value(telemetry_id{5}, telemetry::telemetry_value{1234u}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::uptime))
        .WillOnce(Return(satext::unexpected{devices::eps::status::communication_error}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_3v3))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{1234}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{7}, telemetry::telemetry_value{std::int16_t{1234}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_5v))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{2345}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{8}, telemetry::telemetry_value{std::int16_t{2345}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_12v))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-1234}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{9}, telemetry::telemetry_value{std::int16_t{-1234}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell1))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-2345}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{10}, telemetry::telemetry_value{std::int16_t{-2345}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell2))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-3456}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{11}, telemetry::telemetry_value{std::int16_t{-3456}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::temperature_cell3))
        .WillOnce(Return(telemetry::telemetry_value{std::int16_t{-4567}}));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{12}, telemetry::telemetry_value{std::int16_t{-4567}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::v3v3v_l2))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{1234}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{13}, telemetry::telemetry_value{std::uint16_t{1234}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::i3v3v_l2))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{2345}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{14}, telemetry::telemetry_value{std::uint16_t{2345}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::v5v_l1))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{3456}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{15}, telemetry::telemetry_value{std::uint16_t{3456}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::i5v_l1))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{4567}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{16}, telemetry::telemetry_value{std::uint16_t{4567}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::vbms))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{5678}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{17}, telemetry::telemetry_value{std::uint16_t{5678}}));

    EXPECT_CALL(eps_, get_telemetry(devices::eps::telemetry_id::ibms))
        .WillOnce((Return(telemetry::telemetry_value{std::uint16_t{6789}})));
    EXPECT_CALL(
        state_,
        write_telemetry_value(telemetry_id{18}, telemetry::telemetry_value{std::uint16_t{6789}}));

    eps_sampler(state_);
}

} // namespace
