#include "gtest/gtest.h"

#include "gmock/gmock.h"
#include "devices/muons/mock/muons.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/muons_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;

using sampler = telemetry::samplers::muons_sampler<devices::muons::mock::muons,
                                                   telemetry::mock::telemetry_state,
                                                   telemetry::telemetry_id{5}>;

static_assert(telemetry::sampler_concept<sampler>);

struct MuonsSamplerTest : Test {
    StrictMock<telemetry::mock::telemetry_state> state_;
    StrictMock<devices::muons::mock::muons> muons_;
};

TEST_F(MuonsSamplerTest, SampleCountSuccess) {
    sampler muons_sampler{muons_};

    EXPECT_CALL(muons_, read_count()).WillOnce(Return(0xdeadc0de));
    EXPECT_CALL(state_, write_u32(telemetry::telemetry_id{5}, 0xdeadc0de));

    muons_sampler(state_);
}

TEST_F(MuonsSamplerTest, SampleCountFailure) {
    sampler muons_sampler{muons_};

    EXPECT_CALL(muons_, read_count())
        .WillOnce(Return(satext::unexpected{spl::drivers::i2c::status::timeout}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{5}, std::monostate{}));

    muons_sampler(state_);
}

} // namespace
