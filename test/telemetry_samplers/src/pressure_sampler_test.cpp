#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/mprls/mock/mprls.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/pressure_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;

using sampler = telemetry::samplers::pressure_sampler<devices::mprls::mock::mprls,
                                                      telemetry::mock::telemetry_state,
                                                      telemetry::telemetry_id{10}>;

static_assert(telemetry::sampler_concept<sampler>);

struct PressureSamplerTest : Test {
    StrictMock<telemetry::mock::telemetry_state> state_;
    StrictMock<devices::mprls::mock::mprls> mprls_;
};

TEST_F(PressureSamplerTest, SamplePressureSuccess) {
    sampler pressure_sampler{mprls_};

    EXPECT_CALL(mprls_, read_pressure()).WillOnce(Return(1234));
    EXPECT_CALL(state_, write_u32(telemetry::telemetry_id{10}, 1234));

    pressure_sampler(state_);
}

TEST_F(PressureSamplerTest, SamplePressureFailure) {
    sampler pressure_sampler{mprls_};

    EXPECT_CALL(mprls_, read_pressure())
        .WillOnce(Return(satext::unexpected{spl::drivers::i2c::status::timeout}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{10}, std::monostate{}));

    pressure_sampler(state_);
}

} // namespace
