#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "devices/temperature_sensor/mock/temperature_sensor.h"
#include "telemetry/mock/telemetry_state.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/samplers/temperature_sampler.h"

namespace {
using namespace testing;
using namespace telemetry::samplers;

using sampler =
    telemetry::samplers::temperature_sampler<devices::temperature_sensor::mock::temperature_sensor,
                                             telemetry::mock::telemetry_state,
                                             telemetry::telemetry_id{2}>;

static_assert(telemetry::sampler_concept<sampler>);

struct TemperatureSamplerTest : Test {
    StrictMock<telemetry::mock::telemetry_state> state_;
    StrictMock<devices::temperature_sensor::mock::temperature_sensor> temperature_sensor_;
};

TEST_F(TemperatureSamplerTest, SampleTemperatureSuccess) {
    sampler temperature_sampler{temperature_sensor_};

    EXPECT_CALL(temperature_sensor_, read_temperature()).WillOnce(Return(21.37f));
    EXPECT_CALL(state_, write_float(telemetry::telemetry_id{2}, 21.37f));

    temperature_sampler(state_);
}

TEST_F(TemperatureSamplerTest, SampleTemperatureFailure) {
    sampler temperature_sampler{temperature_sensor_};

    EXPECT_CALL(temperature_sensor_, read_temperature())
        .WillOnce(Return(satext::unexpected{devices::temperature_sensor::error::device_fault}));
    EXPECT_CALL(state_, write_monostate(telemetry::telemetry_id{2}, std::monostate{}));

    temperature_sampler(state_);
}

} // namespace
